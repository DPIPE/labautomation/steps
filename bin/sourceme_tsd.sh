#!/bin/bash

# If you run Steps in TSD and use SLURM, source this script to override sbatch, scancel and squeue so that they run on a submit node via ssh.
# This is useful if Nextflow is running in a container not allowed to run sbatch directly on the host machine.

STEPS_SBATCH_MACHINE=${STEPS_SBATCH_MACHINE:-p22-submit} # Remote machine to ssh into from where to run sbatch/squeue/scancel
STEPS_SBATCH_USER=${STEPS_SBATCH_USER:-p22-serviceuser01} # User in remote machine
STEPS_SSH_IDENTITY_FILE=${STEPS_SSH_IDENTITY_FILE:-/ess/p22/data/durable/serviceuser/serviceuser_key} # Identity file to use for ssh, must be available from within the container (which runs as 'root')
STEPS_KNOWN_HOSTS_FILE=${STEPS_KNOWN_HOSTS_FILE:-/ess/p22/data/durable/serviceuser/known_hosts} # List of approved ssh hosts, must be available from within the container (which runs as 'root')

STEPS_SBATCH_PATH=${STEPS_SBATCH_PATH:-/usr/bin/sbatch}
STEPS_SCANCEL_PATH=${STEPS_SCANCEL_PATH:-/usr/bin/scancel}
STEPS_SQUEUE_PATH=${STEPS_SQUEUE_PATH:-/usr/bin/squeue}

sbatch() {
    ssh -i "${STEPS_SSH_IDENTITY_FILE}" -o UserKnownHostsFile="${STEPS_KNOWN_HOSTS_FILE}" \
        "${STEPS_SBATCH_USER}@${STEPS_SBATCH_MACHINE}" "cd ${PWD} && ${STEPS_SBATCH_PATH} $@"
}

scancel() {
    ssh -i "${STEPS_SSH_IDENTITY_FILE}" -o UserKnownHostsFile="${STEPS_KNOWN_HOSTS_FILE}" \
        "${STEPS_SBATCH_USER}@${STEPS_SBATCH_MACHINE}" "${STEPS_SCANCEL_PATH}" "$@"
}

squeue() {
    ssh -i "${STEPS_SSH_IDENTITY_FILE}" -o UserKnownHostsFile="${STEPS_KNOWN_HOSTS_FILE}" \
        "${STEPS_SBATCH_USER}@${STEPS_SBATCH_MACHINE}" "${STEPS_SQUEUE_PATH}" "$@"
}

export -f sbatch squeue scancel

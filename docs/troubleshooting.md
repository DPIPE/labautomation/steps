## Container(s) won't come up

#### Symptom

When running `docker compose up`, one or more containers show as not being up.

#### Solution

If you have a problem with the containers not coming up, you can try to remove all containers, and then start over:

```bash
docker compose down --remove-orphans
docker compose build
sudo rm -rf .volumes
docker compose up
```

## Maestro won't start - neo4j or nats issues

#### Symptom
    
When running `python src/main.py`, the process won't start, and you get an error message relating to NATS or Neo4j.

#### Solution

Check first that containers are up and running with `docker compose up`. If not, see above.

Clean up the database and NATS data:

```bash
python ops/dev/reset_all.py
```

If that doesn't work, try to remove all containers, and then start over:

```bash
docker compose down --remove-orphans
docker compose build
sudo rm -rf .volumes
docker compose up
```

## Unable to stop maestro

#### Symptom

When running e.g. `python src/main.py`, the process won't stop when pressing Ctrl+C.

#### Solutions

You need to send SIGQUIT to the process (Ctrl+C sends SIGINT).

Ctrl + \ (backslash) sends SIGQUIT (might depend on keyboard layout).

Alternatively, you can send process to background (Ctrl+Z), and then kill the process with `kill %1`.

"""End-to-end test for the full workflow.

Pattern to handle async tests that need to wait:
- Hook a test step to the end of the workflow. Take whatever input you want to test and assert things.
- The test step should return a final test artefact.
- (Note that assertions failing in that step will not fail the pytest).
- Subscribe to a NATS message to know when that last step in the workflow has finished and execution can continue.
- Then verify the final test artefact in the DB, which will only be there if assertions did not fail.
"""

import asyncio
import shutil
from pathlib import Path

import pytest
from artefacts.vcpipe import AnnopipeOutput
from maestro import BaseArtefactModel, Job, MessageWatcher, WorkflowModel, run_workflow_steps
from maestro.config import Config
from maestro.nats.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import read_node
from maestro.workflow_utils import order_workflow
from workflowsteps.annopipe import Annopipe
from workflowsteps.basepipe import Basepipe
from workflowsteps.dotanalysiswatcher import DotanalysisFileWatcher
from workflowsteps.dotsamplewatcher import DotsampleFileWatcher
from workflowsteps.fastqwatcher import FASTQFileWatcher, SeqDataMaker
from workflowsteps.taqmanwatcher import TaqmanFileWatcher
from workflowsteps.triopipe import Triopipe

DATA_DIR = Path(__file__).resolve().parent / "data"
SEQDATA_ID = "7d181fdf-6520-45fd-b62c-b933cedca0b6"


class LastTestStepOutput(BaseArtefactModel):
    """Dummy artefact for testing."""

    seqdata_id: str


class LastTestStepInWorkflow(MessageWatcher):
    """A test step we hook on to the workflow end to do assertions.

    Note that assertions failing here will not fail the pytest on their own.
    But if assertions fail, then no artefact will be returned.
    At the end of the test, we check the DB for that final artefact.
    """

    input_types = {AnnopipeOutput}

    async def process(self, input: AnnopipeOutput):
        assert str(input.seqdata_id) == SEQDATA_ID
        print("Running LastTestStepInWorkflow now")

        return [LastTestStepOutput(seqdata_id=str(input.seqdata_id))]


@pytest.mark.destructive  # Test is destructive and will be skipped by default
async def test_single_end_to_end(tmp_path, truncate_database, mock_vcpipe):
    """Runs base and annotation analyses for a single seqdata (cp_single test).

    Uses a Future to signal when the workflow has finished.
    The Future is set when 'workflow_step.LastStepInWorkflow.done' is published on NATS.

    NOTE: This does not run the actual Nextflow pipelines, but mocked versions.
    """

    file_watch_dir = tmp_path
    workflow_finished = asyncio.Future()

    async def callback(msg):
        workflow_finished.set_result(True)
        await msg.ack()

    async with NATSSession() as nats_session:
        file_watcher_classes = [
            DotsampleFileWatcher,
            FASTQFileWatcher,
            TaqmanFileWatcher,
            DotanalysisFileWatcher,
        ]

        for file_watcher_class in file_watcher_classes:
            file_watcher_class.path = file_watch_dir

        # Start Maestro
        task = await run_workflow_steps(
            message_watcher_classes=[
                LastTestStepInWorkflow,
                SeqDataMaker,
                Basepipe,
                Triopipe,
                Annopipe,
            ],
            artefact_classes=None,
            file_watcher_classes=file_watcher_classes,
        )

        await nats_session.subscribe(
            Config.STREAM,
            "workflow_step.LastTestStepInWorkflow.done",
            callback,
            config={"deliver_policy": "new"},
        )

        # Order a job for LastTestStepInWorkflow
        job = Job(
            workflow_step="LastTestStepInWorkflow",
            inputs={"input": ("AnnopipeOutput", {"seqdata_id": SEQDATA_ID})},
            params={},
        )
        workflow = WorkflowModel(
            workflow_name="OUSAMG_wgs356_HS14386342_HBOC_v1.0.0",
            jobs=[job],
        )
        await order_workflow(workflow)

        await asyncio.sleep(0.1)
        # Trigger file watchers at start of Steps workflow
        shutil.copytree(
            DATA_DIR / "samples" / f"OUSAMG_wgs356_HS14386342_{SEQDATA_ID}",
            file_watch_dir / f"OUSAMG_wgs356_HS14386342_{SEQDATA_ID}",
            copy_function=shutil.copy,
        )
        shutil.copytree(
            DATA_DIR
            / "analyses-work"
            / "OUSAMG_wgs356_HS14386342_02bea762-f251-4f31-ac7c-82a43d7850f7",
            file_watch_dir / "OUSAMG_wgs356_HS14386342_02bea762-f251-4f31-ac7c-82a43d7850f7",
            copy_function=shutil.copy,
        )
        shutil.copytree(
            DATA_DIR
            / "analyses-work"
            / "OUSAMG_wgs356_HS14386342_HBOC_v1.0.0_e6fb6dc2-3404-437e-89d8-30959e51c67f",
            file_watch_dir
            / "OUSAMG_wgs356_HS14386342_HBOC_v1.0.0_e6fb6dc2-3404-437e-89d8-30959e51c67f",
            copy_function=shutil.copy,
        )
        # Await future, pausing execution until the workflow has finished.
        await asyncio.wait_for(workflow_finished, timeout=120.0)

        # Check that the LastTestStepOutput artefact is in the DB.
        # Note: read_node returns None if object not found.
        async with Neo4jTransaction() as neo4j_transaction:
            db_artefact = await read_node(
                neo4j_transaction,
                LastTestStepOutput,
                {"seqdata_id": SEQDATA_ID},
            )
        assert db_artefact is not None
        assert db_artefact.seqdata_id == SEQDATA_ID

        # Cancel the Maestro task to clean up.
        with pytest.raises(asyncio.CancelledError):
            task.cancel()
            await asyncio.wait_for(task, timeout=2.0)

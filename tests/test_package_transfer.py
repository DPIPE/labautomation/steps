import asyncio
import multiprocessing
import multiprocessing.synchronize
import tempfile
from contextlib import ExitStack
from multiprocessing.context import ForkProcess
from pathlib import Path
from typing import override
from unittest.mock import Mock, patch
from uuid import uuid4

import pytest
import pytest_asyncio
from artefacts.base import StepsBaseArtefact, StepsBaseFileArtefact
from artefacts.package import PackageArtefact
from maestro import (
    BaseArtefactModel,
    FileArtefactModel,
    FileWatcher,
    Job,
    MessageWatcher,
    WorkflowModel,
    order_workflow,
    run_workflow_steps,
)
from maestro.config import Config as MaestroConfig
from maestro.models import JobStatus, WorkflowStepModelWithArtefacts
from maestro.nats.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import fetch_node_by_rel, read_nodes
from maestro.utils import subscribe_to_job_update
from maestro.workflow_utils import resume_halted_job
from pydantic import UUID4, BaseModel
from pydantic.fields import FieldInfo
from workflowsteps.packagestager import PackageStager
from workflowsteps.packagetransferrer import PackageTransferrer
from workflowsteps.packageunpacker import PackageUnpacker
from workflowsteps.utils import package

from config import PackageUnpackingConfig, config

SEQDATA_ID = uuid4()
QC_STEP_MAESTRO_ID = uuid4()
SANITY_CHECK_MAESTRO_ID = uuid4()
FINAL_STEP_MAESTRO_ID = uuid4()

# Set up instance directories and package configs
tmpdir_local = tempfile.TemporaryDirectory()
STORAGE_LOCAL = Path(tmpdir_local.name)
LOCAL_INSTANCE = "local"

tmpdir_transfer = tempfile.TemporaryDirectory()
STORAGE_TRANSFER = Path(tmpdir_transfer.name)
TRANSFER_INSTANCE = "transfer"

tmpdir_remote = tempfile.TemporaryDirectory()
STORAGE_REMOTE = Path(tmpdir_remote.name)
REMOTE_INSTANCE = "remote"
ARTEFACT_STORAGE = STORAGE_REMOTE / "artefacts"
ARTEFACT_STORAGE.mkdir()
ARTEFACT_STORAGE_C = ARTEFACT_STORAGE / "C"
ARTEFACT_STORAGE_C.mkdir()
FILE_STORAGE = STORAGE_REMOTE / "files"
FILE_STORAGE.mkdir()

IN_TRANSFER_INSTANCE = "in_transfer"

# Instance-specific configurations for package handling
CONFIG = config.model_copy()
CONFIG.package.staging = config.package.staging.model_copy(
    update={"transfer_dir": STORAGE_TRANSFER, "instance": LOCAL_INSTANCE}
)
CONFIG.package.transfer = config.package.transfer.model_copy(
    update={
        "bucket": STORAGE_REMOTE.name,
        "filelock": STORAGE_REMOTE,
        "instance": TRANSFER_INSTANCE,
        "transfer_dir": STORAGE_TRANSFER,
    }
)
CONFIG.package.unpacking = config.package.unpacking.model_copy(
    update={
        "file_routings": [
            PackageUnpackingConfig.FileRouting(
                **{
                    "glob": "*.artefact*",
                    "target_dir": ARTEFACT_STORAGE,
                }
            ),
            PackageUnpackingConfig.FileRouting(
                **{
                    "glob": "*.file*",
                    "target_dir": FILE_STORAGE,
                }
            ),
        ],
        "instance": REMOTE_INSTANCE,
        "filelock_instance": IN_TRANSFER_INSTANCE,
    }
)
CONFIG.package.run_transfer = True


class DummyQCArtefact(StepsBaseArtefact):
    seqdata_id: UUID4


class DummyQCPass(StepsBaseArtefact):
    seqdata_id: UUID4


class SanityCheckPass(StepsBaseArtefact):
    seqdata_id: UUID4


class DummyFileArtefact(StepsBaseFileArtefact):
    seqdata_id: UUID4


class DummyFileArtefactStart(DummyFileArtefact):  # fmt: skip
    ...


class DummyFileArtefactA(DummyFileArtefact):  # fmt: skip
    ...


class DummyFileArtefactB(DummyFileArtefact):  # fmt: skip
    ...


class DummyFileArtefactC(DummyFileArtefact):  # fmt: skip
    ...


class DummyFileArtefactD(DummyFileArtefact):  # fmt: skip
    ...


class DummyFileWatcher(FileWatcher):
    path = STORAGE_LOCAL
    pattern = r".*\.start"

    @override
    async def process(self, input_artefact: FileArtefactModel) -> list[DummyFileArtefactStart]:
        start_artefact = DummyFileArtefactStart(path=input_artefact.path, seqdata_id=SEQDATA_ID)

        # Order workflow jobs
        maestro_id = uuid4()
        workflow = WorkflowModel(
            maestro_id=maestro_id,
            workflow_name="transfer_workflow",
            jobs=[
                Job(  # first local step
                    workflow_step=DummyStepA,
                    inputs={
                        "input_artefact": (
                            DummyFileArtefactStart,
                            {"instance_id": LOCAL_INSTANCE, "seqdata_id": SEQDATA_ID},
                        )
                    },
                    params={"workflow_maestro_id": maestro_id},
                ),
                Job(  # QC step
                    maestro_id=QC_STEP_MAESTRO_ID,
                    workflow_step=DummyQCStep,
                    inputs={
                        "input_artefact": (
                            DummyQCArtefact,
                            {"seqdata_id": SEQDATA_ID},
                        ),
                    },
                ),
                Job(
                    maestro_id=SANITY_CHECK_MAESTRO_ID,
                    workflow_step=DummySanityCheckStep,
                    inputs={
                        "input_artefact": (
                            DummyFileArtefactA,
                            {"instance_id": LOCAL_INSTANCE, "seqdata_id": SEQDATA_ID},
                        )
                    },
                ),
                Job(  # second local step
                    workflow_step=DummyStepB,
                    inputs={
                        "input_artefact": (
                            DummyFileArtefactB,
                            {"instance_id": LOCAL_INSTANCE, "seqdata_id": SEQDATA_ID},
                        ),
                    },
                    params={"workflow_maestro_id": maestro_id},
                ),
                Job(  # remote step
                    maestro_id=FINAL_STEP_MAESTRO_ID,
                    workflow_step=DummyStepC,
                    instance_id=REMOTE_INSTANCE,  # override default LOCAL_INSTANCE
                    inputs={
                        "input_artefact_primary": (
                            DummyFileArtefactA,
                            {"instance_id": REMOTE_INSTANCE, "seqdata_id": SEQDATA_ID},
                        ),
                        "input_artefact_secondary": (
                            DummyFileArtefactC,
                            {"instance_id": REMOTE_INSTANCE, "seqdata_id": SEQDATA_ID},
                        ),
                    },
                    params={"workflow_maestro_id": maestro_id},
                ),
            ],
        )

        await order_workflow(workflow)

        return [start_artefact]


class DummyStepA(MessageWatcher):
    """Set output directory and workflow name in process() method, use default file globbing patterns."""

    output_dir = STORAGE_LOCAL / "DummyStepA"

    @override
    @package(
        job_output_dir=lambda self, *args, **kwargs: self.job_output_dir,
        workflow_maestro_id=lambda self, *args, **kwargs: self.workflow_maestro_id,
        delayers=[
            (
                DummyQCPass,
                {"seqdata_id": lambda self, *args, **kwargs: self.seqdata_id},
            ),
            (
                SanityCheckPass,
                {
                    "seqdata_id": lambda self, *args, **kwargs: self.seqdata_id,
                },
            ),
        ],
    )
    async def process(
        self, input_artefact: DummyFileArtefactStart, workflow_maestro_id: UUID4
    ) -> list[BaseArtefactModel]:
        self.workflow_maestro_id = workflow_maestro_id
        self.seqdata_id = input_artefact.seqdata_id

        # Simulate process
        self.job_output_dir = self.output_dir
        self.job_output_dir.mkdir()
        artefact_a_path = self.job_output_dir / "FileArtefactA.artefact.gz"
        artefact_a_path.touch()
        artefact_a_path.write_text(f"Dummy content for {artefact_a_path}")
        artefact_b_path = self.job_output_dir / "FileArtefactB.artefact"
        artefact_b_path.touch()
        artefact_b_path.write_text(f"Dummy content for {artefact_b_path}")

        # Add other files
        (self.job_output_dir / "DummyStepA.file").touch()
        (self.job_output_dir / "DummyStepA2.file").touch()

        artefact_a = DummyFileArtefactA(path=artefact_a_path, seqdata_id=self.seqdata_id)
        artefact_b = DummyFileArtefactB(path=artefact_b_path, seqdata_id=self.seqdata_id)
        qc_artefact = DummyQCArtefact(seqdata_id=self.seqdata_id)

        return [artefact_a, artefact_b, qc_artefact]


class DummyQCStep(MessageWatcher):
    """Dummy QC step which produces a DummyQCPass used as delayer for package transfer
    of DummyStepA's output artefacts and files."""

    @override
    async def process(self, input_artefact: DummyQCArtefact) -> list[DummyQCPass]:
        # Halt workflow to simulate manual inspection of QC results
        await self.halt()

        qc_artefact = DummyQCPass(seqdata_id=input_artefact.seqdata_id)

        return [qc_artefact]


class DummySanityCheckStep(MessageWatcher):
    """Dummy sanity check step which produces a SanityCheckPass used as delayer for package transfer
    of DummyStepA's output artefacts and files."""

    @override
    async def process(self, input_artefact: DummyFileArtefactA) -> list[SanityCheckPass]:
        output = SanityCheckPass(seqdata_id=input_artefact.seqdata_id)

        return [output]


class DummyStepB(MessageWatcher):
    """Specify output directory, workflow name and file globbing patterns in @package()."""

    output_dir = STORAGE_LOCAL / "DummyStepB"

    @override
    @package(
        job_output_dir=output_dir,
        workflow_maestro_id=lambda self, *args, **kwargs: self.workflow_maestro_id,
        routings=[
            ("*.artefact*", ARTEFACT_STORAGE_C),
        ],
    )
    async def process(
        self, input_artefact: DummyFileArtefactB, workflow_maestro_id: UUID4
    ) -> list[DummyFileArtefactC]:
        # Simulate process
        self.workflow_maestro_id = workflow_maestro_id
        self.output_dir.mkdir()
        artefact_c_path = self.output_dir / "FileArtefactC.artefact.tar"
        artefact_c_path.touch()
        artefact_c_path.write_text(f"Dummy content for {artefact_c_path}")

        artefact_c = DummyFileArtefactC(path=artefact_c_path, seqdata_id=input_artefact.seqdata_id)

        return [artefact_c]


class DummyStepC(MessageWatcher):
    """Scheduled on REMOTE_INSTANCE, so no package transfer is triggered."""

    @override
    @package(
        job_output_dir=lambda self, *args, **kwargs: self.job_output_dir,
        workflow_maestro_id=lambda self, *args, **kwargs: self.workflow_maestro_id,
    )
    async def process(
        self,
        input_artefact_primary: DummyFileArtefactA,
        input_artefact_secondary: DummyFileArtefactC,
        workflow_maestro_id: UUID4,
    ) -> list[DummyFileArtefactD]:
        # Simulate process
        self.workflow_maestro_id = workflow_maestro_id
        self.job_output_dir = STORAGE_REMOTE / "DummyStepC"
        self.job_output_dir.mkdir()
        artefact_path = self.job_output_dir / "FileArtefactD.artefact"
        artefact_path.touch()

        artefact_d = DummyFileArtefactD(
            path=artefact_path, seqdata_id=input_artefact_secondary.seqdata_id
        )

        return [artefact_d]


async def run_workflow_steps_in_subprocess(
    message_watcher_classes: list[type[MessageWatcher]],
    file_watcher_classes: list[type[FileWatcher]] | None,
    instance_id: str,
):
    """Wrapper to run workflow steps in a separate process, with updated config values
    for instance_id."""

    async def main(
        message_watcher_classes: list[type[MessageWatcher]],
        file_watcher_classes: list[type[FileWatcher]] | None,
        started_flag: multiprocessing.synchronize.Event,
    ):
        task = await run_workflow_steps(
            message_watcher_classes=message_watcher_classes,
            file_watcher_classes=file_watcher_classes,
        )
        # Need to give it some time, to ensure that file watchers are started
        await asyncio.sleep(0)
        started_flag.set()
        await task

    def start(
        message_watcher_classes: list[type[MessageWatcher]],
        file_watcher_classes: list[type[FileWatcher]] | None,
        instance_id: str,
        started_flag: multiprocessing.synchronize.Event,
    ):
        """Start workflow steps in a subprocess."""
        MaestroConfig.INSTANCE_ID = instance_id

        # Update instance id default value. As this is inherited from the parent process,
        # the default value has already been set
        models: list[type[BaseModel]] = [
            Job,
            DummyFileArtefactStart,
            DummyFileArtefactA,
            DummyFileArtefactB,
            DummyFileArtefactC,
            DummyFileArtefactD,
            PackageArtefact,
        ]
        for model in models:
            model.model_fields["instance_id"] = FieldInfo.merge_field_infos(
                model.model_fields["instance_id"], FieldInfo(default=MaestroConfig.INSTANCE_ID)
            )
            model.model_rebuild(force=True)
            assert (
                model.model_fields["instance_id"].default == MaestroConfig.INSTANCE_ID != "default"
            )

        asyncio.run(main(message_watcher_classes, file_watcher_classes, started_flag))

    with ExitStack() as stack:
        # Mock instance id and config
        stack.enter_context(patch("workflowsteps.utils.MaestroConfig.INSTANCE_ID", new=instance_id))
        stack.enter_context(patch("config.config", new=CONFIG))

        # Mock S3cmd command
        def mock_s3_command(source_dir: Path) -> list[str]:
            return [
                "rsync",
                "-av",
                f"{source_dir.as_posix()}/",
                CONFIG.package.transfer.filelock.as_posix(),
            ]

        mock_build_s3_command = stack.enter_context(
            patch("workflowsteps.packagetransferrer.PackageTransferrer.build_s3_command")
        )
        mock_build_s3_command.side_effect = mock_s3_command

        started_flag = multiprocessing.Event()
        process = multiprocessing.get_context("fork").Process(
            target=start,
            kwargs={
                "message_watcher_classes": message_watcher_classes,
                "file_watcher_classes": file_watcher_classes,
                "instance_id": instance_id,
                "started_flag": started_flag,
            },
            name=f"Maestro Instance '{instance_id}'",
        )
        print(f"Starting instance '{instance_id}'")
        process.start()

        # Ensure that the process has started within 5 seconds
        async with asyncio.timeout(5):
            while True:
                if started_flag.is_set():
                    break
                await asyncio.sleep(0.1)
        assert process.is_alive()
        print(f"Instance '{instance_id}' started (PID: {process.pid})")
        return process


@pytest_asyncio.fixture()
async def local_run_workflow_steps():
    """
    Start up a process to run workflow steps with INSTANCE_ID set to LOCAL_INSTANCE.

    Runs one file watcher (DummyFileWatcher) and three message watchers (DummyStepA, DummyStepB,
    and PackageStager).
    """
    process = await run_workflow_steps_in_subprocess(
        message_watcher_classes=[
            DummyStepA,
            DummyQCStep,
            DummySanityCheckStep,
            DummyStepB,
            PackageStager,
        ],
        file_watcher_classes=[DummyFileWatcher],
        instance_id=LOCAL_INSTANCE,
    )
    yield process

    process.terminate()
    process.join()


@pytest_asyncio.fixture()
async def transfer_run_workflow_steps():
    """
    Start up a process to run workflow steps with INSTANCE_ID set to TRANSFER_INSTANCE.

    Runs one message watcher (PackageTransferrer).
    """
    process = await run_workflow_steps_in_subprocess(
        message_watcher_classes=[PackageTransferrer],
        file_watcher_classes=[],
        instance_id=TRANSFER_INSTANCE,
    )
    yield process

    process.terminate()
    process.join()


@pytest_asyncio.fixture()
async def remote_run_workflow_steps():
    """
    Start up a process to run workflow steps with INSTANCE_ID set to REMOTE_INSTANCE.

    Runs two message watchers (PackageUnpacker and DummyStepC).
    """
    process = await run_workflow_steps_in_subprocess(
        message_watcher_classes=[PackageUnpacker, DummyStepC],
        file_watcher_classes=[],
        instance_id=REMOTE_INSTANCE,
    )
    yield process

    process.terminate()
    process.join()


@pytest.mark.asyncio
async def test_package_transfer(
    local_run_workflow_steps: ForkProcess,
    transfer_run_workflow_steps: ForkProcess,
    remote_run_workflow_steps: ForkProcess,
    truncate_database,
):
    halted_future = asyncio.Future()
    sanity_check_future = asyncio.Future()
    completed_future = asyncio.Future()

    async with NATSSession() as nats_session:
        await subscribe_to_job_update(
            nats_session, QC_STEP_MAESTRO_ID, JobStatus.HALTED, halted_future
        )
        await subscribe_to_job_update(
            nats_session, SANITY_CHECK_MAESTRO_ID, JobStatus.COMPLETED, sanity_check_future
        )
        await subscribe_to_job_update(
            nats_session, FINAL_STEP_MAESTRO_ID, JobStatus.COMPLETED, completed_future
        )

        # Trigger the start of the workflow
        Path(STORAGE_LOCAL / "analysis.start").touch()

        # Wait for the workflow to halt and sanity check to complete
        await asyncio.wait_for(asyncio.gather(*[halted_future, sanity_check_future]), 10)

        # Wait for package staging of DummyStepB's output to complete
        async with asyncio.timeout(10):
            while True:
                async with Neo4jTransaction() as neo4j_tx:
                    # Package artefact for DummyStepA should exist
                    workflow_steps = await read_nodes(neo4j_tx, WorkflowStepModelWithArtefacts, {})
                    dummy_step_a = next(
                        step for step in workflow_steps if DummyStepA.__name__ in step.labels
                    )
                    package_artefact = await fetch_node_by_rel(
                        neo4j_tx,
                        WorkflowStepModelWithArtefacts,
                        {"maestro_id": dummy_step_a.maestro_id},
                        PackageArtefact,
                    )
                    assert package_artefact is not None, "Expected package artefact for DummyStepA"

                    # But it should not be staged for transfer
                    package_stager_steps = [
                        step for step in workflow_steps if PackageStager.__name__ in step.labels
                    ]

                    if package_stager_steps:
                        for step in package_stager_steps:
                            assert (
                                package_artefact not in step.INPUT
                            ), "Expected no PackageStager step for DummyStepA's package artefact"
                        break

                await asyncio.sleep(0.1)

        # Unhalt QC step
        await resume_halted_job(QC_STEP_MAESTRO_ID)

        # Wait for the workflow to finish
        await asyncio.wait_for(completed_future, 10)

    async with Neo4jTransaction() as neo4j_tx:
        # Check that jobs were run and completed on the correct instance
        local_jobs = await read_nodes(neo4j_tx, Job, {"instance_id": LOCAL_INSTANCE})
        transfer_jobs = await read_nodes(neo4j_tx, Job, {"instance_id": TRANSFER_INSTANCE})
        remote_jobs = await read_nodes(neo4j_tx, Job, {"instance_id": REMOTE_INSTANCE})
        assert len(local_jobs) == 7, "Expected 7 local jobs"
        assert len(transfer_jobs) == 2, "Expected 2 transfer jobs"
        assert len(remote_jobs) == 3, "Expected 3 remote jobs"
        assert all(job.status == JobStatus.COMPLETED for job in local_jobs + remote_jobs)

        # Check workflow steps
        workflow_steps = await read_nodes(neo4j_tx, WorkflowStepModelWithArtefacts, {})
        assert len(workflow_steps) == len(local_jobs) + len(transfer_jobs) + len(
            remote_jobs
        ), "Expected number of workflow steps to match total number of jobs"

        # Check file watcher step
        file_watcher_step = next(
            step for step in workflow_steps if DummyFileWatcher.__name__ in step.labels
        )
        assert len(file_watcher_step.OUTPUT) == 1, "File watcher should have 1 output artefact"
        assert file_watcher_step.OUTPUT[0].model_dump(include={"path", "instance_id"}) == {
            "path": STORAGE_LOCAL / "analysis.start",
            "instance_id": LOCAL_INSTANCE,
        }

        # Check DummyStepA
        dummy_step_a = next(step for step in workflow_steps if DummyStepA.__name__ in step.labels)

        assert len(dummy_step_a.INPUT) == 1, "DummyStepA should have 1 input artefact"
        assert dummy_step_a.INPUT[0].model_dump(include={"path", "instance_id"}) == {
            "path": STORAGE_LOCAL / "analysis.start",
            "instance_id": LOCAL_INSTANCE,
        }
        assert (
            len(dummy_step_a.OUTPUT) == 4
        ), "DummyStepA should have 4 output artefacts, 2 dummy file artefacts, 1 dummy qc artefact and 1 package artefact"

        dummy_artefact_a = next(
            output_artefact
            for output_artefact in dummy_step_a.OUTPUT
            if isinstance(output_artefact, DummyFileArtefactA)
        )
        assert dummy_artefact_a.model_dump(include={"path", "instance_id"}) == {
            "path": STORAGE_LOCAL / "DummyStepA/FileArtefactA.artefact.gz",
            "instance_id": LOCAL_INSTANCE,
        }

        dummy_artefact_b = next(
            output_artefact
            for output_artefact in dummy_step_a.OUTPUT
            if isinstance(output_artefact, DummyFileArtefactB)
        )
        assert dummy_artefact_b.model_dump(include={"path", "instance_id"}) == {
            "path": STORAGE_LOCAL / "DummyStepA/FileArtefactB.artefact",
            "instance_id": LOCAL_INSTANCE,
        }

        package_artefact = next(
            output_artefact
            for output_artefact in dummy_step_a.OUTPUT
            if isinstance(output_artefact, PackageArtefact)
        )
        assert (
            package_artefact.file_artefacts
            == {
                dummy_artefact_a,
                dummy_artefact_b,
            }
        ), f"Expected {dummy_artefact_a} and {dummy_artefact_b} as file artefact of package artefact"
        assert (
            len(package_artefact.extra_artefacts) == 2
        ), "Expected 2 extra file artefacts in package"

        # Check package stager step for DummyStepA package artefact
        package_stager_step = next(
            step
            for step in workflow_steps
            if PackageStager.__name__ in step.labels and package_artefact in step.INPUT
        )
        assert len(package_stager_step.INPUT) == 3, "Package stager should have 3 input artefact"
        assert len(package_stager_step.OUTPUT) == 1, "Package stager should have 1 output artefact"
        assert isinstance(package_stager_step.OUTPUT[0], PackageArtefact)
        output_package_artefact = package_stager_step.OUTPUT[0]
        assert all(
            file_artefact.instance_id == TRANSFER_INSTANCE
            for file_artefact in output_package_artefact.file_artefacts
        ), f"Expected file artefacts staged for transfer to have instance_id set to {TRANSFER_INSTANCE}"

        # Check DummyStepB
        dummy_step_b = next(step for step in workflow_steps if DummyStepB.__name__ in step.labels)
        assert len(dummy_step_b.INPUT) == 1, "DummyStepB should have 1 input artefact"
        assert (
            dummy_step_b.INPUT[0] == dummy_artefact_b
        ), f"Expected input artefact to be {dummy_artefact_b}"
        assert (
            len(dummy_step_b.OUTPUT) == 2
        ), "DummyStepB should have 2 output artefacts, 1 dummy file artefact and 1 package artefact"

        dummy_artefact_c = next(
            output_artefact
            for output_artefact in dummy_step_b.OUTPUT
            if isinstance(output_artefact, DummyFileArtefactC)
        )
        assert dummy_artefact_c.model_dump(include={"path", "instance_id"}) == {
            "path": STORAGE_LOCAL / "DummyStepB/FileArtefactC.artefact.tar",
            "instance_id": LOCAL_INSTANCE,
        }

        package_artefact = next(
            output_artefact
            for output_artefact in dummy_step_b.OUTPUT
            if isinstance(output_artefact, PackageArtefact)
        )
        assert package_artefact.file_artefacts == {
            dummy_artefact_c
        }, f"Expected {dummy_artefact_c} as file artefact of package artefact"
        assert (
            len(package_artefact.extra_artefacts) == 0
        ), "Expected 0 extra file artefact in package"

        # Check package stager step for DummyStepB package artefact
        package_stager_step = next(
            step
            for step in workflow_steps
            if PackageStager.__name__ in step.labels and package_artefact in step.INPUT
        )
        assert len(package_stager_step.INPUT) == 1, "Package stager should have 1 input artefact"
        assert len(package_stager_step.OUTPUT) == 1, "Package stager should have 1 output artefact"
        assert isinstance(package_stager_step.OUTPUT[0], PackageArtefact)
        output_package_artefact = package_stager_step.OUTPUT[0]
        assert all(
            file_artefact.instance_id == TRANSFER_INSTANCE
            for file_artefact in output_package_artefact.file_artefacts
        ), f"Expected file artefacts staged for transfer to have instance_id set to {TRANSFER_INSTANCE}"

        # Check DummyStepC
        dummy_step_c = next(step for step in workflow_steps if DummyStepC.__name__ in step.labels)
        assert len(dummy_step_c.INPUT) == 2, "DummyStepC should have 2 input artefacts"
        for input_artefact in dummy_step_c.INPUT:
            assert isinstance(input_artefact, DummyFileArtefactA | DummyFileArtefactC)
            assert input_artefact.instance_id == REMOTE_INSTANCE

        assert len(dummy_step_c.OUTPUT) == 1, "DummyStepC should have 1 output artefact"
        dummy_artefact_d = dummy_step_c.OUTPUT[0]
        assert isinstance(dummy_artefact_d, DummyFileArtefactD)

        # Ensure that no package artefact are created for DummyStepC
        non_existing_package_artefact = await fetch_node_by_rel(
            neo4j_tx,
            DummyFileArtefactD,
            {"maestro_id": dummy_artefact_d.maestro_id},
            PackageArtefact,
        )
        assert non_existing_package_artefact is None, "Expected no package artefact for DummyStepC"

        # Ensure contents of local and remote file artefacts are the same
        remote_dummy_artefact_a = await read_nodes(
            neo4j_tx, DummyFileArtefactA, {"instance_id": REMOTE_INSTANCE}
        )
        assert len(remote_dummy_artefact_a) == 1, "Expected 1 remote DummyFileArtefactA"
        assert dummy_artefact_a.path.read_text() == remote_dummy_artefact_a[0].path.read_text()

        remote_dummy_artefact_b = await read_nodes(
            neo4j_tx, DummyFileArtefactB, {"instance_id": REMOTE_INSTANCE}
        )
        assert len(remote_dummy_artefact_b) == 1, "Expected 1 remote DummyFileArtefactB"
        assert dummy_artefact_b.path.read_text() == remote_dummy_artefact_b[0].path.read_text()

        remote_dummy_artefact_c = await read_nodes(
            neo4j_tx, DummyFileArtefactC, {"instance_id": REMOTE_INSTANCE}
        )
        assert len(remote_dummy_artefact_c) == 1, "Expected 1 remote DummyFileArtefactC"
        assert dummy_artefact_c.path.read_text() == remote_dummy_artefact_c[0].path.read_text()

        # Ensure that all file artefacts are routed correctly
        assert (
            remote_dummy_artefact_a[0].path.parent == ARTEFACT_STORAGE
        ), f"Expected artefact to be routed to {ARTEFACT_STORAGE}"

        assert (
            remote_dummy_artefact_b[0].path.parent == ARTEFACT_STORAGE
        ), f"Expected artefact to be routed to {ARTEFACT_STORAGE}"

        assert (
            remote_dummy_artefact_c[0].path.parent == ARTEFACT_STORAGE_C
        ), f"Expected artefact to be routed to {ARTEFACT_STORAGE_C}"

        remote_file_artefacts = await read_nodes(
            neo4j_tx, StepsBaseFileArtefact, {"instance_id": REMOTE_INSTANCE}
        )
        for artefact in remote_file_artefacts:
            if artefact.path.suffix == ".file":
                assert (
                    artefact.path.parent == FILE_STORAGE
                ), f"Expected all .file files to be routed to {FILE_STORAGE}"

        # Ensure that all file artefacts on local and remote instances are still valid
        file_artefacts: set[DummyFileArtefact] = {
            dummy_artefact_a,
            dummy_artefact_b,
            dummy_artefact_c,
            remote_dummy_artefact_a[0],
            remote_dummy_artefact_b[0],
            remote_dummy_artefact_c[0],
        }
        assert all(file_artefact.invalidated_at is None for file_artefact in file_artefacts)

        # Ensure that all file artefacts in transfer are invalidated
        transfer_file_artefacts = await read_nodes(
            neo4j_tx, StepsBaseFileArtefact, {"instance_id": TRANSFER_INSTANCE}
        )
        unpacked_file_artefacts = await read_nodes(
            neo4j_tx, StepsBaseFileArtefact, {"instance_id": IN_TRANSFER_INSTANCE}
        )
        assert all(
            file_artefact.invalidated_at is not None
            for file_artefact in transfer_file_artefacts + unpacked_file_artefacts
        )

        # Ensure that all files in transfer staging and package unpacking directories are deleted
        assert all(
            not file_artefact.path.exists()
            for file_artefact in transfer_file_artefacts + unpacked_file_artefacts
        )


@pytest.mark.asyncio
@patch("workflowsteps.utils.MaestroConfig.INSTANCE_ID", new=LOCAL_INSTANCE)
async def test_package_decorator_no_output_artefact(tmp_path: Path):
    """Ensure that a ValueError is raised if a @package decorated process() method does not return
    any output artefacts."""

    workflow_maestro_id = uuid4()

    class NoOutputStep(MessageWatcher):
        @override
        @package(
            job_output_dir=lambda self, *args, **kwargs: self.job_output_dir,
            workflow_maestro_id=workflow_maestro_id,
        )
        async def process(self) -> list[DummyFileArtefact]:
            self.job_output_dir = tmp_path / "NoOutputStep"
            self.job_output_dir.mkdir()
            return []

    with pytest.raises(ValueError):
        await NoOutputStep().process()


@pytest.mark.asyncio
@patch("workflowsteps.utils.MaestroConfig.INSTANCE_ID", new=LOCAL_INSTANCE)
async def test_files_in_job_output_dir(tmp_path: Path):
    """Ensure that a ValueError is raised if any file is not in the job_output_dir."""

    workflow_maestro_id = uuid4()

    class FilesInJobOutputDir(MessageWatcher):
        @override
        @package(
            job_output_dir=lambda self, *args, **kwargs: self.job_output_dir,
            workflow_maestro_id=workflow_maestro_id,
        )
        async def process(self) -> list[DummyFileArtefact]:
            self.job_output_dir = tmp_path / "FilesInJobOutputDir"
            self.job_output_dir.mkdir()

            # File artefact in job_output_dir's parent directory
            artefact = DummyFileArtefact(
                seqdata_id=SEQDATA_ID, path=self.job_output_dir.parent / "artefact"
            )

            (self.job_output_dir / "file1").touch()

            return [artefact]

    with pytest.raises(ValueError):
        await FilesInJobOutputDir().process()


@pytest.mark.asyncio
async def test_package_decorator_invalid_method():
    """Ensure that a TypeError is raised if @package() is added to an invalid class method."""

    workflow_maestro_id = uuid4()

    class InvalidMethodClass(MessageWatcher):
        @override
        async def process(self, input_artefact: DummyFileArtefactStart) -> list[DummyFileArtefact]:
            return []

        @package(job_output_dir=Path("dummy"), workflow_maestro_id=workflow_maestro_id)
        async def invalid_method(
            self, input_artefact: DummyFileArtefactStart
        ) -> list[DummyFileArtefact]:
            return []

    class InvalidClass:
        @package(job_output_dir=Path("dummy"), workflow_maestro_id=workflow_maestro_id)
        async def process(self, input_artefact: DummyFileArtefactStart) -> list[DummyFileArtefact]:
            return []

    input_artefact = DummyFileArtefactStart(seqdata_id=SEQDATA_ID, path=Path("dummy.start"))

    with pytest.raises(TypeError):
        (
            await InvalidMethodClass().invalid_method(input_artefact),
            "Expected TypeError for invalid method",
        )

    with pytest.raises(TypeError):
        await InvalidClass().process(input_artefact), "Expected TypeError for invalid class"


@pytest.mark.asyncio
@patch("workflowsteps.utils.MaestroConfig.INSTANCE_ID", new=LOCAL_INSTANCE)
async def test_error_on_unrouted_files(tmp_path: Path):
    """Ensure that a ValueError is raised if a file to be packaged does not match any file routing."""

    workflow_maestro_id = uuid4()

    class UnroutedFileStep(MessageWatcher):
        @override
        @package(
            job_output_dir=lambda self, *args, **kwargs: self.job_output_dir,
            workflow_maestro_id=workflow_maestro_id,
        )
        async def process(self, input_artefact: DummyFileArtefactStart) -> list[DummyFileArtefact]:
            self.job_output_dir = tmp_path / "UnroutedFileStep"
            self.job_output_dir.mkdir()
            artefact = DummyFileArtefact(
                seqdata_id=input_artefact.seqdata_id, path=self.job_output_dir / "dummy.artefact"
            )
            file = self.job_output_dir / "unrouted.file"
            file.touch()

            return [artefact]

    target_dir = tmp_path / "artefacts"

    new_config = CONFIG.model_copy(deep=True)
    new_config.package.unpacking.file_routings = [
        PackageUnpackingConfig.FileRouting.model_validate(
            {
                "glob": "*.artefact*",
                "target_dir": target_dir,
            }
        )
    ]  # missing default file routing for .file files

    input_artefact = DummyFileArtefactStart(seqdata_id=SEQDATA_ID, path=Path("dummy.start"))

    with patch("workflowsteps.utils.config", new=new_config):
        with pytest.raises(ValueError):
            await UnroutedFileStep().process(input_artefact)


@pytest.mark.asyncio
@patch("workflowsteps.utils.MaestroConfig.INSTANCE_ID", new=LOCAL_INSTANCE)
async def test_invalid_job_output_dir():
    """Ensure that an error is raised if job_output_dir is not a Path or is a non-existent directory."""

    workflow_maestro_id = uuid4()

    class NonExistentJobOutputDir(MessageWatcher):
        @override
        @package(
            job_output_dir=lambda self, *args, **kwargs: self.job_output_dir,
            workflow_maestro_id=workflow_maestro_id,
        )
        async def process(self, input_artefact: DummyFileArtefactStart) -> list[DummyFileArtefact]:
            self.job_output_dir = Path("non_existent")  # non-existent Path
            return []

    class NotPathJobOutPutDir(MessageWatcher):
        @override
        @package(
            job_output_dir=lambda self, *args, **kwargs: self.job_output_dir,
            workflow_maestro_id=workflow_maestro_id,
        )
        async def process(self, input_artefact: DummyFileArtefactStart) -> list[DummyFileArtefact]:
            self.job_output_dir = "non_existent"  # not a Path
            return []

    input_artefact = DummyFileArtefactStart(seqdata_id=SEQDATA_ID, path=Path("dummy.start"))

    with pytest.raises(AttributeError):
        await NonExistentJobOutputDir().process(input_artefact)

    with pytest.raises(AssertionError):
        await NotPathJobOutPutDir().process(input_artefact)


def test_get_short_lived_token_key_error():
    """Ensure that a RuntimeError is raised if token is missing in the response."""
    with patch("requests.post") as mock_post:
        mock_response = Mock()
        mock_response.raise_for_status.return_value = None
        mock_response.json.return_value = {}
        mock_post.return_value = mock_response

        transferrer = PackageTransferrer()
        with pytest.raises(RuntimeError, match="Failed to retrieve short-lived token"):
            transferrer.get_short_lived_token()


@pytest.mark.asyncio
@patch("workflowsteps.utils.MaestroConfig.INSTANCE_ID", new=LOCAL_INSTANCE)
async def test_existing_workflow(tmp_path: Path):
    """Ensure that workflow_name must point to an existing workflow."""

    class DummyStep(MessageWatcher):
        @override
        @package(
            job_output_dir=lambda self, *args, **kwargs: self.job_output_dir,
            workflow_maestro_id=UUID4(
                "4f84870b-d119-48ed-9047-88ccb0148aea"
            ),  # non-existent workflow maestro id
        )
        async def process(self, input_artefact: DummyFileArtefactStart) -> list[DummyFileArtefact]:
            self.job_output_dir = tmp_path / "DummyStep"
            self.job_output_dir.mkdir()
            artefact = DummyFileArtefact(
                seqdata_id=input_artefact.seqdata_id, path=self.job_output_dir / "dummy.artefact"
            )

            return [artefact]

    input_artefact = DummyFileArtefactStart(seqdata_id=SEQDATA_ID, path=Path("dummy.start"))

    with pytest.raises(AssertionError):
        await DummyStep().process(input_artefact)

# Test data

This directory contains test data. Most of the data is version-controlled with [DVC](https://dvc.org/), and stored remotely.
When files are added to DVC, it creates `.dvc` meta files to be stored in git, and with our settings moves the original files to a cache and hardlinks to the cache. The cache contents can be pushed to a remote.

The Steps container has `dvc` installed, and is configured with a remote on DigitalOcean (DO).
Before you can download/upload data from/to the remote, you will need an access key identifier and key to DigitalOcean spaces. With these, run the following command to configure these secrets for your repo:

`dvc remote modify --local digitalocean access_key_id 'myaccesskeyid'`
`dvc remote modify --local digitalocean secret_access_key 'mysecretkey'`

When you then have a git commit checked out, you can run `dvc pull` to download all data to the cache, or you can specify specific files.
`dvc pull` will run `dvc fetch` which downloads data from the remote to the cache, and `dvc checkout` which creates links in the workspace to match the data in the cache for the checked out .dvc files.

Note that we have configured dvc to use *hard*links, as Docker does not support [reflinks](https://dvc.org/doc/user-guide/data-management/large-dataset-optimization#file-link-types-for-the-dvc-cache), symlinks won't point to the correct path when used outside the container, and we don't want to store two copies of the same file content (both dvc cache and the workspace).
If you need to update or replace a dvc-tracked file, you need to use [`dvc unprotect`](https://dvc.org/doc/command-reference/unprotect) / `dvc remove` as explained in the [dvc docs](https://dvc.org/doc/user-guide/how-to/update-tracked-data#modifying-content).


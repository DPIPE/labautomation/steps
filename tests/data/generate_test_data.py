#!/usr/bin/env python

import argparse
import os
import random
import re
import time
import uuid
from collections import OrderedDict
from enum import Enum
from pathlib import Path

from artefacts.seqdata import GivenSexEnum
from jinja2 import Environment, FileSystemLoader

SCRIPT_DIR = Path(__file__).parent
JINJA2_TEMPLATE_DIR = SCRIPT_DIR / "template"
jinja_env = Environment(loader=FileSystemLoader(JINJA2_TEMPLATE_DIR))
TEST_DIR = SCRIPT_DIR
SINGLE_DOT_SAMPLE_TEMPLATE = "samples/single.sample"
BASEPIPE_TEMPLATE = "analyses-work/basepipe"
SINGLE_ANNOPIPE_TEMPLATE = "analyses-work/single.annopipe"
TRIOPIPE_TEMPLATE = "analyses-work/triopipe"
TRIO_ANNOPIPE_TEMPLATE = "analyses-work/trio.annopipe"

FILENAME_SEPARATOR = "_"

PRIORITIES = [1, 2, 3]
GENEPANELS = ["HBOC", "DMD", "OCD", "RAS"]
GENEPANEL_VERSIONS = ["v1.0.0", "v2.1.0"]

# sample IDs and family IDs must be unique
SAMPLE_IDS = []
FAM_IDS = []

INTERVAL = 0.1


def pretty_print_nested(d, indent=0):
    for key, value in d.items():
        print(" " * indent + str(key) + ":", end=" ")
        if isinstance(value, OrderedDict):
            print()
            pretty_print_nested(value, indent + 2)
        else:
            print(value)


class SAMPLE_TAG(Enum):
    """ """

    Normal_Single = "NS"
    Normal_Trio = "NT"
    Pending_Basepipe_Single = "PBS"
    Fail_Basepipe_Single = "FBS"
    Pending_Basepipe_Trio = "PBT"
    Fail_Basepipe_Trio = "FBT"
    Fail_Triopipe = "FTT"
    Fail_Annpipeo_Single = "FAS"
    Fail_Annopipe_Trio = "FAT"
    Pending_Seqdatamaker_Single = "PSS"
    Pending_Seqdatamaker_Trio = "PST"


def generate_uuid() -> str:
    return str(uuid.uuid4())


def generate_num_id(digits: int = 8) -> int:
    """
    Generate a random sample ID with a specified number of digits.

    Args:
    digits (int): The number of digits for the sample ID. Default is 8.

    Returns:
    int: The generated sample ID.
    """
    num_id = random.randint(int((digits - 1) * "9") + 1, int(digits * "9"))
    if num_id in SAMPLE_IDS:  # Check if the generated ID already exists in the list
        return generate_num_id()  # If it does, recursively call the function to generate a new ID

    SAMPLE_IDS.append(num_id)  # If the generated ID is unique, add it to the list of existing IDs
    return num_id  # Return the generated unique ID


def generate_famid(digits: int = 5) -> int:
    # Generate a random family ID with x digits
    famid = random.randint(int((digits - 1) * "9") + 1, int(digits * "9"))
    if famid in FAM_IDS:
        return generate_famid()

    FAM_IDS.append(famid)
    return famid


def generate_sample_id(prefix: SAMPLE_TAG = SAMPLE_TAG.Normal_Single, index: int = 1) -> str:
    """
    Generate a sample ID based on the given prefix and index.

    Parameters:
    prefix (SAMPLE_TAG): The prefix for the sample ID, default is SAMPLE_TAG.Normal_Single.
    index (int): The index to be used in the sample ID, default is 1.

    Returns:
    str: The generated sample ID.
    """

    if prefix in [
        SAMPLE_TAG.Normal_Single,
        SAMPLE_TAG.Normal_Trio,
    ]:  # Check if the prefix is for normal samples
        num_id = generate_num_id(digits=8)  # Generate a numerical ID with 8 digits
        stem = str(num_id)  # Convert the numerical ID to a string
    else:
        num_id = index  # Use the provided index as the numerical ID
        stem = f"{index:07d}"  # Format the index as a string with leading zeros if necessary

    return prefix.value + stem  # Combine the prefix and stem to form the sample ID


def print_tree(startpath, indent=""):
    for item in os.listdir(startpath):
        path = os.path.join(startpath, item)
        if os.path.isdir(path):
            print(f" {indent}├── {item}/")
            print_tree(path, indent + "│   ")
        else:
            print(f"{indent}├── {item}")


def touch_single_taqman_file(sample_dir: Path, owner_id: str, project: str, sample_id: str):
    stem = FILENAME_SEPARATOR.join([owner_id, project, sample_id])
    dot_taqman_file = stem + ".taqman"
    dot_taqman_path = sample_dir / dot_taqman_file
    dot_taqman_path.write_text("")


def write_single_dot_sample(
    sample_dir: Path, owner_id: str, project: str, sample_id: str, uuid: str, given_sex: str
):
    jinja_template = jinja_env.get_template(SINGLE_DOT_SAMPLE_TEMPLATE)

    content = jinja_template.render(
        owner_id=owner_id,
        uuid=uuid,
        num_id=sample_id,
        short_project=project,
        given_sex=given_sex,
    )

    stem = FILENAME_SEPARATOR.join([owner_id, project, sample_id, uuid])
    dot_sample_file = stem + ".sample"
    dot_sample_path = sample_dir / dot_sample_file
    dot_sample_path.write_text(content)
    return dot_sample_path


def touch_fastq_files(sample_dir: Path, owner_id: str, project: str, sample_id: str, uuid: str):
    R1 = FILENAME_SEPARATOR.join([owner_id, project, sample_id, uuid, "S4", "R1", "001.fastq.gz"])
    R2 = FILENAME_SEPARATOR.join([owner_id, project, sample_id, uuid, "S4", "R2", "001.fastq.gz"])
    Path(sample_dir, R1).write_text("")
    Path(sample_dir, R2).write_text("")


def generate_sample_dir(owner_id: str, project: str, sample_id: str, uuid: str, given_sex: str):
    if sample_id.startswith(SAMPLE_TAG.Pending_Basepipe_Single.value) or sample_id.startswith(
        SAMPLE_TAG.Pending_Basepipe_Trio.value
    ):
        return
    # create sample directory
    stem = FILENAME_SEPARATOR.join([owner_id, project, sample_id, uuid])
    sample_dir = TEST_DIR / Path("samples", stem)
    sample_dir.mkdir(parents=True, exist_ok=False)

    # write .sample file
    write_single_dot_sample(sample_dir, owner_id, project, sample_id, uuid, given_sex)

    # write .taqman file
    if re.match(r"^([A-Za-z]+)[sS]\d+$", sample_id):
        touch_single_taqman_file(sample_dir, owner_id, project, sample_id)

    # touch 2 fastq files
    if not sample_id.startswith(
        SAMPLE_TAG.Pending_Seqdatamaker_Single.value
    ) and not sample_id.startswith(SAMPLE_TAG.Pending_Seqdatamaker_Trio.value):
        touch_fastq_files(sample_dir, owner_id, project, sample_id, uuid)

    print(f"\n {sample_dir} created ✔ ")
    print_tree(sample_dir)

    time.sleep(INTERVAL)


def generate_basepipe(
    analyses_dir: Path,
    owner_id: str,
    project: str,
    sample_id: str,
    sample_uuid: str,
    priority: int,
    given_sex: str,
    workflow_id: str,
    workflow_name: str,
    taqman: str,
):
    basepipe_uuid = generate_uuid()
    basepipe_name = FILENAME_SEPARATOR.join([owner_id, project, sample_id, basepipe_uuid])
    basepipe_dir = analyses_dir / basepipe_name
    basepipe_dir.mkdir(parents=True, exist_ok=False)
    dot_analysis_file = basepipe_name + ".analysis"
    dot_analysis_path = basepipe_dir / dot_analysis_file

    jinja_template = jinja_env.get_template(BASEPIPE_TEMPLATE)

    content = jinja_template.render(
        priority=priority,
        taqman=taqman,
        name=basepipe_name,
        sample=FILENAME_SEPARATOR.join([owner_id, project, sample_id, sample_uuid]),
        given_sex=given_sex,
        workflow_id=workflow_id,
        workflow_name=workflow_name,
    )

    dot_analysis_path.write_text(content)
    print(f"\n {basepipe_dir} created ✔ ")
    print_tree(basepipe_dir)

    time.sleep(INTERVAL)


def generate_single_annopipe(
    analyses_dir: Path,
    owner_id: str,
    project: str,
    sample_id: str,
    sample_uuid: str,
    priority: int,
    given_sex: str,
    workflow_id: str,
    workflow_name: str,
):
    annopipe_uuid = generate_uuid()
    genepanel_name = random.choice(GENEPANELS)
    genepanel_version = random.choice(GENEPANEL_VERSIONS)
    annopipe_name = FILENAME_SEPARATOR.join(
        [
            owner_id,
            project,
            sample_id,
            genepanel_name,
            genepanel_version,
            annopipe_uuid,
        ]
    )
    annopipe_dir = analyses_dir / annopipe_name
    annopipe_dir.mkdir(parents=True, exist_ok=False)
    dot_analysis_file = annopipe_name + ".analysis"
    dot_analysis_path = annopipe_dir / dot_analysis_file

    jinja_template = jinja_env.get_template(SINGLE_ANNOPIPE_TEMPLATE)

    content = jinja_template.render(
        priority=priority,
        genepanel_name=genepanel_name,
        genepanel_version=genepanel_version,
        name=annopipe_name,
        sample=FILENAME_SEPARATOR.join([owner_id, project, sample_id, sample_uuid]),
        given_sex=given_sex,
        workflow_id=workflow_id,
        workflow_name=workflow_name,
    )

    dot_analysis_path.write_text(content)
    print(f"\n {annopipe_dir} created ✔ ")
    print_tree(annopipe_dir)

    time.sleep(INTERVAL)


def generate_single_sample_analyses(
    owner_id: str, project: str, sample_id: str, sample_uuid: str, given_sex: str
):
    analyses_dir = TEST_DIR / Path("analyses-work")

    priority = random.choice(PRIORITIES)
    genepanel_name = random.choice(GENEPANELS)
    genepanel_version = random.choice(GENEPANEL_VERSIONS)
    workflow_id = generate_uuid()
    workflow_name = FILENAME_SEPARATOR.join(
        [owner_id, project, sample_id, genepanel_name, genepanel_version]
    )

    generate_basepipe(
        analyses_dir,
        owner_id,
        project,
        sample_id,
        sample_uuid,
        priority,
        given_sex,
        workflow_id,
        workflow_name,
        taqman="true",
    )
    generate_single_annopipe(
        analyses_dir,
        owner_id,
        project,
        sample_id,
        sample_uuid,
        priority,
        given_sex,
        workflow_id,
        workflow_name,
    )


def _generate_pf(pending_or_fail: list[tuple[SAMPLE_TAG, int]], owner_id: str, project: str):
    for pf_type, pf_num in pending_or_fail:
        for i in range(pf_num):
            sample_id = generate_sample_id(prefix=pf_type, index=i + 1)
            sample_uuid = generate_uuid()
            given_sex = random.choice(list(GivenSexEnum))
            # skip generating sample dir if it is pending basepipe
            if pf_type not in [
                SAMPLE_TAG.Pending_Basepipe_Single,
                SAMPLE_TAG.Pending_Basepipe_Trio,
            ]:
                generate_sample_dir(owner_id, project, sample_id, sample_uuid, given_sex)
            generate_single_sample_analyses(owner_id, project, sample_id, sample_uuid, given_sex)


def generate_single(
    num_singles: int = 1,
    num_pending_s_basepipe: int = 0,
    num_fail_s_basepipe: int = 0,
    num_fail_s_annopipe: int = 0,
    num_pending_s_seqdatamaker: int = 0,
):
    # attributes
    owner_id = "OUSAMG"
    project = "wgs345"

    pending_or_fail = [
        (SAMPLE_TAG.Pending_Basepipe_Single, num_pending_s_basepipe),
        (SAMPLE_TAG.Fail_Basepipe_Single, num_fail_s_basepipe),
        (SAMPLE_TAG.Fail_Annpipeo_Single, num_fail_s_annopipe),
        (SAMPLE_TAG.Pending_Seqdatamaker_Single, num_pending_s_seqdatamaker),
    ]

    _generate_pf(pending_or_fail, owner_id, project)

    for _ in range(
        max(
            num_singles
            - num_pending_s_basepipe
            - num_fail_s_basepipe
            - num_fail_s_annopipe
            - num_pending_s_seqdatamaker,
            0,
        )
    ):
        sample_id = generate_sample_id(prefix=SAMPLE_TAG.Normal_Single)
        sample_uuid = generate_uuid()
        given_sex = random.choice(list(GivenSexEnum))

        generate_sample_dir(owner_id, project, sample_id, sample_uuid, given_sex)
        generate_single_sample_analyses(owner_id, project, sample_id, sample_uuid, given_sex)


def generate_triopipe(
    analyses_dir: Path,
    owner_id: str,
    project: str,
    pb_id: str,
    pb_uuid: str,
    fm_id: str,
    fm_uuid: str,
    mk_id: str,
    mk_uuid: str,
    priority: int,
    pb_given_sex: str,
    workflow_id: str,
    workflow_name: str,
):
    triopipe_uuid = generate_uuid()
    triopipe_name = FILENAME_SEPARATOR.join(
        [
            owner_id,
            project,
            pb_id,
            "TRIO",
            triopipe_uuid,
        ]
    )
    triopipe_dir = analyses_dir / triopipe_name
    triopipe_dir.mkdir(parents=True, exist_ok=False)
    dot_analysis_file = triopipe_name + ".analysis"
    dot_analysis_path = triopipe_dir / dot_analysis_file

    jinja_template = jinja_env.get_template(TRIOPIPE_TEMPLATE)

    content = jinja_template.render(
        priority=priority,
        pb_sample=FILENAME_SEPARATOR.join([owner_id, project, pb_id, pb_uuid]),
        pb_given_sex=pb_given_sex,
        fm_sample=FILENAME_SEPARATOR.join([owner_id, project, fm_id, fm_uuid]),
        mk_sample=FILENAME_SEPARATOR.join([owner_id, project, mk_id, mk_uuid]),
        name=triopipe_name,
        workflow_id=workflow_id,
        workflow_name=workflow_name,
    )

    dot_analysis_path.write_text(content)
    print(f"\n {triopipe_dir} created ✔ ")
    print_tree(triopipe_dir)

    time.sleep(INTERVAL)


def generate_trio_annopipe(
    analyses_dir: Path,
    owner_id: str,
    project: str,
    pb_id: str,
    pb_uuid: str,
    fm_id: str,
    fm_uuid: str,
    mk_id: str,
    mk_uuid: str,
    priority: int,
    pb_given_sex: str,
    workflow_id: str,
    workflow_name: str,
):
    annopipe_uuid = generate_uuid()
    genepanel_name = random.choice(GENEPANELS)
    genepanel_version = random.choice(GENEPANEL_VERSIONS)
    annopipe_name = FILENAME_SEPARATOR.join(
        [
            owner_id,
            project,
            pb_id,
            "TRIO",
            genepanel_name,
            genepanel_version,
            annopipe_uuid,
        ]
    )
    annopipe_dir = analyses_dir / annopipe_name
    annopipe_dir.mkdir(parents=True, exist_ok=False)
    dot_analysis_file = annopipe_name + ".analysis"
    dot_analysis_path = annopipe_dir / dot_analysis_file

    jinja_template = jinja_env.get_template(TRIO_ANNOPIPE_TEMPLATE)

    content = jinja_template.render(
        priority=priority,
        genepanel_name=genepanel_name,
        genepanel_version=genepanel_version,
        pb_sample=FILENAME_SEPARATOR.join([owner_id, project, pb_id, pb_uuid]),
        pb_given_sex=pb_given_sex,
        fm_sample=FILENAME_SEPARATOR.join([owner_id, project, fm_id, fm_uuid]),
        mk_sample=FILENAME_SEPARATOR.join([owner_id, project, mk_id, mk_uuid]),
        name=annopipe_name,
        workflow_id=workflow_id,
        workflow_name=workflow_name,
    )

    dot_analysis_path.write_text(content)
    print(f"\n {annopipe_dir} created ✔ ")
    print_tree(annopipe_dir)

    time.sleep(INTERVAL)


def generate_trio_analyses(
    owner_id, project, pb_id, pb_uuid, pb_given_sex, fm_id, fm_uuid, mk_id, mk_uuid
):
    analyses_dir = TEST_DIR / Path("analyses-work")
    priority = random.choice(PRIORITIES)

    genepanel_name = random.choice(GENEPANELS)
    genepanel_version = random.choice(GENEPANEL_VERSIONS)
    workflow_id = generate_uuid()
    workflow_name = FILENAME_SEPARATOR.join(
        [owner_id, project, pb_id, "TRIO", genepanel_name, genepanel_version]
    )

    generate_basepipe(
        analyses_dir,
        owner_id,
        project,
        pb_id,
        pb_uuid,
        priority,
        pb_given_sex,
        workflow_id,
        workflow_name,
        taqman="false",
    )

    generate_basepipe(
        analyses_dir,
        owner_id,
        project,
        fm_id,
        fm_uuid,
        priority,
        "male",
        workflow_id,
        workflow_name,
        taqman="false",
    )

    generate_basepipe(
        analyses_dir,
        owner_id,
        project,
        mk_id,
        mk_uuid,
        priority,
        "female",
        workflow_id,
        workflow_name,
        taqman="false",
    )

    generate_triopipe(
        analyses_dir,
        owner_id,
        project,
        pb_id,
        pb_uuid,
        fm_id,
        fm_uuid,
        mk_id,
        mk_uuid,
        priority,
        pb_given_sex,
        workflow_id,
        workflow_name,
    )

    generate_trio_annopipe(
        analyses_dir,
        owner_id,
        project,
        pb_id,
        pb_uuid,
        fm_id,
        fm_uuid,
        mk_id,
        mk_uuid,
        priority,
        pb_given_sex,
        workflow_id,
        workflow_name,
    )


def generate_trio(
    num_trios: int,
    num_pending_t_basepipe: int,
    num_fail_t_basepipe: int,
    num_fail_triopipe: int,
    num_fail_t_annopipe: int,
    num_pending_t_seqdatamaker: int,
):
    owner_id = "OUSAMG"
    project = "wgs345"

    pf_members = (
        [
            generate_sample_id(prefix=SAMPLE_TAG.Pending_Basepipe_Trio, index=i + 1)
            for i in range(num_pending_t_basepipe)
        ]
        + [
            generate_sample_id(prefix=SAMPLE_TAG.Fail_Basepipe_Trio, index=i + 1)
            for i in range(num_fail_t_basepipe)
        ]
        + [
            generate_sample_id(prefix=SAMPLE_TAG.Pending_Seqdatamaker_Trio, index=i + 1)
            for i in range(num_pending_t_seqdatamaker)
        ]
    )

    i = 0
    while i + 3 <= len(pf_members):
        pb_id, fm_id, mk_id = pf_members[i : i + 3]
        pb_uuid = generate_uuid()
        fm_uuid = generate_uuid()
        mk_uuid = generate_uuid()
        pb_given_sex = random.choice(list(GivenSexEnum))
        generate_sample_dir(owner_id, project, pb_id, pb_uuid, pb_given_sex)
        generate_sample_dir(owner_id, project, fm_id, fm_uuid, "male")
        generate_sample_dir(owner_id, project, mk_id, mk_uuid, "female")
        generate_trio_analyses(
            owner_id, project, pb_id, pb_uuid, pb_given_sex, fm_id, fm_uuid, mk_id, mk_uuid
        )

        i += 3

    if i < len(pf_members):
        use_normal = 3 + i - len(pf_members)
        pb_id, fm_id, mk_id = pf_members[i:] + [
            generate_sample_id(
                prefix=SAMPLE_TAG.Normal_Trio,
                index=i + 1,
            )
            for _ in range(use_normal)
        ]
        pb_uuid = generate_uuid()
        fm_uuid = generate_uuid()
        mk_uuid = generate_uuid()
        pb_given_sex = random.choice(list(GivenSexEnum))
        generate_sample_dir(owner_id, project, pb_id, pb_uuid, pb_given_sex)
        generate_sample_dir(owner_id, project, fm_id, fm_uuid, "male")
        generate_sample_dir(owner_id, project, mk_id, mk_uuid, "female")
        generate_trio_analyses(
            owner_id, project, pb_id, pb_uuid, pb_given_sex, fm_id, fm_uuid, mk_id, mk_uuid
        )
    else:
        use_normal = 0

    def _generate_trio_fail_trio_anno(fail_trio_anno: list[tuple[SAMPLE_TAG, int]]):
        for f_type, f_num in fail_trio_anno:
            for _ in range(f_num):
                pb_id = generate_sample_id(prefix=f_type)
                fm_id = generate_sample_id(prefix=SAMPLE_TAG.Normal_Trio)
                mk_id = generate_sample_id(prefix=SAMPLE_TAG.Normal_Trio)
                pb_uuid = generate_uuid()
                fm_uuid = generate_uuid()
                mk_uuid = generate_uuid()
                pb_given_sex = random.choice(list(GivenSexEnum))
                generate_sample_dir(owner_id, project, pb_id, pb_uuid, pb_given_sex)
                generate_sample_dir(owner_id, project, fm_id, fm_uuid, "male")
                generate_sample_dir(owner_id, project, mk_id, mk_uuid, "female")
                generate_trio_analyses(
                    owner_id, project, pb_id, pb_uuid, pb_given_sex, fm_id, fm_uuid, mk_id, mk_uuid
                )

    _generate_trio_fail_trio_anno(
        [
            (SAMPLE_TAG.Fail_Annopipe_Trio, num_fail_t_annopipe),
            (SAMPLE_TAG.Fail_Triopipe, num_fail_triopipe),
        ]
    )

    # normals
    for _ in range(
        (
            max(
                num_trios * 3
                - num_pending_t_basepipe
                - num_fail_t_basepipe
                - num_pending_t_seqdatamaker
                - use_normal
                - num_fail_triopipe * 3
                - num_fail_t_annopipe * 3,
                0,
            )
            - 1
        )
        // 3
        + 1
    ):
        pb_id = generate_sample_id(prefix=SAMPLE_TAG.Normal_Trio)
        fm_id = generate_sample_id(prefix=SAMPLE_TAG.Normal_Trio)
        mk_id = generate_sample_id(prefix=SAMPLE_TAG.Normal_Trio)
        pb_uuid = generate_uuid()
        fm_uuid = generate_uuid()
        mk_uuid = generate_uuid()
        pb_given_sex = random.choice(list(GivenSexEnum))

        generate_sample_dir(owner_id, project, pb_id, pb_uuid, pb_given_sex)
        generate_sample_dir(owner_id, project, fm_id, fm_uuid, "male")
        generate_sample_dir(owner_id, project, mk_id, mk_uuid, "female")
        generate_trio_analyses(
            owner_id, project, pb_id, pb_uuid, pb_given_sex, fm_id, fm_uuid, mk_id, mk_uuid
        )


def expected_jobs(
    num_pending_s_seqdatamaker,
    num_pending_s_basepipe,
    num_fail_s_annopipe,
    num_fail_s_basepipe,
    num_singles,
    num_trios,
    num_fail_t_basepipe,
    num_fail_triopipe,
    num_fail_t_annopipe,
    num_pending_t_basepipe,
    num_pending_t_seqdatamaker,
):
    pending_triopipe = (
        num_pending_t_seqdatamaker + num_pending_t_basepipe + num_fail_t_basepipe + 2
    ) // 3
    failed_triopipe = num_fail_triopipe
    if num_trios - pending_triopipe - failed_triopipe - num_fail_t_annopipe > 0:
        completed_triopipe = num_trios - pending_triopipe - failed_triopipe
        real_total_trios = num_trios
    else:
        completed_triopipe = num_fail_t_annopipe
        real_total_trios = pending_triopipe + failed_triopipe + num_fail_t_annopipe
    if (
        num_singles
        - num_pending_s_seqdatamaker
        - num_pending_s_basepipe
        - num_fail_s_basepipe
        - num_fail_s_annopipe
        > 0
    ):
        real_total_singles = num_singles
    else:
        real_total_singles = (
            num_pending_s_seqdatamaker
            + num_pending_s_basepipe
            + num_fail_s_basepipe
            + num_fail_s_annopipe
        )
    # pending basepipe no sample dir
    completed_taqman_filewatcher = real_total_singles - num_pending_s_basepipe
    pending_seqdatamaker = num_pending_s_seqdatamaker + num_pending_t_seqdatamaker
    pending_basepipe = num_pending_s_basepipe + num_pending_t_basepipe + pending_seqdatamaker
    failed_basepipe = num_fail_s_basepipe + num_fail_t_basepipe

    completed_dot_analysis_filewatcher = real_total_singles * 2 + real_total_trios * (3 + 1 + 1)
    # pending basepipe no sample dir
    completed_dot_sample_filewatcher = (
        real_total_singles + real_total_trios * 3 - num_pending_s_basepipe - num_pending_t_basepipe
    )
    completed_seqdatamaker = (
        real_total_singles
        + real_total_trios * 3
        - num_pending_s_seqdatamaker
        - num_pending_t_seqdatamaker
        - num_pending_s_basepipe
        - num_pending_t_basepipe
    )
    completed_fastq_filewatcher = completed_seqdatamaker * 2

    completed_basepipe = (
        real_total_singles + real_total_trios * 3 - pending_basepipe - failed_basepipe
    )
    pending_annopipe = (
        num_pending_s_basepipe
        + num_pending_s_seqdatamaker
        + num_fail_s_basepipe
        + pending_triopipe
        + failed_triopipe
    )
    failed_annopipe = num_fail_s_annopipe + num_fail_t_annopipe
    completed_s_annopipe = (
        real_total_singles
        - num_pending_s_basepipe
        - num_fail_s_basepipe
        - num_pending_s_seqdatamaker
        - num_fail_s_annopipe
    )
    completed_t_annopipe = completed_triopipe - num_fail_t_annopipe
    completed_annopipe = completed_s_annopipe + completed_t_annopipe

    stats = {
        "completed": {
            "total": completed_fastq_filewatcher
            + completed_taqman_filewatcher
            + completed_dot_sample_filewatcher
            + completed_dot_analysis_filewatcher
            + completed_seqdatamaker
            + completed_basepipe
            + completed_triopipe
            + completed_annopipe,
            "FASTQFileWatcher": completed_fastq_filewatcher,
            "TaqmanFileWatcher": completed_taqman_filewatcher,
            "DotsampleFileWatcher": completed_dot_sample_filewatcher,
            "SeqDataMaker": completed_seqdatamaker,
            "DotanalysisFileWatcher": completed_dot_analysis_filewatcher,
            "Basepipe": completed_basepipe,
            "Triopipe": completed_triopipe,
            "Annopipe": completed_annopipe,
        },
        "pending": {
            "total": pending_seqdatamaker + pending_basepipe + pending_triopipe + pending_annopipe,
            "SeqDataMaker": pending_seqdatamaker,
            "Basepipe": pending_basepipe,
            "Triopipe": pending_triopipe,
            "Annopipe": pending_annopipe,
        },
        "failed": {
            "total": failed_basepipe + failed_triopipe + failed_annopipe,
            "Basepipe": failed_basepipe,
            "Triopipe": failed_triopipe,
            "Annopipe": failed_annopipe,
        },
    }

    return stats


def generate_test_data(
    num_singles=0,
    num_pending_s_basepipe=0,
    num_fail_s_basepipe=0,
    num_fail_s_annopipe=0,
    num_pending_s_seqdatamaker=0,
    num_trios=0,
    num_pending_t_basepipe=0,
    num_fail_t_basepipe=0,
    num_fail_triopipe=0,
    num_fail_t_annopipe=0,
    num_pending_t_seqdatamaker=0,
):
    generate_single(
        num_singles,
        num_pending_s_basepipe,
        num_fail_s_basepipe,
        num_fail_s_annopipe,
        num_pending_s_seqdatamaker,
    )

    generate_trio(
        num_trios,
        num_pending_t_basepipe,
        num_fail_t_basepipe,
        num_fail_triopipe,
        num_fail_t_annopipe,
        num_pending_t_seqdatamaker,
    )

    stats = expected_jobs(
        num_pending_s_seqdatamaker,
        num_pending_s_basepipe,
        num_fail_s_annopipe,
        num_fail_s_basepipe,
        num_singles,
        num_trios,
        num_fail_t_basepipe,
        num_fail_triopipe,
        num_fail_t_annopipe,
        num_pending_t_basepipe,
        num_pending_t_seqdatamaker,
    )

    return stats


if __name__ == "__main__":
    arg_parser = argparse.ArgumentParser(
        description="""
        Generate test data for steps.


        Pending and failed samples are guaranteed to be generated.
        Normal single samples are generated if the total number of pending and failed single samples is less than the requested total number of singles to generate.
        Normal trios are generated if the total number of pending and failed trios is less than the requested total number of trios to generate.
        """,
        formatter_class=argparse.RawTextHelpFormatter,
    )

    arg_parser.add_argument(
        "-s",
        "--total-singles",
        type=int,
        help="Total number of single samples to generate",
        default=0,
    )
    arg_parser.add_argument(
        "-t",
        "--total-trios",
        type=int,
        help="Total number of trios (1 trio == 3 samples) to generate",
        default=0,
    )
    arg_parser.add_argument(
        "-d",
        "--testdir",
        type=str,
        help="Directory where the samples and analyses-work test data dirs are, default is the script directory",
    )

    arg_parser.add_argument(
        "-i",
        "--interval",
        type=float,
        help="Interval between folder creation in seconds, default is 0.1s",
    )

    arg_parser.add_argument(
        "--pending-s-seqdatamaker",
        type=int,
        help="Number of single-analysis samples pending SeqDataMaker",
        default=0,
    )

    arg_parser.add_argument(
        "--fail-s-basepipe",
        type=int,
        help="Number of single analysis basepipes to fail",
        default=0,
    )

    arg_parser.add_argument(
        "--fail-t-basepipe",
        type=int,
        help="Number of trio analysis basepipes to fail",
        default=0,
    )

    arg_parser.add_argument(
        "--fail-triopipe",
        type=int,
        help="Number of triopipes to fail, implies that all 3 basepipes succeeded",
        default=0,
    )

    arg_parser.add_argument(
        "--fail-s-annopipe",
        type=int,
        help="Number of single annopipes to fail",
        default=0,
    )

    arg_parser.add_argument(
        "--fail-t-annopipe",
        type=int,
        help="Number of trio annopipes to fail",
        default=0,
    )

    arg_parser.add_argument(
        "--pending-s-basepipe",
        type=int,
        help="Number of single analysis basepipes to be pending",
        default=0,
    )

    arg_parser.add_argument(
        "--pending-t-seqdatamaker",
        type=int,
        help="Number of trio-analysis members pending SeqDataMaker",
        default=0,
    )

    arg_parser.add_argument(
        "--pending-t-basepipe",
        type=int,
        help="Number of trio analysis basepipes to be pending",
        default=0,
    )

    args = arg_parser.parse_args()
    num_singles = args.total_singles
    num_trios = args.total_trios
    num_fail_s_basepipe = args.fail_s_basepipe
    num_fail_t_basepipe = args.fail_t_basepipe
    num_fail_triopipe = args.fail_triopipe
    num_fail_s_annopipe = args.fail_s_annopipe
    num_fail_t_annopipe = args.fail_t_annopipe
    num_pending_s_basepipe = args.pending_s_basepipe
    num_pending_t_basepipe = args.pending_t_basepipe
    num_pending_s_seqdatamaker = args.pending_s_seqdatamaker
    num_pending_t_seqdatamaker = args.pending_t_seqdatamaker

    if not any(
        [
            num_singles,
            num_trios,
            num_fail_s_basepipe,
            num_fail_t_basepipe,
            num_fail_triopipe,
            num_fail_s_annopipe,
            num_fail_t_annopipe,
            num_pending_s_basepipe,
            num_pending_t_basepipe,
            num_pending_s_seqdatamaker,
            num_pending_t_seqdatamaker,
        ]
    ):
        num_singles = 1

    if args.testdir:
        TEST_DIR = args.testdir
    if args.interval is not None:
        INTERVAL = args.interval

    print("REQUESTED TO GENERATE:")
    print("single samples pending SeqDataMaker: ", num_pending_s_seqdatamaker)
    print("single samples pending basepipe: ", num_pending_s_basepipe)
    print("single samples fail basepipe: ", num_fail_s_basepipe)
    print("single samples fail annopipe: ", num_fail_s_annopipe)
    print(
        "single samples normal: ",
        max(num_singles - num_pending_s_basepipe - num_fail_s_basepipe - num_fail_s_annopipe, 0),
    )
    print("trio members pending SeqDataMaker: ", num_pending_t_seqdatamaker)
    print("trio members pending basepipe: ", num_pending_t_basepipe)
    print("trio members fail basepipe: ", num_fail_t_basepipe)
    print("trios fail triopipe: ", num_fail_triopipe)
    print("trios fail annopipe: ", num_fail_t_annopipe)
    print(
        "trios complete normal: ",
        (
            max(
                num_trios * 3
                - num_pending_t_basepipe
                - num_fail_t_basepipe
                - num_fail_triopipe * 3
                - num_fail_t_annopipe * 3,
                0,
            )
            - 1
        )
        // 3
        + 1,
    )

    stats = generate_test_data(
        num_singles,
        num_pending_s_basepipe,
        num_fail_s_basepipe,
        num_fail_s_annopipe,
        num_pending_s_seqdatamaker,
        num_trios,
        num_pending_t_basepipe,
        num_fail_t_basepipe,
        num_fail_triopipe,
        num_fail_t_annopipe,
        num_pending_t_seqdatamaker,
    )

    pretty_print_nested(stats)

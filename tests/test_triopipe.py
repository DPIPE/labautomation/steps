import json
import uuid
from pathlib import Path

import pytest

import artefacts.vcpipe
from artefacts.seqdata import DotSampleFile
from artefacts.vcpipe import GenomicVariantFile, MTVariantFile, QCResult
from tests.conftest import dvc_require
from workflowsteps.triopipe import Triopipe

DATA_DIR = Path(__file__).resolve().parent.parent / "vcpipe-testdata/dragen-trio"


PROBAND_SEQDATA_ID = uuid.UUID("d25a1931-af9c-4d3d-a3a5-c11dba186f79")
FM_SEQDATA_ID = uuid.UUID("3fd7fa9c-ffba-4a91-ad49-f34fe649292f")
MK_SEQDATA_ID = uuid.UUID("06781d39-c9b5-46f2-9a48-d37aedaf0a34")
WORKFLOW_ID = uuid.uuid4()
WORKFLOW_NAME = uuid.uuid4().hex


def get_pedigree(dotanalysis):
    with open(dotanalysis) as f:
        trio_dotanalysis = json.load(f)
        pedigree = trio_dotanalysis["params"]["pedigree"]
    return json.dumps(pedigree)


@dvc_require(
    [
        "vcpipe-refdata/genomic/common/general/human_g1k_v37_decoy.fasta*",
        "vcpipe-refdata/genomic/knownSites/dbsnp/*",
        "singularity/vcpipe-essentials/*",
        "vcpipe-testdata/dragen-trio/*",
    ]
)
@pytest.mark.asyncio
async def test_triopipe():
    TRIO_DOTANALYSIS = DATA_DIR / "Diag-wgs286-HG002K1-PM-DR-TRIO.analysis"
    triopipe = Triopipe()
    with open(DATA_DIR / "Diag-wgs286-HG002K1-PM.sample") as f:
        contents = f.read()
        dotsample = DotSampleFile.model_validate_json(contents)

    triopipe_artefacts = await triopipe.process(
        workflow_id=WORKFLOW_ID,
        workflow_name=WORKFLOW_NAME,
        job_id=uuid.uuid4(),
        analysis_name=uuid.uuid4().hex,
        specialized_pipeline="Dragen",
        proband_dot_sample=dotsample,
        pedigree_json=get_pedigree(TRIO_DOTANALYSIS),
        gvcf_1=GenomicVariantFile(
            seqdata_id=PROBAND_SEQDATA_ID,
            path=DATA_DIR / "Diag-wgs286-HG002K1-PM.g.vcf.gz",
            workflow_id=WORKFLOW_ID,
        ),
        gvcf_2=GenomicVariantFile(
            seqdata_id=FM_SEQDATA_ID,
            path=DATA_DIR / "Diag-wgs286-HG003B2-FM.g.vcf.gz",
            workflow_id=WORKFLOW_ID,
        ),
        gvcf_3=GenomicVariantFile(
            seqdata_id=MK_SEQDATA_ID,
            path=DATA_DIR / "Diag-wgs286-HG004B3-MK.g.vcf.gz",
            workflow_id=WORKFLOW_ID,
        ),
        qc_results_1=QCResult(
            seqdata_id=PROBAND_SEQDATA_ID,
            path=DATA_DIR / "Diag-wgs286-HG002K1-PM.qc_result.json",
            workflow_id=WORKFLOW_ID,
        ),
        qc_results_2=QCResult(
            seqdata_id=FM_SEQDATA_ID,
            path=DATA_DIR / "Diag-wgs286-HG003B2-FM.qc_result.json",
            workflow_id=WORKFLOW_ID,
        ),
        qc_results_3=QCResult(
            seqdata_id=MK_SEQDATA_ID,
            path=DATA_DIR / "Diag-wgs286-HG004B3-MK.qc_result.json",
            workflow_id=WORKFLOW_ID,
        ),
        mtvcf_1=MTVariantFile(
            seqdata_id=PROBAND_SEQDATA_ID,
            path=DATA_DIR / "Diag-wgs286-HG002K1-PM.chrMT.final.vcf.gz",
            workflow_id=WORKFLOW_ID,
        ),
        mtvcf_2=MTVariantFile(
            seqdata_id=MK_SEQDATA_ID,
            path=DATA_DIR / "Diag-wgs286-HG004B3-MK.chrMT.final.vcf.gz",
            workflow_id=WORKFLOW_ID,
        ),
    )

    assert {type(a) for a in triopipe_artefacts} == {
        artefacts.vcpipe.MultiSampleVariantFile,
        artefacts.vcpipe.QCResultTrio,
        artefacts.vcpipe.QCReportTrio,
        artefacts.vcpipe.QCWarningsTrio,
        artefacts.vcpipe.MultiSampleMTVariantFile,
    }
    assert len(triopipe_artefacts) == 5

import uuid
from pathlib import Path
from unittest.mock import patch

import pytest
from maestro import FileArtefactModel
from workflowsteps.dotsamplewatcher import DotsampleFileWatcher

DATA_DIR = Path(__file__).resolve().parent / "data/samples"
SEQDATA = Path("OUSAMG_wgs123_HG123456_2f08b36a-b3e3-406b-9c6d-e8e47d20d922")
UUID = uuid.UUID("2f08b36a-b3e3-406b-9c6d-e8e47d20d922")


@patch("workflowsteps.dotsamplewatcher.order_workflow_with_job_filtering")
@pytest.mark.datafiles(DATA_DIR)
async def test_dotsamplefilewatcher(SeqDataMaker_order_mock, datafiles):
    DotsampleFileWatcher.path = datafiles
    dotsample = FileArtefactModel(path=datafiles / SEQDATA / "sample1.sample")
    fw = DotsampleFileWatcher()
    artefacts = await fw.process(dotsample)

    assert len(artefacts) == 1
    assert artefacts[0].seqdata_id == UUID

    # Ensure that we placed an order to SeqDataMaker
    SeqDataMaker_order_mock.assert_awaited_once()

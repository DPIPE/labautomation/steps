import pytest
from zappy.sapioclient import SapioClient, SapioRecordNotFound

from tests.zappy.sapiomock import Directory


async def test_sapioclient_get_root_directory_record(sapio_client_readonly: SapioClient) -> None:
    record = await sapio_client_readonly.get_record(Directory, 1)
    assert record is not None
    assert record == Directory(RecordId=1, DirectoryName="Root Directory")


async def test_sapioclient_update_root_directory_record(sapio_client: SapioClient) -> None:
    record = await sapio_client.get_record(Directory, 1)
    assert record is not None
    record.DirectoryName = "Root Directory Updated"
    await sapio_client.batchupdate(updated_records=[record])
    updated_record = await sapio_client.get_record(Directory, 1)
    assert updated_record == Directory(RecordId=1, DirectoryName="Root Directory Updated")


async def test_sapioclient_add_new_directory_record(sapio_client: SapioClient) -> None:
    new_directory = Directory(RecordId=-1, DirectoryName="New Directory")
    [db_record_id] = await sapio_client.batchupdate(new_records=[new_directory])
    db_record = await sapio_client.get_record(Directory, db_record_id)
    assert db_record.model_dump(exclude={"RecordId"}) == new_directory.model_dump(
        exclude={"RecordId"}
    )


async def test_sapioclient_get_non_existing_record_raises(sapio_client: SapioClient) -> None:
    with pytest.raises(SapioRecordNotFound):
        await sapio_client.get_record(Directory, 2)

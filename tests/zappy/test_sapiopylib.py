from sapiopylib.rest.DataMgmtService import DataMgmtServer
from sapiopylib.rest.DataRecordManagerService import DataRecordManager
from sapiopylib.rest.DataTypeService import DataTypeManager
from sapiopylib.rest.User import SapioUser
from sapiopylib.rest.utils.recordmodel.RecordModelManager import (
    RecordModelInstanceManager,
    RecordModelManager,
)


def test_DataTypeManager(sapiopylib_client: SapioUser) -> None:
    """
    Test the "DataTypeManager" class by requesting the list of data types and
    asserting that it includes some expected data types.
    """
    data_type_manager: DataTypeManager = DataMgmtServer.get_data_type_manager(sapiopylib_client)
    data_type_name_list = data_type_manager.get_data_type_name_list()
    assert {"Directory", "Attachment"} <= set(data_type_name_list)


def test_DataRecord_query_one(sapiopylib_client: SapioUser) -> None:
    record_manager: DataRecordManager = DataMgmtServer.get_data_record_manager(sapiopylib_client)
    root_directory_record = record_manager.query_system_for_record("Directory", 1)
    assert root_directory_record.data_type_name == "Directory"
    assert root_directory_record.record_id == 1
    assert root_directory_record.fields.keys() >= {"DirectoryName"}
    assert root_directory_record.get_field_value("DirectoryName") == "Root Directory"
    assert not root_directory_record.is_deleted
    assert not root_directory_record.is_new


def test_DataRecord_query_nonexistent(sapiopylib_client: SapioUser) -> None:
    record_manager: DataRecordManager = DataMgmtServer.get_data_record_manager(sapiopylib_client)
    assert record_manager.query_system_for_record("Directory", -1) is None


def test_DataRecord_query_all(sapiopylib_client: SapioUser) -> None:
    record_manager: DataRecordManager = DataMgmtServer.get_data_record_manager(sapiopylib_client)
    result = record_manager.query_all_records_of_type("Directory")
    directory_records = result.result_list
    assert len(directory_records) >= 1
    for record in directory_records:
        assert record.data_type_name == "Directory"
        assert record.fields.keys() >= {"DirectoryName"}
        assert not record.is_deleted
        assert not record.is_new


def test_RecordModelManager_create_and_delete_record(sapiopylib_client: SapioUser) -> None:
    model_manager: RecordModelManager = RecordModelManager(sapiopylib_client)
    instance_manager: RecordModelInstanceManager = model_manager.instance_manager
    test_directory_record = instance_manager.add_new_record("Directory")
    assert test_directory_record.record_id == -1
    assert test_directory_record.data_type_name == "Directory"
    assert test_directory_record.fields.copy_to_dict().keys() >= {"DirectoryName"}
    assert test_directory_record.get_field_value("DirectoryName") == "New Directory"
    assert not test_directory_record.is_deleted
    assert test_directory_record.is_new
    test_directory_record.set_field_value("DirectoryName", "Test Directory")
    model_manager.store_and_commit()
    assert test_directory_record.record_id > 0
    assert not test_directory_record.is_new
    test_directory_record.delete()
    assert test_directory_record.is_deleted
    model_manager.store_and_commit()
    assert test_directory_record.is_deleted
    record_manager: DataRecordManager = DataMgmtServer.get_data_record_manager(sapiopylib_client)
    test_directory_record = record_manager.query_system_for_record(
        "Directory", test_directory_record.record_id
    )
    assert test_directory_record is None


def test_RecordModelManager_update_and_delete_queried_records(sapiopylib_client: SapioUser) -> None:
    model_manager: RecordModelManager = RecordModelManager(sapiopylib_client)
    instance_manager: RecordModelInstanceManager = model_manager.instance_manager
    attachment_record1 = instance_manager.add_new_record("Attachment")
    assert attachment_record1.get_field_value("FilePath") is None
    attachment_record1.set_field_value("FilePath", "TestAttachment1.txt")
    attachment_record2 = instance_manager.add_new_record("Attachment")
    assert attachment_record2.get_field_value("FilePath") is None
    attachment_record2.set_field_value("FilePath", "TestAttachment2.txt")
    model_manager.store_and_commit()
    record_manager: DataRecordManager = DataMgmtServer.get_data_record_manager(sapiopylib_client)
    result = record_manager.query_data_records(
        "Attachment", "FilePath", ["TestAttachment1.txt", "TestAttachment2.txt"]
    )
    assert len(result) >= 2
    attachment_records = instance_manager.add_existing_records(result.result_list)
    for attachment_record in attachment_records:
        field_value = attachment_record.get_field_value("FilePath")
        assert isinstance(field_value, str)
        attachment_record.set_field_value("FilePath", field_value.replace(".txt", ".pdf"))
    model_manager.store_and_commit()
    result = record_manager.query_data_records(
        "Attachment", "FilePath", ["TestAttachment1.pdf", "TestAttachment2.pdf"]
    )
    assert len(result) >= 2
    for attachment_record in attachment_records:
        attachment_record.delete()
    model_manager.store_and_commit()
    result = record_manager.query_data_records(
        "Attachment",
        "FilePath",
        [
            "TestAttachment1.txt",
            "TestAttachment2.txt",
            "TestAttachment1.pdf",
            "TestAttachment2.pdf",
        ],
    )
    assert len(result) == 0

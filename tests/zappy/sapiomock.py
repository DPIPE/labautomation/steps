import asyncio
from typing import TypeVar

from aiohttp import web
from zappy.models import (
    ACMG,
    Allele,
    Analysis,
    BioinfTrigger,
    EllaWorkflowStepChange,
    Individual,
    NeedsVerification,
    NotRelevantVariant,
    ReportedVariant,
    TranscriptHGVS,
    VariantReference,
)
from zappy.sapiorecord import SapioRecord


class Directory(SapioRecord):
    DirectoryName: str = "New Directory"


class Attachment(SapioRecord):
    FilePath: str


class SapioMock:
    DATA_TYPES: dict[str, type[SapioRecord]] = {
        "Directory": Directory,
        "Attachment": Attachment,
        "BioinfTrigger": BioinfTrigger,
        "Analysis": Analysis,
        "Individual": Individual,
        "TranscriptHGVS": TranscriptHGVS,
        "Allele": Allele,
        "NeedsVerification": NeedsVerification,
        "ACMG": ACMG,
        "VariantReference": VariantReference,
        "ReportedVariant": ReportedVariant,
        "NotRelevantVariant": NotRelevantVariant,
        "EllaWorkflowStepChange": EllaWorkflowStepChange,
    }

    def __init__(self, fail_count: int = 0) -> None:
        self.datarecord_fail_count = fail_count
        self.runbatchupdate_fail_count = fail_count
        self.querydatarecords_fail_count = fail_count
        self.app = web.Application()
        self.app.router.add_get("/datatypemanager/datatypenamelist", self.datatypenamelist)
        self.app.router.add_get("/datarecord", self.datarecord)
        self.app.router.add_get("/datarecordlist/all", self.datarecordlist_all)
        self.app.router.add_get("/datatypemanager/veloxfieldlist/{type}", self.veloxfieldlist)
        self.app.router.add_post("/datarecordlist/runbatchupdate", self.runbatchupdate)
        self.app.router.add_post("/datarecordlist", self.datarecordlist)
        self.app.router.add_post("/datarecordmanager/querydatarecords", self.querydatarecords)
        self.runbatchupdate_requests: asyncio.Queue[web.Request] = asyncio.Queue()
        self.data: dict[str, dict[int, SapioRecord]] = {}
        self.next_record_id: int = 1
        self.field_list: dict[str, list[dict[str, str]]] = {
            "Directory": [
                {
                    "dataFieldType": "STRING",
                    "dataFieldName": "DirectoryName",
                    "defaultValue": "New Directory",
                }
            ],
            "Attachment": [],
        }
        self.insert_data(Directory(DirectoryName="Root Directory"))

    def insert_data(self, record: SapioRecord) -> int:
        data_type_name = type(record).__name__
        assert data_type_name in self.DATA_TYPES
        if data_type_name not in self.data:
            self.data[data_type_name] = {}
        if record.RecordId is None:
            record.RecordId = self.next_record_id
            self.next_record_id += 1
        else:
            self.next_record_id = max(self.next_record_id, record.RecordId + 1)
        self.data[data_type_name][record.RecordId] = record
        return record.RecordId

    SapioRecordSubType = TypeVar("SapioRecordSubType", bound=SapioRecord)

    def get_data_of_type(
        self, data_type: type[SapioRecordSubType]
    ) -> dict[int, SapioRecordSubType]:
        if data_type.__name__ not in self.data:
            return {}
        return self.data[data_type.__name__]  # type: ignore[return-value]

    async def start(self) -> None:
        self.runner = web.AppRunner(self.app)
        await self.runner.setup()
        site = web.TCPSite(self.runner, "127.0.0.1", 0)
        await site.start()
        assert isinstance(site._server, asyncio.Server)
        host, port = site._server.sockets[0].getsockname()
        self.url = f"http://{host}:{port}"

    async def cleanup(self):
        await self.runner.cleanup()

    async def datatypenamelist(self, _: web.Request) -> web.Response:
        return web.json_response(list(self.DATA_TYPES.keys()))

    async def datarecord(self, request: web.Request) -> web.Response:
        if self.datarecord_fail_count > 0:
            self.datarecord_fail_count -= 1
            return web.Response(status=500)
        dataTypeName = request.query["dataTypeName"]
        recordId = int(request.query["recordId"])
        if recordId not in self.data[dataTypeName]:
            return web.Response(status=204)
        return web.json_response(
            {
                "dataTypeName": dataTypeName,
                "recordId": recordId,
                "fields": self.data[dataTypeName][recordId].model_dump(mode="json"),
            }
        )

    async def datarecordlist_all(self, request: web.Request) -> web.Response:
        dataTypeName = request.query["dataTypeName"]
        return web.json_response(
            {
                "resultList": [
                    {
                        "dataTypeName": dataTypeName,
                        "recordId": recordId,
                        "fields": record.model_dump(mode="json"),
                    }
                    for recordId, record in self.data[dataTypeName].items()
                ]
            }
        )

    async def veloxfieldlist(self, request: web.Request) -> web.Response:
        dataTypeName = request.match_info["type"]
        return web.json_response(self.field_list[dataTypeName])

    async def runbatchupdate(self, request: web.Request) -> web.Response:
        if self.runbatchupdate_fail_count > 0:
            self.runbatchupdate_fail_count -= 1
            return web.Response(status=500)
        body = await request.json()
        added_record_updates = {}
        for added_record in body["recordsAdded"]:
            dataTypeName = added_record["dataTypeName"]
            assert added_record["new"]
            assert not added_record["deleted"]
            recordId = added_record["recordId"]
            assert recordId < 0
            fields = added_record["fields"]
            record = self.DATA_TYPES[dataTypeName].model_validate(fields)
            assert record.RecordId is None
            recordId = self.insert_data(record)
            added_record_updates[added_record["recordId"]] = {
                "dataTypeName": dataTypeName,
                "recordId": recordId,
            }
        for changed_record in body["recordFieldsChanged"]:
            dataTypeName = changed_record["dataTypeName"]
            assert not changed_record["new"]
            assert not changed_record["deleted"]
            assert "RecordId" not in changed_record["fields"]
            recordId = changed_record["recordId"]
            fields = self.data[dataTypeName][recordId].model_dump()
            fields.update(changed_record["fields"])
            record = self.DATA_TYPES[dataTypeName].model_validate(fields)
            self.data[dataTypeName][recordId] = record
        for deleted_record in body["recordsDeleted"]:
            dataTypeName = deleted_record["dataTypeName"]
            recordId = deleted_record["recordId"]
            del self.data[dataTypeName][recordId]
        self.runbatchupdate_requests.put_nowait(request)
        return web.json_response({"addedRecordUpdates": added_record_updates})

    async def datarecordlist(self, request: web.Request) -> web.Response:
        dataTypeName = request.query["dataTypeName"]
        recordIds = await request.json()
        pageSize = int(request.query["pageSize"])
        return web.json_response(
            {
                "resultList": [
                    {
                        "dataTypeName": dataTypeName,
                        "recordId": recordId,
                        "fields": record.model_dump(mode="json"),
                    }
                    for recordId, record in self.data[dataTypeName].items()
                    if recordId in recordIds
                ][:pageSize]
            }
        )

    async def querydatarecords(self, request: web.Request) -> web.Response:
        if self.querydatarecords_fail_count > 0:
            self.querydatarecords_fail_count -= 1
            return web.Response(status=500)
        dataTypeName = request.query["dataTypeName"]
        dataFieldName = request.query["dataFieldName"]
        values = await request.json()
        return web.json_response(
            {
                "resultList": [
                    {
                        "dataTypeName": dataTypeName,
                        "recordId": recordId,
                        "fields": record.model_dump(mode="json"),
                    }
                    for recordId, record in (
                        self.data[dataTypeName].items() if dataTypeName in self.data else []
                    )
                    if getattr(record, dataFieldName) in values
                ]
            }
        )

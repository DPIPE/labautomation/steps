import asyncio
import json
from datetime import UTC, datetime

import pytest
import pytest_asyncio
import workflowsteps.dotanalysiswatcher
import zappy.run
from ellamock import EllaMock
from sapiomock import SapioMock
from schemas.examples.workflow_step_change import example
from workflowsteps.annopipe import Annopipe
from workflowsteps.basepipe import Basepipe
from workflowsteps.dotsamplewatcher import DotsampleFileWatcher
from workflowsteps.fastqwatcher import FASTQFileWatcher, SeqDataMaker
from workflowsteps.taqmanwatcher import TaqmanFileWatcher
from workflowsteps.triopipe import Triopipe
from zappy.models import (
    ACMG,
    Allele,
    Analysis,
    BioinfTrigger,
    EllaWorkflowStepChange,
    HandleStatusEnum,
    Individual,
    NeedsVerification,
    NotRelevantVariant,
    ReportedVariant,
    TranscriptHGVS,
    TriggerTypeEnum,
    VariantReference,
)

from config import config
from maestro import WorkflowModel, run_workflow_steps
from maestro.workflow_utils import order_workflow_with_job_filtering
from tests.data import generate_test_data


class TestError(RuntimeError):
    __test__ = False


@pytest_asyncio.fixture()
async def ella_mock():
    ella_mock = EllaMock()
    await ella_mock.setup()
    ella_task = asyncio.create_task(ella_mock.run())
    try:
        yield ella_mock
    finally:
        ella_task.cancel()
        with pytest.raises(asyncio.CancelledError):
            await asyncio.wait_for(ella_task, 5)


@pytest.mark.destructive
@pytest.mark.parametrize("simulate_errors, pedigree_size", [(False, 1), (True, 3)])
async def test_zappy_handle_one_analysis_trigger_record(
    simulate_errors,
    pedigree_size,
    ella_mock: EllaMock,
    tmp_path,
    monkeypatch,
    truncate_database,
    mock_vcpipe,
):
    """
    Test that Zappy can handle a single .analysis trigger record from Sapio.

    When parameter "simulate_errors" is set to True, test that Zappy recovers from processing hiccups caused
    by Sapio errors or errors during order handling.

    In the error case, the test sets up "SapioMock" so that it fails to respond to the first GET request and
    the first POST request, returning instead a 500 status code.

    Furthermore, in the error case, the first call to handle_order() raises a "RuntimeError", simulating an
    error during order handling. This will cause the Zappy task to terminate once during the test, but upon
    being restarted, Zappy should successfully handle the order which remains waiting in the NATS stream.
    """
    ordered_workflows: asyncio.Queue[WorkflowModel] = asyncio.Queue()
    fail_count = 1 if simulate_errors else 0

    async def mock_order_workflow_with_job_filtering(workflow: WorkflowModel) -> None:
        nonlocal fail_count
        if fail_count > 0:
            fail_count -= 1
            raise TestError()
        ordered_workflows.put_nowait(workflow.model_copy())
        await order_workflow_with_job_filtering(workflow)

    monkeypatch.setattr(
        workflowsteps.dotanalysiswatcher,
        "order_workflow_with_job_filtering",
        mock_order_workflow_with_job_filtering,
    )
    sapio_mock = SapioMock(fail_count)
    file_watcher_classes = [DotsampleFileWatcher, FASTQFileWatcher, TaqmanFileWatcher]
    for file_watcher_class in file_watcher_classes:
        monkeypatch.setattr(file_watcher_class, "path", tmp_path)
    steps_task = await run_workflow_steps(
        message_watcher_classes=[
            SeqDataMaker,
            Basepipe,
            Triopipe,
            Annopipe,
            zappy.run.EllaDelivery,
        ],
        file_watcher_classes=file_watcher_classes,
    )
    # Allow file watchers' run() methods to reach awatch() before copying files.
    await asyncio.sleep(0)
    generate_test_data.TEST_DIR = tmp_path
    if pedigree_size == 1:
        generate_test_data.generate_test_data(num_singles=1)
        [dot_analysis_path] = list(
            (tmp_path / "analyses-work").glob(
                "*/OUSAMG_wgs345_??????????_????????-????-????-????-????????????.analysis"
            )
        )
        [dot_analysis_annopipe_path] = list(
            (tmp_path / "analyses-work").glob(
                "*/OUSAMG_wgs345_??????????_*_????????-????-????-????-????????????.analysis"
            )
        )
    elif pedigree_size == 3:
        generate_test_data.generate_test_data(num_trios=1)
        [dot_analysis_path] = list(
            (tmp_path / "analyses-work").glob(
                "*/OUSAMG_wgs345_??????????_TRIO_????????-????-????-????-????????????.analysis"
            )
        )
        [dot_analysis_annopipe_path] = list(
            (tmp_path / "analyses-work").glob(
                "*/OUSAMG_wgs345_??????????_TRIO_*_????????-????-????-????-????????????.analysis"
            )
        )
    else:
        assert False, "Invalid value for 'pedigree_size'."
    dot_analysis: dict = json.loads(dot_analysis_path.open().read())
    dot_analysis_annopipe: dict = json.loads(dot_analysis_annopipe_path.open().read())
    if pedigree_size == 1:
        proband_id = sapio_mock.insert_data(
            Individual(
                sample=dot_analysis["samples"][0],
                given_sex=dot_analysis["gender"],
            )
        )
    elif pedigree_size == 3:
        mother_id = sapio_mock.insert_data(
            Individual(
                sample=dot_analysis["params"]["pedigree"]["mother"]["sample"],
                given_sex=dot_analysis["params"]["pedigree"]["mother"]["gender"],
            )
        )
        father_id = sapio_mock.insert_data(
            Individual(
                sample=dot_analysis["params"]["pedigree"]["father"]["sample"],
                given_sex=dot_analysis["params"]["pedigree"]["father"]["gender"],
            )
        )
        proband_id = sapio_mock.insert_data(
            Individual(
                sample=dot_analysis["params"]["pedigree"]["proband"]["sample"],
                given_sex=dot_analysis["params"]["pedigree"]["proband"]["gender"],
                mother_id=mother_id,
                father_id=father_id,
            )
        )
    else:
        assert False, "Invalid value for 'pedigree_size'."
    analysis_id = sapio_mock.insert_data(
        Analysis(
            priority=dot_analysis["priority"],
            proband_id=proband_id,
            specialized_pipeline=dot_analysis["params"]["specialized_pipeline"],
            taqman=dot_analysis["params"]["taqman"] if pedigree_size == 1 else False,
            genepanel=dot_analysis_annopipe["params"]["genepanel"],
            date_analysis_requested=dot_analysis["date_analysis_requested"],
        )
    )
    record_id = sapio_mock.insert_data(
        BioinfTrigger(
            type=TriggerTypeEnum.NEW_ANALYSIS,
            root_id=analysis_id,
            trigger_time=datetime.now(UTC),
        )
    )
    await sapio_mock.start()
    monkeypatch.setattr(config.zappy, "sapio_url", sapio_mock.url)
    monkeypatch.setattr(config.zappy, "polling_interval", 0.1)
    zappy_task = asyncio.create_task(zappy.run.main())
    try:
        if simulate_errors:
            # In the error case, the Zappy task is expected to terminate once due to the TestError
            # exception raised by the first call to mock_order_workflow_with_job_filtering().
            with pytest.raises(TestError):
                await asyncio.wait_for(zappy_task, 5)
            # Restart the Zappy task and expect it to pick up from where it left off.
            zappy_task = asyncio.create_task(zappy.run.main())
        # Wait for Zappy to order the workflow and inspect some of its properties.
        for _ in range(pedigree_size):
            basepipe_order = await asyncio.wait_for(ordered_workflows.get(), 5)
            assert len(basepipe_order.jobs) == 1
            assert {job.workflow_step for job in basepipe_order.jobs} == {"Basepipe"}
        if pedigree_size == 3:
            triopipe_order = await asyncio.wait_for(ordered_workflows.get(), 5)
            assert len(triopipe_order.jobs) == 1
            assert {job.workflow_step for job in triopipe_order.jobs} == {"Triopipe"}
        annopipe_order = await asyncio.wait_for(ordered_workflows.get(), 5)
        assert len(annopipe_order.jobs) == 1
        assert {job.workflow_step for job in annopipe_order.jobs} == {"Annopipe"}
        # Wait until BioinfTrigger record has been set to "handled".
        async with asyncio.timeout(15):
            while True:
                trigger_record = sapio_mock.get_data_of_type(BioinfTrigger)[record_id]
                if trigger_record.handle_status == HandleStatusEnum.HANDLED:
                    break
                await sapio_mock.runbatchupdate_requests.get()
        assert trigger_record.handle_time is not None
        # Wait until Analysis record has a non-None status.
        async with asyncio.timeout(120):
            while True:
                analysis_record = sapio_mock.get_data_of_type(Analysis)[analysis_id]
                if analysis_record.status is not None:
                    break
                await sapio_mock.runbatchupdate_requests.get()
        assert analysis_record.status.value == "completed"
        ella_input = await asyncio.wait_for(ella_mock.received_input.get(), 5)
        assert ella_input.sapio_id == analysis_id
        await ella_mock.send_workflow_step_change(
            example.model_copy(update={"external_id": str(ella_input.sapio_id)})
        )
        # Wait until there exist VariantReference records from EllaMock. This record type
        # is the last one to be imported by Zappy.
        async with asyncio.timeout(5):
            while True:
                if len(sapio_mock.get_data_of_type(VariantReference)) > 0:
                    break
                await sapio_mock.runbatchupdate_requests.get()
        reported_variants = sapio_mock.get_data_of_type(ReportedVariant)
        assert len(reported_variants) == len(example.reported_variants)
        [reported_variant] = reported_variants.values()
        assert reported_variant.model_dump(
            exclude={"RecordId", "workflow_step_change_id", "allele_id"}
        ) == example.reported_variants[0].model_dump(
            exclude={"allele", "acmg_criteria", "references"}
        )
        reported_variant_acmg_criteria = [
            acmg_criterium.model_dump(exclude={"RecordId", "variant_id"})
            for acmg_criterium in sapio_mock.get_data_of_type(ACMG).values()
            if acmg_criterium.variant_id == reported_variant.RecordId
        ]
        assert reported_variant_acmg_criteria == [
            acmg_criterium.model_dump()
            for acmg_criterium in example.reported_variants[0].acmg_criteria
        ]
        reported_variant_references = [
            reference.reference
            for reference in sapio_mock.get_data_of_type(VariantReference).values()
            if reference.variant_id == reported_variant.RecordId
        ]
        assert reported_variant_references == example.reported_variants[0].references
        reported_variant_allele = sapio_mock.get_data_of_type(Allele)[reported_variant.allele_id]
        assert reported_variant_allele.model_dump(
            exclude={"RecordId"}
        ) == example.reported_variants[0].allele.model_dump(exclude={"transcript_hgvs"})
        reported_variant_allele_transcript_hgvs = [
            transcript_hgvs.model_dump(exclude={"RecordId", "allele_id"})
            for transcript_hgvs in sapio_mock.get_data_of_type(TranscriptHGVS).values()
            if transcript_hgvs.allele_id == reported_variant_allele.RecordId
        ]
        assert reported_variant_allele_transcript_hgvs == [
            transcript_hgvs.model_dump()
            for transcript_hgvs in example.reported_variants[0].allele.transcript_hgvs
        ]
        workflow_step_change = sapio_mock.get_data_of_type(EllaWorkflowStepChange)[
            reported_variant.workflow_step_change_id
        ]
        workflow_step_change.analysis_id = analysis_id
        assert workflow_step_change.model_dump(
            exclude={"RecordId", "analysis_id"}
        ) == example.model_dump(
            exclude={
                "external_id",
                "reported_variants",
                "not_relevant_variants",
                "needs_verification",
            }
        )
        not_relevant_variants = [
            variant
            for variant in sapio_mock.get_data_of_type(NotRelevantVariant).values()
            if variant.workflow_step_change_id == workflow_step_change.RecordId
        ]
        assert len(not_relevant_variants) == len(example.not_relevant_variants)
        [not_relevant_variant] = not_relevant_variants
        assert not_relevant_variant.model_dump(
            exclude={"RecordId", "workflow_step_change_id", "allele_id"}
        ) == example.not_relevant_variants[0].model_dump(
            exclude={"allele", "acmg_criteria", "references"}
        )
        not_relevant_variant_acmg_criteria = [
            acmg_criterium.model_dump(exclude={"RecordId", "variant_id"})
            for acmg_criterium in sapio_mock.get_data_of_type(ACMG).values()
            if acmg_criterium.variant_id == not_relevant_variant.RecordId
        ]
        assert not_relevant_variant_acmg_criteria == [
            acmg_criterium.model_dump()
            for acmg_criterium in example.not_relevant_variants[0].acmg_criteria
        ]
        not_relevant_variant_references = [
            reference.reference
            for reference in sapio_mock.get_data_of_type(VariantReference).values()
            if reference.variant_id == not_relevant_variant.RecordId
        ]
        assert not_relevant_variant_references == example.not_relevant_variants[0].references
        not_relevant_variant_allele = sapio_mock.get_data_of_type(Allele)[
            not_relevant_variant.allele_id
        ]
        assert not_relevant_variant_allele.model_dump(
            exclude={"RecordId"}
        ) == example.not_relevant_variants[0].allele.model_dump(exclude={"transcript_hgvs"})
        not_relevant_variant_allele_transcript_hgvs = [
            transcript_hgvs.model_dump(exclude={"RecordId", "allele_id"})
            for transcript_hgvs in sapio_mock.get_data_of_type(TranscriptHGVS).values()
            if transcript_hgvs.allele_id == not_relevant_variant_allele.RecordId
        ]
        assert not_relevant_variant_allele_transcript_hgvs == [
            transcript_hgvs.model_dump()
            for transcript_hgvs in example.not_relevant_variants[0].allele.transcript_hgvs
        ]
        needs_verifications = [
            needs_verification
            for needs_verification in sapio_mock.get_data_of_type(NeedsVerification).values()
            if needs_verification.workflow_step_change_id == workflow_step_change.RecordId
        ]
        assert len(needs_verifications) == len(example.needs_verification)
        [needs_verification] = needs_verifications
        assert needs_verification.model_dump(
            exclude={"RecordId", "workflow_step_change_id", "allele_id"}
        ) == example.needs_verification[0].model_dump(exclude={"allele"})
        needs_verification_allele = sapio_mock.get_data_of_type(Allele)[
            needs_verification.allele_id
        ]
        assert needs_verification_allele.model_dump(
            exclude={"RecordId"}
        ) == example.needs_verification[0].allele.model_dump(exclude={"transcript_hgvs"})
        needs_verification_transcript_hgvs = [
            transcript_hgvs.model_dump(exclude={"RecordId", "allele_id"})
            for transcript_hgvs in sapio_mock.get_data_of_type(TranscriptHGVS).values()
            if transcript_hgvs.allele_id == needs_verification_allele.RecordId
        ]
        assert needs_verification_transcript_hgvs == [
            transcript_hgvs.model_dump()
            for transcript_hgvs in example.needs_verification[0].allele.transcript_hgvs
        ]
        assert reported_variant_allele.model_dump() == needs_verification_allele.model_dump()
        assert ordered_workflows.qsize() == 0
    finally:
        zappy_task.cancel()
        with pytest.raises(asyncio.CancelledError):
            await asyncio.wait_for(zappy_task, 5)
        steps_task.cancel()
        with pytest.raises(asyncio.CancelledError):
            await asyncio.wait_for(steps_task, 10)
        await sapio_mock.cleanup()

import asyncio

from nats.aio.msg import Msg
from schemas.models import WorkflowStepChange
from zappy.models import EllaInput

from maestro.nats.nats import NATSSession


class EllaMock:
    STREAM_NAME = "ella"
    INPUT_SUBJECT = "input"
    OUTPUT_SUBJECT = "output"

    def __init__(self):
        self.received_input: asyncio.Queue[EllaInput] = asyncio.Queue()

    async def setup(self):
        async with NATSSession() as nats_session:
            await nats_session.add_or_update_stream(
                self.STREAM_NAME, [self.INPUT_SUBJECT, self.OUTPUT_SUBJECT]
            )

    async def run(self):
        async with NATSSession() as nats_session:
            await nats_session.subscribe(self.STREAM_NAME, self.INPUT_SUBJECT, self.handle_input)
            while True:
                await asyncio.sleep(1)

    async def handle_input(self, msg: Msg):
        input = EllaInput.model_validate_json(msg.data.decode())
        self.received_input.put_nowait(input)
        await msg.ack()

    async def send_workflow_step_change(self, message: WorkflowStepChange):
        async with NATSSession() as nats_session:
            await nats_session.publish(
                self.OUTPUT_SUBJECT, message.model_dump_json().encode(), self.STREAM_NAME
            )

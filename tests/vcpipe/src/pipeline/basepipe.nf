process mockup {
    publishDir params.output, saveAs: {
        if (it ==~ /.*chrMT.*/) {
            "${params.output}/data/mtdna/${it}"
        }
        else if (it ==~ /.*\.bigWig|merged.*\.vcf/) {
            "${params.output}/data/cnv/${it}"
        }
        else if (it ==~ /.*(\.qc_result\.json|\.qc_report\.md|\.qc_warnings\.md)/) {
            "${params.output}/data/qc/${it}"
        }
        else if (it ==~ /.*(\.ba[mi]|\.cram|\.cram\.crai)/) {
            "${params.output}/data/mapping/${it}"
        }
        else if (it ==~ /.*\.vcf(\..*)?/) {
            "${params.output}/data/variantcalling/${it}"
        }
    }, mode: 'copy'


    output:
    file "*.final.vcf"
    file "*.g.vcf.gz"
    file "*.g.vcf.gz.tbi"
    file "*.bam"
    file "*.bai"
    file "*.qc_result.json"
    file "*.qc_report.md"
    file "*.qc_warnings.md"
    file "*.bigWig"
    file "merged*.vcf"
    file "*chrMT.final.vcf.gz"
    file "*chrMT.final.vcf.gz.tbi"

    script:
    sample_id_prefix = params.analysis_name.split("_")[2].substring(0,2)

    """
    touch "${params.analysis_name}.final.vcf"
    touch "${params.analysis_name}.g.vcf.gz"
    touch "${params.analysis_name}.g.vcf.gz.tbi"
    touch "${params.analysis_name}.bam"
    touch "${params.analysis_name}.bai"
    touch "${params.analysis_name}.qc_result.json"
    touch "${params.analysis_name}.qc_report.md"
    touch "${params.analysis_name}.qc_warnings.md"
    touch "${params.analysis_name}.bigWig"
    touch "merged${params.analysis_name}.vcf"
    touch "${params.analysis_name}chrMT.final.vcf.gz"
    touch "${params.analysis_name}chrMT.final.vcf.gz.tbi"

    sleep 5

    if [[ "${sample_id_prefix}" == "FB" ]]; then
        exit 140
    fi
    """
}
workflow {
  mockup()
}

process mockup {
    publishDir params.output, saveAs: {
        if (it ==~ /.*chrMT.*/) {
            "${params.output}/data/mtdna/${it}"
        }
        else if (it ==~ /.*(\.qc_result\.json|\.qc_report\.md|\.qc_warnings\.md)/) {
            "${params.output}/data/qc/${it}"
        }
        else if (it ==~ /.*\.vcf(\..*)?/) {
            "${params.output}/data/variantcalling/${it}"
        }
    }, mode: 'copy'


    output:
    file "*.all.final.vcf"
    file "*.all.filter.chrMT.vcf"
    file "*.qc_result.json"
    file "*.qc_report.md"
    file "*.qc_warnings.md"

    script:
    sample_id_prefix = params.analysis_name.split("_")[2].substring(0,2)

    """
    touch ${params.analysis_name}.all.final.vcf
    touch ${params.analysis_name}.all.filter.chrMT.vcf
    touch ${params.analysis_name}.qc_result.json
    touch ${params.analysis_name}.qc_report.md
    touch ${params.analysis_name}.qc_warnings.md

    sleep 5

    if [[ "${sample_id_prefix}" == "FT" ]]; then
        exit 140
    fi
    """
}
workflow {
  mockup()
}

process mockup {
    script:
    sample_id_prefix = params.analysis_name.split("_")[2].substring(0,2)
  
    """
    mkdir -p ${params.output}/ella
    tar cf ${params.output}/ella.tar ${params.output}/ella

    sleep 5

    if [[ "${sample_id_prefix}" == "FA" ]]; then
        exit 140
    fi
    """
}
workflow {
  mockup()
}

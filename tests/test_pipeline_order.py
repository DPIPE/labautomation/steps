import json
import re
import string
from collections.abc import Callable
from typing import Any, Literal
from uuid import UUID

import hypothesis as ht
import hypothesis.strategies as st
import pytest
from artefacts.base import StepsBaseArtefact
from artefacts.pipeline_order import (
    SAMPLE_ID_PATTERN,
    InputArtefactType,
    InputBase,
    PipelineOrder,
    PipelineType,
)
from artefacts.seqdata import GivenSexEnum, PriorityEnum
from jinja2 import Environment, FileSystemLoader
from pydantic import BaseModel, ValidationError, model_validator
from utils import Identifiers

from tests.data.generate_test_data import (
    BASEPIPE_TEMPLATE,
    GENEPANEL_VERSIONS,
    GENEPANELS,
    JINJA2_TEMPLATE_DIR,
    SAMPLE_TAG,
    SINGLE_ANNOPIPE_TEMPLATE,
    TRIO_ANNOPIPE_TEMPLATE,
    TRIOPIPE_TEMPLATE,
    generate_sample_id,
)
from tests.data.generate_test_data import (
    FILENAME_SEPARATOR as SEPARATOR,
)

JINJA_ENV = Environment(loader=FileSystemLoader(JINJA2_TEMPLATE_DIR))
ALPHABET = string.ascii_letters + string.digits


class PipelineOrderSampler:
    """Helper class for generating pipeline order data with or without errors."""

    def __init__(self, draw: st.DrawFn):
        self.draw = draw

    def draw_genepanel(self) -> str:
        return self.draw(st.sampled_from(GENEPANELS))

    def draw_genepanel_version(self, with_errors: bool = False) -> str:
        if with_errors:
            invalid_collection = st.text(alphabet=ALPHABET).filter(
                lambda x: not re.match(r"\d+\.\d+\.\d+", x)
            )
            genepanel_version = self.draw(invalid_collection)
        else:
            genepanel_version = self.draw(st.sampled_from(GENEPANEL_VERSIONS))
        return genepanel_version

    def draw_given_sex(self, with_errors: bool = False) -> str:
        if with_errors:
            invalid_collection = st.text(alphabet=string.ascii_letters).filter(
                lambda x: x not in GivenSexEnum
            )
            given_sex = self.draw(invalid_collection)
        else:
            given_sex = self.draw(st.sampled_from(GivenSexEnum))
        return given_sex

    def draw_priority(self, with_errors: bool = False) -> int:
        if with_errors:
            invalid_collection = st.integers().filter(
                lambda x: x not in [item.value for item in PriorityEnum]
            )
            priority = self.draw(invalid_collection)
        else:
            priority = self.draw(st.sampled_from(PriorityEnum)).value

        return priority

    def draw_project_id(self) -> str:
        prefix = self.draw(st.sampled_from(["wgs", "EKG"]))
        suffix = self.draw(st.integers(min_value=1))
        return f"{prefix}{suffix}"

    def draw_sample_id(self, with_errors: bool = False) -> str:
        if with_errors:
            sample_id = self.draw(
                st.text(alphabet=ALPHABET).filter(lambda x: not re.match(SAMPLE_ID_PATTERN, x))
            )
        else:
            prefix = self.draw(st.sampled_from(SAMPLE_TAG))
            index = self.draw(st.integers(min_value=1))
            sample_id = generate_sample_id(prefix, index)

        return sample_id

    def draw_taqman(self) -> bool:
        return self.draw(st.booleans())

    def draw_uuid(self, with_errors: bool = False) -> str:
        if with_errors:
            uuid = self.draw(st.text(alphabet=ALPHABET))
        else:
            uuid = str(self.draw(st.uuids(version=4)))

        return uuid


def generate_name_template(
    base_name: str,
    sample_or_proband_id: str,
    genepanel_name: str,
    genepanel_version: str,
    pipeline_uuid: str,
    is_trio: bool,
    is_anno: bool,
) -> str:
    """Generate name template for the pipeline order."""

    if is_trio:
        suffix = "_TRIO"
    else:
        suffix = ""

    if is_anno:
        return f"{base_name}_{sample_or_proband_id}{suffix}_{genepanel_name}_{genepanel_version}_{pipeline_uuid}"
    else:
        return f"{base_name}_{sample_or_proband_id}{suffix}_{pipeline_uuid}"


def populate_render_args(
    render_args: dict[str, Any],
    sampler: PipelineOrderSampler,
    base_name: str,
    is_trio: bool,
) -> None:
    """Populate render_args with random values."""

    sample_or_proband_id = render_args["sample_or_proband_id"]
    sample_or_proband_uuid = render_args["sample_or_proband_uuid"]

    if is_trio:
        render_args["pb_sample"] = SEPARATOR.join(
            [base_name, sample_or_proband_id, sample_or_proband_uuid]
        )
        render_args["fm_sample"] = SEPARATOR.join(
            [base_name, render_args["fm_sample_id"], sampler.draw_uuid()]
        )
        render_args["mk_sample"] = SEPARATOR.join(
            [base_name, render_args["mk_sample_id"], sampler.draw_uuid()]
        )
        render_args["pb_given_sex"] = render_args.pop("given_sex")
    else:
        render_args["sample"] = SEPARATOR.join(
            [base_name, sample_or_proband_id, sample_or_proband_uuid]
        )
        render_args["taqman"] = sampler.draw_taqman()

    render_args["workflow_name"] = SEPARATOR.join(
        [
            base_name,
            sample_or_proband_id,
            render_args["genepanel_name"],
            render_args["genepanel_version"],
        ]
    )


def inject_error(
    sampler: PipelineOrderSampler, render_args: dict[str, Any], is_trio: bool, is_anno: bool
) -> None:
    """Injects random errors into the render_args"""

    error_args: dict[str, Callable] = {
        "sample_or_proband_id": sampler.draw_sample_id,
        "pipeline_uuid": sampler.draw_uuid,
        "sample_or_proband_uuid": sampler.draw_uuid,
        "priority": sampler.draw_priority,
        "workflow_id": sampler.draw_uuid,
        "given_sex": sampler.draw_given_sex,
    }

    if is_trio:
        error_args.update(
            {
                "fm_sample_id": sampler.draw_sample_id,
                "mk_sample_id": sampler.draw_sample_id,
            }
        )

    if is_anno:
        error_args.update(
            {
                "genepanel_version": sampler.draw_genepanel_version,
            }
        )

    # Select random field and inject the error
    key = sampler.draw(st.sampled_from(list(error_args.keys())))
    error_field = error_args[key](True)

    # Update constituent of composite fields
    if key == "sample_or_proband_id":
        render_args["sample_or_proband_id"] = error_field
    elif key == "pipeline_uuid":
        render_args["pipeline_uuid"] = error_field
    elif key == "sample_or_proband_uuid":
        render_args["sample_or_proband_uuid"] = error_field
    else:
        render_args[key] = error_field


@st.composite
def dotanalysis_contents(
    draw: st.DrawFn,
    pipeline_st: st.SearchStrategy[PipelineType],
    is_trio: bool = False,
    with_errors: bool = True,
) -> tuple[dict, bool]:
    """Generate arbitrary .analysis file contents with or without errors."""

    pipeline = draw(pipeline_st)
    assert not (
        is_trio and pipeline == PipelineType.basepipe
    ), f"Cannot combine {PipelineType.basepipe} with trio"

    if with_errors:
        add_errors = draw(st.booleans())
    else:
        add_errors = False

    is_anno = pipeline == PipelineType.annopipe

    # Sample valid fields
    sampler = PipelineOrderSampler(draw)

    owner_id = "OUSAMG"
    project_id = sampler.draw_project_id()
    genepanel_name = sampler.draw_genepanel()
    genepanel_version = sampler.draw_genepanel_version()
    base_name = SEPARATOR.join([owner_id, project_id])
    render_args = {
        "priority": sampler.draw_priority(),
        "pipeline_uuid": sampler.draw_uuid(),
        "sample_or_proband_id": sampler.draw_sample_id(),
        "sample_or_proband_uuid": sampler.draw_uuid(),
        "genepanel_name": genepanel_name,
        "genepanel_version": genepanel_version,
        "given_sex": sampler.draw_given_sex(),
        "workflow_id": sampler.draw_uuid(),
        "fm_sample_id": sampler.draw_sample_id(),
        "mk_sample_id": sampler.draw_sample_id(),
    }

    # Add a random error to the render_args
    if add_errors:
        inject_error(sampler, render_args, is_trio, is_anno)

    sample_or_proband_id = render_args["sample_or_proband_id"]
    pipeline_uuid = render_args["pipeline_uuid"]

    # Generate basepipe, triopipe, or annopipe
    template = (
        (TRIO_ANNOPIPE_TEMPLATE if is_trio else SINGLE_ANNOPIPE_TEMPLATE)
        if is_anno
        else (TRIOPIPE_TEMPLATE if is_trio else BASEPIPE_TEMPLATE)
    )
    name_template = generate_name_template(
        base_name,
        sample_or_proband_id,  # type: ignore[arg-type]
        genepanel_name,
        genepanel_version,
        pipeline_uuid,  # type: ignore[arg-type]
        is_trio,
        is_anno,
    )
    populate_render_args(render_args, sampler, base_name, is_trio)

    render_args["name"] = name_template.format(
        sample_or_proband_id=sample_or_proband_id,
        pipeline_uuid=pipeline_uuid,
    )

    # Render template and generate dotanalysis contents
    jinja_template = JINJA_ENV.get_template(template)
    content = jinja_template.render(render_args)
    dotanalysis: dict = json.loads(content)

    return dotanalysis, add_errors


@ht.example(
    dotanalysis_tuple=(
        {
            "priority": 3,
            "params": {
                "name": "OUSAMG_EKG2360_FBS0000112_d4767e97-8939-42f0-86c3-32fd4482c533",
                "specialized_pipeline": "Dragen",
                "taqman": "True",
                "workflow_id": "53d8d468-9f62-45c2-b5ae-99e79bb6218d",
                "workflow_name": "OUSAMG_EKG2360_FBS0000112_OCD_v2.1.0",
            },
            "samples": ["OUSAMG_EKG2360_FBS0000112_c648f20b-2e35-42aa-8ff4-a89eba770a06"],
            "gender": "male",
            "type": "basepipe",
            "export_priority": 1,
            "date_analysis_requested": "2024-01-26",
        },
        False,
    )
)
@ht.example(
    dotanalysis_tuple=(
        {
            "priority": 3,
            "params": {
                "name": "OUSAMG_wgs87_NS36757081_TLO4KOPC13NL273l4",  # invalid pipeline_uuid
                "specialized_pipeline": "Dragen",
                "taqman": "True",
                "workflow_id": "470b9805-d2d6-4877-bdc5-9a3ad035d259",
                "workflow_name": "OUSAMG_wgs87_NS36757081_RAS_v2.1.0",
            },
            "samples": ["OUSAMG_wgs87_NS36757081_766bad07-34c2-4a80-83cc-0f2793fdcab8"],
            "gender": "male",
            "type": "basepipe",
            "export_priority": 1,
            "date_analysis_requested": "2024-01-26",
        },
        True,
    )
)
@ht.given(
    dotanalysis_contents(
        pipeline_st=st.just(PipelineType.basepipe),
    )
)
def test_basepipe_order(dotanalysis_tuple: tuple[dict, bool]) -> None:
    """Test basepipe PipelineOrder model."""

    dotanalysis, with_errors = dotanalysis_tuple

    if with_errors:
        with pytest.raises(ValidationError):
            PipelineOrder.validate_python(dotanalysis)
    else:
        PipelineOrder.validate_python(dotanalysis)


@ht.example(
    dotanalysis_tuple=(
        {
            "priority": 1,
            "params": {
                "genepanel": "DMD_v2.1.0",
                "name": "OUSAMG_EKG15434_NT65662992_DMD_v2.1.0_7988573d-7014-4234-97d2-e7414dcdd1cd",
                "specialized_pipeline": "Dragen",
                "workflow_id": "859957b2-3804-4a61-aea4-00fa63fb8e04",
                "workflow_name": "OUSAMG_EKG15434_NT65662992_DMD_v2.1.0",
            },
            "samples": ["OUSAMG_EKG15434_NT65662992_5bf1eadb-68fc-48ff-bc73-3c6092300907"],
            "gender": "male",
            "type": "annopipe",
            "export_priority": 1,
            "date_analysis_requested": "2023-12-27",
        },
        False,
    )
)
@ht.example(
    dotanalysis_tuple=(
        {
            "priority": 1,
            "params": {
                "genepanel": "DMD_v2.1.0",
                "name": "OUSAMG_EKG15434_NT65662992_DMD_v2.1.0_7988573d-7014-4234-97d2-e7414dcdd1cd",
                "specialized_pipeline": "Dragen",
                "workflow_id": "859957b2-3804-4a61-aea4-00fa63fb8e04",
                "workflow_name": "OUSAMG_EKG15434_NT65662992_DMD_v2.1.0",
            },
            "samples": ["OUSAMG_EKG15434_NT65662992_5bf1eadb-68fc-48ff-bc73-3c6092300907"],
            "gender": "male",
            "type": "invalidpipe",  # invalid pipeline type
            "export_priority": 1,
            "date_analysis_requested": "2023-12-27",
        },
        True,
    )
)
@ht.given(
    dotanalysis_contents(
        pipeline_st=st.just(PipelineType.annopipe),
    )
)
def test_anno_single_order(dotanalysis_tuple: tuple[dict, bool]) -> None:
    """Test annopipe single PipelineOrder model."""

    dotanalysis, with_errors = dotanalysis_tuple

    if with_errors:
        with pytest.raises(ValidationError):
            PipelineOrder.validate_python(dotanalysis)
    else:
        PipelineOrder.validate_python(dotanalysis)


@ht.example(
    dotanalysis_tuple=(
        {
            "priority": 2,
            "params": {
                "name": "OUSAMG_wgs26263_PSS372764528_TRIO_51e495aa-cf6d-4d70-81c3-934568ca86d5",
                "pedigree": {
                    "proband": {
                        "sample": "OUSAMG_wgs26263_PSS372764528_f4b834a1-0338-47a2-aef0-8e117af01178",
                        "gender": "male",
                    },
                    "father": {
                        "sample": "OUSAMG_wgs26263_FAS372764528_03c71ca8-433d-4b34-a802-c6a2bd903da9",
                        "gender": "male",
                    },
                    "mother": {
                        "sample": "OUSAMG_wgs26263_FTT0026263_f5527972-5563-453c-b397-ec797c59fd62",
                        "gender": "female",
                    },
                },
                "specialized_pipeline": "Dragen",
                "workflow_id": "efdb05e4-221a-488f-8b26-d98fcc3c0c2f",
                "workflow_name": "OUSAMG_wgs26263_PSS372764528_HBOC_v1.0.0",
            },
            "samples": [
                "OUSAMG_wgs26263_PSS372764528_f4b834a1-0338-47a2-aef0-8e117af01178",
                "OUSAMG_wgs26263_FAS372764528_03c71ca8-433d-4b34-a802-c6a2bd903da9",
                "OUSAMG_wgs26263_FTT0026263_f5527972-5563-453c-b397-ec797c59fd62",
            ],
            "type": "triopipe",
            "export_priority": 2,
        },
        False,
    )
)
@ht.example(
    dotanalysis_tuple=(
        {
            "priority": 2,
            "params": {
                "name": "OUSAMG_wgs26263_PSS372764528_TRIO_51e495aa-cf6d-4d70-81c3-934568ca86d5",
                "pedigree": {
                    "proband": {
                        "sample": "OUSAMG_wgs26263_PSS372764528_f4b834a1-0338-47a2-aef0-8e117af01178",
                        "gender": "male",
                    },
                    "father": {
                        "sample": "OUSAMG_wgs26263_FAS372764528_03c71ca8-433d-4b34-a802-c6a2bd903da9",
                        "gender": "male",
                    },
                    "mother": {
                        "sample": "OUSAMG_wgs26263_FTT0026263_f5527972-5563-453c-b397-ec797c59fd62",
                        "gender": "somegender",  # invalid gender
                    },
                },
                "specialized_pipeline": "Dragen",
                "workflow_id": "efdb05e4-221a-488f-8b26-d98fcc3c0c2f",
                "workflow_name": "OUSAMG_wgs26263_PSS372764528_HBOC_v1.0.0",
            },
            "samples": [
                "OUSAMG_wgs26263_PSS372764528_f4b834a1-0338-47a2-aef0-8e117af01178",
                "OUSAMG_wgs26263_FAS372764528_03c71ca8-433d-4b34-a802-c6a2bd903da9",
                "OUSAMG_wgs26263_FTT0026263_f5527972-5563-453c-b397-ec797c59fd62",
            ],
            "type": "triopipe",
            "export_priority": 2,
        },
        True,
    )
)
@ht.given(
    dotanalysis_contents(
        pipeline_st=st.just(PipelineType.triopipe),
        is_trio=True,
    )
)
def test_triopipe_order(dotanalysis_tuple: tuple[dict, bool]) -> None:
    """Test triopipe PipelineOrder model."""

    dotanalysis, with_errors = dotanalysis_tuple

    if with_errors:
        with pytest.raises(ValidationError):
            PipelineOrder.validate_python(dotanalysis)
    else:
        PipelineOrder.validate_python(dotanalysis)


@ht.example(
    dotanalysis_tuple=(
        {
            "priority": 2,
            "params": {
                "genepanel": "OCD_v1.0.0",
                "name": "OUSAMG_EKG1884_NT87960284_TRIO_OCD_v1.0.0_662514c2-2b3d-43bc-a2ad-323ca5cfcd77",
                "pedigree": {
                    "proband": {
                        "sample": "OUSAMG_EKG1884_NT87960284_d3eb0323-8f15-4a31-90d3-45cbad59aee3",
                        "gender": "male",
                    },
                    "father": {
                        "sample": "OUSAMG_EKG1884_NS51307183_6dc89bb6-15cd-46e6-b084-8475faade33e",
                        "gender": "male",
                    },
                    "mother": {
                        "sample": "OUSAMG_EKG1884_NS30992063_5e081ac4-7c10-4cee-bc18-14cc910df08a",
                        "gender": "female",
                    },
                },
                "specialized_pipeline": "Dragen",
                "workflow_id": "082cd4f5-4a93-48ef-b464-f64acd4883de",
                "workflow_name": "OUSAMG_EKG1884_NT87960284_OCD_v1.0.0",
            },
            "samples": [
                "OUSAMG_EKG1884_NT87960284_d3eb0323-8f15-4a31-90d3-45cbad59aee3",
                "OUSAMG_EKG1884_NS51307183_6dc89bb6-15cd-46e6-b084-8475faade33e",
                "OUSAMG_EKG1884_NS30992063_5e081ac4-7c10-4cee-bc18-14cc910df08a",
            ],
            "gender": "male",
            "type": "annopipe",
            "export_priority": 2,
            "date_analysis_requested": "2024-01-29",
        },
        False,
    )
)
@ht.example(
    dotanalysis_tuple=(
        {
            "priority": 2,
            "params": {
                "genepanel": "OCD_v1.0.0",
                "name": "OUSAMG_EKG1884_NT87960284_TRIO_OCD_v1.0.0_662514c2-2b3d-43bc-a2ad-323ca5cfcd77",
                "pedigree": {
                    "proband": {
                        "sample": "OUSAMG_EKG1884_NT87960284_d3eb0323-8f15-4a31-90d3-45cbad59aee3",
                        "gender": "male",
                    },
                    "father": {
                        "sample": "OUSAMG_EKG1884_NS51307183_6dc89bb6-15cd-46e6-b084-8475faade33e",
                        "gender": "male",
                    },
                    "mother": {
                        "sample": "OUSAMG_EKG1884_NS30992063_5e081ac4-7c10-4cee-bc18-14cc910df08a",
                        "gender": "female",
                    },
                },
                "specialized_pipeline": "Dragen",
                "workflow_id": "082cd4f5-4a93-48ef-b464-f64acd4883de",
                "workflow_name": "OUSAMG_EKG1884_NT87960284_OCD_v1.0.0",
            },
            "samples": [
                "OUSAMG_EKG1884_NT87960284_d3eb0323-8f15-4a31-90d3-45cbad59aee3",
                "OUSAMG_EKG1884_NS51307183_6dc89bb6-15cd-46e6-b084-8475faade33e",
                "OUSAMG_EKG1884_NS30992063_5e081ac4-7c10-4cee-bc18-14cc910df08a",
                "OUSAMG_EKG1884_NS30992063_5e081ac4-7c10-4cee-bc18-14cc910df08b",  # additional sample
            ],
            "gender": "male",
            "type": "annopipe",
            "export_priority": 2,
            "date_analysis_requested": "2024-01-29",
        },
        True,
    )
)
@ht.given(
    dotanalysis_contents(
        pipeline_st=st.just(PipelineType.triopipe),
        is_trio=True,
    )
)
def test_anno_trio_order(dotanalysis_tuple: tuple[dict, bool]) -> None:
    """Test annopipe trio PipelineOrder model."""

    dotanalysis, with_errors = dotanalysis_tuple

    if with_errors:
        with pytest.raises(ValidationError):
            PipelineOrder.validate_python(dotanalysis)
    else:
        PipelineOrder.validate_python(dotanalysis)


@ht.given(
    dotanalysis_contents(
        pipeline_st=st.sampled_from([PipelineType.triopipe, PipelineType.annopipe]),
        is_trio=True,
        with_errors=False,
    )
)
def test_pedigree_json(dotanalysis_tuple: tuple[dict, bool]) -> None:
    """Ensure that pedigree_json is serialized to valid JSON."""

    dotanalysis, _ = dotanalysis_tuple
    pipeline_order = PipelineOrder.validate_python(dotanalysis)
    serialized_pipeline_order = pipeline_order.model_dump()

    if pipeline_order.pipeline_type == PipelineType.basepipe:
        assert serialized_pipeline_order["params"]["pedigree_json"] is None
    else:
        assert json.loads(serialized_pipeline_order["params"]["pedigree_json"])


class ArtefactA(StepsBaseArtefact):
    seqdata_id: UUID


class ArtefactB(StepsBaseArtefact):
    seqdata_id: UUID


@ht.given(
    dotanalysis_contents(
        pipeline_st=st.sampled_from(PipelineType),
        with_errors=False,
    ),
    st.sampled_from(["A", "B"]),
)
def test_input_artefact_tuples(
    dotanalysis_tuple: tuple[dict, bool], artefact_type: Literal["A", "B"]
) -> None:
    """Ensure that input artefact tuple field of inputs must be constructed beforehand in the
    @model_validator whenever field is a union of artefacts."""

    class TestInput(InputBase):
        artefact: InputArtefactType[ArtefactA] | InputArtefactType[ArtefactB]

    class ValidPipeOrder(BaseModel):
        inputs: TestInput

        @model_validator(mode="before")
        @classmethod
        def validate_model(cls, data: dict) -> dict:
            """Create tuple artefact beforehand."""

            sample = data["samples"][0]
            identifiers = Identifiers.from_string(sample)
            artefact_class = ArtefactA if artefact_type == "A" else ArtefactB
            data["inputs"] = {"artefact": (artefact_class, {"seqdata_id": identifiers.seqdata_id})}

            return data

    class InvalidPipeOrder(BaseModel):
        inputs: TestInput

        @model_validator(mode="before")
        @classmethod
        def validate_model(cls, data: dict) -> dict:
            """Do not create tuple artefact beforehand."""

            sample = data["samples"][0]
            identifiers = Identifiers.from_string(sample)
            data["inputs"] = {"artefact": {"seqdata_id": identifiers.seqdata_id}}

            return data

    dotanalysis, _ = dotanalysis_tuple

    # Tuple artefact field correctly prepared in @model_validator as artefact class cannot
    # be inferred from field annotation of ValidInput.artefact alone.
    ValidPipeOrder.model_validate(dotanalysis)

    # Incorrectly do not prepare tuple artefact fields in @model_validator.
    with pytest.raises(ValidationError):
        InvalidPipeOrder.model_validate(dotanalysis)

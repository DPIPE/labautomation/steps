import asyncio

import pytest
from maestro import run_workflow_steps
from maestro.neo4j.neo4j import Neo4jTransaction
from workflowsteps.annopipe import Annopipe
from workflowsteps.basepipe import Basepipe
from workflowsteps.dotanalysiswatcher import DotanalysisFileWatcher
from workflowsteps.dotsamplewatcher import DotsampleFileWatcher
from workflowsteps.fastqwatcher import FASTQFileWatcher, SeqDataMaker
from workflowsteps.taqmanwatcher import TaqmanFileWatcher
from workflowsteps.triopipe import Triopipe

from tests.data import generate_test_data


async def get_job_status_counts():
    d = dict()
    async with Neo4jTransaction() as neo4j_tx:
        res = await neo4j_tx.query(
            "MATCH (n:Job) RETURN DISTINCT n.status, n.workflow_step, count(n)"
        )
        for r in res:
            if r["n.status"] not in d:
                d[r["n.status"]] = dict()
            d[r["n.status"]][r["n.workflow_step"]] = r["count(n)"]
            d[r["n.status"]]["total"] = d[r["n.status"]].get("total", 0) + r["count(n)"]
        return dict(d)


@pytest.mark.destructive  # Test is destructive and will be skipped by default
async def test_generate_test_data(tmp_path, truncate_database, mock_vcpipe):
    # Configure file watchers to use temporary directory.
    file_watch_dir = tmp_path
    file_watcher_classes = [
        DotsampleFileWatcher,
        FASTQFileWatcher,
        TaqmanFileWatcher,
        DotanalysisFileWatcher,
    ]
    for file_watcher_class in file_watcher_classes:
        file_watcher_class.path = file_watch_dir

    # Start Steps/Maestro.
    task = await run_workflow_steps(
        message_watcher_classes=[SeqDataMaker, Basepipe, Triopipe, Annopipe],
        file_watcher_classes=file_watcher_classes,
    )
    # Allow file watchers to reach awatch().
    await asyncio.sleep(0)

    # Generate test data.
    generate_test_data.TEST_DIR = file_watch_dir
    expected_job_counts = generate_test_data.generate_test_data(
        num_singles=6,
        num_pending_s_basepipe=1,
        num_fail_s_basepipe=1,
        num_fail_s_annopipe=1,
        num_pending_s_seqdatamaker=1,
        num_trios=5,
        num_pending_t_basepipe=2,
        num_fail_t_basepipe=2,
        num_fail_triopipe=1,
        num_fail_t_annopipe=1,
        num_pending_t_seqdatamaker=1,
    )

    # Wait long enough for system to get warmed up.
    await asyncio.sleep(5)

    # The full runtime has been measured to be around 200s, so 600s should be comfortable unless
    # system is under considerable load or less powerful than my development machine.
    async with asyncio.timeout(600):
        last_status_counts = {}
        while True:
            # If Steps/Maestro termintes (due to an exception), keep an eye on that here, to avoid
            # having to reach the timeout before failing the test.
            if task.done():
                await task
                raise RuntimeError(
                    "Steps/Maestro terminated prematurely, but no exception was raised."
                )
            # Get the current job status counts.
            status_counts = await get_job_status_counts()
            # If no jobs are queued or running and the counts are the same as last time,
            # system has probably reached idle state, so end the loop.
            if (
                "queued" not in status_counts
                and "running" not in status_counts
                and status_counts == last_status_counts
            ):
                break
            last_status_counts = status_counts
            await asyncio.sleep(10)

    # Assert the expected number of jobs in each status.
    assert status_counts == expected_job_counts

    # Shutdown Steps/Maestro.
    with pytest.raises(asyncio.CancelledError):
        task.cancel()
        await asyncio.wait_for(task, timeout=10)

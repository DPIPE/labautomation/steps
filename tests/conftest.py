import asyncio
import multiprocessing
import multiprocessing.connection
import os
import signal
from collections.abc import AsyncGenerator, Callable, Generator
from contextlib import contextmanager
from functools import wraps
from pathlib import Path

import git
import pytest
import pytest_asyncio
from dvc.repo import Repo
from dvc.repo.pull import pull
from dvc.utils import glob_targets
from logger import (
    LOGGING_CONFIG,  # noqa: F401
)
from sapiopylib.rest.User import SapioUser
from zappy.sapioclient import SapioClient

from config import config
from maestro.config import Config
from maestro.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.run import shutdown_workflow_steps
from tests.zappy.sapiomock import SapioMock

GIT_ROOT = git.Repo(search_parent_directories=True).git.rev_parse("--show-toplevel")
DVC_CACHE_DIR = os.environ["DVC_CACHE_DIR"]


def clean_dvc() -> None:
    dvc_files = Repo(GIT_ROOT).ls(".", dvc_only=True, recursive=True)
    for f in dvc_files:
        path = Path(GIT_ROOT) / f["path"]
        if path.exists():
            path.unlink()


def dvc_require(targets: list[str | Path]) -> Callable:
    """
    Ensure all files matching the targets list are pulled with dvc

    Globbing is supported, and will pull files/folders recursively.
    """

    def inner(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            if os.environ.get("CI") == "true":
                clean_dvc()

            # Ensure we glob everything recursively, and only pull dvc files
            # The `glob` flag to `pull` doesn't filter out non-dvc files
            def to_recursive_glob(x):
                if Path(x).is_dir():
                    return Path(x) / "**"
                if str(x).endswith("/*"):
                    return Path(x).parent / "**"
                return Path(x)

            globbable_targets = [str(x) for x in map(to_recursive_glob, targets)]
            globbed_targets = [
                x for x in glob_targets(globbable_targets, glob=True) if x.endswith(".dvc")
            ]

            pull(
                Repo(GIT_ROOT, config={"cache": {"dir": DVC_CACHE_DIR}}),
                targets=globbed_targets,
            )
            return func(*args, **kwargs)

        return wrapper

    return inner


@pytest_asyncio.fixture(autouse=True)
async def cleanup_streams():
    # Do not delete Config.NEO4J_SYNC_STREAM - this silently invalidates subscribers
    async with NATSSession() as nats:
        streams = await nats._js.streams_info()
        for stream in streams:
            assert stream.config.name is not None
            if stream.config.name != Config.NEO4J_SYNC_STREAM:
                await nats.delete_stream(stream.config.name)
    yield


@pytest_asyncio.fixture
async def truncate_database():
    async with Neo4jTransaction() as neo4j:
        await neo4j.query(
            "MATCH (n) WHERE NOT n:`__Neo4jMigration` AND NOT n:`__NATSBridge` DETACH DELETE n"
        )
    yield


@pytest.fixture
def mock_vcpipe(monkeypatch):
    # Monkeypatch config and workflowsteps to use mock vcpipe directory
    from workflowsteps import annopipe, basepipe, triopipe

    from config import config, with_mocked_vcpipe

    mocked_config = with_mocked_vcpipe(config)

    # Ensure that the test config is used in basepipe and annopipe
    monkeypatch.setattr(basepipe, "config", mocked_config)
    monkeypatch.setattr(annopipe, "config", mocked_config)
    monkeypatch.setattr(triopipe, "config", mocked_config)


def pytest_addoption(parser):
    parser.addoption(
        "--run-destructive",
        action="store_true",
        default=False,
        help="Run tests that are destructive, e.g. by using truncate_database fixture",
    )


def pytest_collection_modifyitems(config, items):
    if config.getoption("--run-destructive"):
        # If --run-destructive is provided, do not skip any tests
        return
    skip_marker = pytest.mark.skip(
        reason="Skipped by default because it is destructive, e.g. uses the truncate_database fixture. Use --run-destructive to run."
    )
    for item in items:
        if "destructive" in item.keywords:
            item.add_marker(skip_marker)


@pytest_asyncio.fixture(autouse=True)
async def cleanup_workflow_steps():
    yield
    try:
        await asyncio.wait_for(shutdown_workflow_steps(), 10)
    except asyncio.CancelledError:
        pass

    assert asyncio.all_tasks() == {asyncio.current_task()}


@contextmanager
def run_sapio_mock_in_subprocess() -> Generator[str, None]:
    def child_process(pipe: multiprocessing.connection.Connection) -> None:
        async def run_sapio_mock(pipe: multiprocessing.connection.Connection) -> None:
            current_task = asyncio.current_task()
            assert current_task is not None
            asyncio.get_running_loop().add_signal_handler(signal.SIGTERM, current_task.cancel)
            sapio_mock = SapioMock()
            await sapio_mock.start()
            pipe.send(sapio_mock.url)
            try:
                while True:
                    await asyncio.sleep(1)
            except asyncio.CancelledError:
                pass
            await sapio_mock.cleanup()

        asyncio.run(run_sapio_mock(pipe))

    (pipe, child_pipe) = multiprocessing.Pipe(False)
    process = multiprocessing.Process(target=child_process, args=(child_pipe,))
    process.start()
    try:
        yield pipe.recv()
    finally:
        process.terminate()
        process.join()
        assert process.exitcode == 0


def get_real_sapio_config() -> dict[str, str] | None:
    if os.environ.get("SAPIO_REAL", "").lower() in ("true", "yes", "1"):
        # These environment variables are exported from GitLab CI/CD settings.
        # https://gitlab.com/DPIPE/labautomation/steps/-/settings/ci_cd#js-cicd-variables-settings
        return {
            "url": os.environ["SAPIO_API_URL_BASE"],
            "guid": os.environ["SAPIO_APP_KEY"],
            "api_token": os.environ["SAPIO_API_TOKEN"],
            "server_cert_fingerprint": os.environ["SAPIO_API_CERT_FINGERPRINT"],
        }
    else:
        return None


@pytest_asyncio.fixture()
async def sapio_client_readonly(monkeypatch) -> AsyncGenerator[SapioClient]:
    """
    Yield a SapioClient instance that is read-only.

    If the environment variable SAPIO_REAL is set to "true", the client will
    connect to the real Sapio API, or otherwise it will use SapioMock.
    """
    real_sapio_config = get_real_sapio_config()
    if real_sapio_config is not None:
        for key, value in real_sapio_config.items():
            monkeypatch.setattr(config.zappy, f"sapio_{key}", value)
        yield SapioClient(read_only=True)
    else:
        sapio_mock = SapioMock()
        await sapio_mock.start()
        monkeypatch.setattr(config.zappy, "sapio_url", sapio_mock.url)
        try:
            yield SapioClient(read_only=True)
        finally:
            await sapio_mock.cleanup()


@pytest_asyncio.fixture()
async def sapio_client(monkeypatch) -> AsyncGenerator[SapioClient]:
    """
    Yield a SapioClient instance that is read-write and always uses "SapioMock".
    """
    sapio_mock = SapioMock()
    await sapio_mock.start()
    monkeypatch.setattr(config.zappy, "sapio_url", sapio_mock.url)
    try:
        yield SapioClient()
    finally:
        await sapio_mock.cleanup()


@pytest.fixture()
def sapiopylib_client() -> Generator[SapioUser]:
    real_sapio_config = get_real_sapio_config()
    if real_sapio_config is not None:
        yield SapioUser(
            url=real_sapio_config["url"],
            guid=real_sapio_config["guid"],
            api_token=real_sapio_config["api_token"],
            verify_ssl_cert=not real_sapio_config["url"].startswith("https://"),
        )
    else:
        with run_sapio_mock_in_subprocess() as url:
            yield SapioUser(url=url)

import uuid
from datetime import datetime
from pathlib import Path
from subprocess import CalledProcessError

import pytest
import utils
from pydantic import ValidationError

from config import config


def test_run_subprocess():
    cmd = "echo 'hello'"
    return_code = utils.run_subprocess(cmd)
    assert return_code == 0

    cmd = "wrong_command"
    with pytest.raises(CalledProcessError):
        return_code = utils.run_subprocess(cmd)


def test_get_job_output_dir(monkeypatch: pytest.MonkeyPatch):
    monkeypatch.setattr(config.steps, "job_root_dir", Path("/ROOT/DIR"))

    workflow_id = uuid.uuid4()
    job_id = uuid.uuid4()

    dir = utils.get_job_output_dir(
        prefixes=["ousamg", "wgs123", "dna456"],
        workflow_id=workflow_id,
        job_id=job_id,
        step_name="basepipe",
    )
    assert dir == Path(
        f"/ROOT/DIR/{datetime.now().year}/ousamg_wgs123_dna456_{workflow_id!s}/ousamg_wgs123_dna456_basepipe_{job_id!s}"
    )
    dir = utils.get_job_output_dir(
        prefixes=[],
        workflow_id=workflow_id,
        job_id=job_id,
        step_name="stepname",
    )
    assert dir == Path(f"/ROOT/DIR/{datetime.now().year}/{workflow_id!s}/stepname_{job_id!s}")

    with pytest.raises(AssertionError):
        dir = utils.get_job_output_dir(
            prefixes=[],
            workflow_id="non_uuid_workflow_id",  # type: ignore[arg-type]
            job_id=job_id,
            step_name="stepname",
        )

    with pytest.raises(AssertionError):
        dir = utils.get_job_output_dir(
            prefixes=[],
            workflow_id=workflow_id,
            job_id="non_uuid_job_id",  # type: ignore[arg-type]
            step_name="stepname",
        )


def test_identifiers():
    UUID = uuid.UUID("2f08b36a-b3e3-406b-9c6d-e8e47d20d922")
    SEQDATA = Path(f"OUSAMG_wgs123_HG123456_{UUID}")
    p1 = SEQDATA / "some_arbitrary_file_here"

    # When passed a path, it should fetch the identifiers from the parent directory
    assert utils.Identifiers.from_path(p1) == utils.Identifiers(
        owner_id="OUSAMG",
        batch_id="wgs123",
        dna_id="HG123456",
        project_type="wgs",
        seqdata_id=UUID,
    )

    # When passed a string, it should fetch the identifiers from the string itself
    assert utils.Identifiers.from_string(str(SEQDATA)) == utils.Identifiers(
        owner_id="OUSAMG",
        batch_id="wgs123",
        dna_id="HG123456",
        project_type="wgs",
        seqdata_id=UUID,
    )

    with pytest.raises(ValidationError):
        utils.Identifiers.from_string(f"OUSAMG_no-project-type_HG123456_{UUID}")

    with pytest.raises(ValidationError):
        utils.Identifiers.from_string("OUSAMG_wgs123_HG123456_not-a-uuid")

    with pytest.raises(TypeError):
        utils.Identifiers.from_string(123)  # type: ignore[arg-type]


def test_strip_uuid():
    UUID = "2f08b36a-b3e3-406b-9c6d-e8e47d20d922"
    assert utils.strip_uuid(f"some_text_{UUID}") == "some_text"
    assert utils.strip_uuid(UUID) == ""
    assert utils.strip_uuid("some_text") == "some_text"
    assert utils.strip_uuid("some_text_") == "some_text_"
    assert utils.strip_uuid(f"{UUID}_some_text") == "_some_text"
    assert utils.strip_uuid(f"{UUID}") == ""

    # With prefix and suffix
    assert utils.strip_uuid(f"prefix_{UUID}_suffix") == "prefix_suffix"

    # With multiple UUIDs
    assert utils.strip_uuid(f"{UUID}{UUID}") == ""

    # With separators
    assert utils.strip_uuid(f"prefix-{UUID}:{UUID}_{UUID}suffix") == "prefixsuffix"

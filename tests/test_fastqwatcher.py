import uuid
from pathlib import Path

import pytest
from artefacts.seqdata import DotSampleFile, PreFASTQFile
from maestro import FileArtefactModel
from workflowsteps.fastqwatcher import FASTQFileWatcher, SeqDataMaker

DATA_DIR = Path(__file__).resolve().parent / "data/samples"
SEQDATA = Path("OUSAMG_wgs123_HG123456_2f08b36a-b3e3-406b-9c6d-e8e47d20d922")
UUID = uuid.UUID("2f08b36a-b3e3-406b-9c6d-e8e47d20d922")


def test_get_read_file_number_from_path():
    p1 = Path("OUSAMG_wgs123_HG123456_R1.fastq.gz")
    assert FASTQFileWatcher.get_read_file_number_from_path(p1) == 1
    p2 = Path("OUSAMG_wgs123_HG123456_R2.fastq.gz")
    assert FASTQFileWatcher.get_read_file_number_from_path(p2) == 2
    with pytest.raises(ValueError):
        FASTQFileWatcher.get_read_file_number_from_path(Path("OUSAMG_wgs123_HG123456.fastq.gz"))


@pytest.mark.datafiles(DATA_DIR / SEQDATA, keep_top_dir=True)
async def test_fastq_file_watcher(datafiles):
    FASTQFileWatcher.path = datafiles
    fw = FASTQFileWatcher()
    fastq1 = FileArtefactModel(
        path=datafiles / SEQDATA / "OUSAMG_wgs123_HG123456_R1.fastq.gz",
    )
    artefacts = await fw.process(fastq1)
    assert artefacts[0].seqdata_id == UUID
    assert artefacts[0].path == Path(datafiles / SEQDATA / "OUSAMG_wgs123_HG123456_R1.fastq.gz")
    assert artefacts[0].read_file_number == 1


@pytest.mark.datafiles(DATA_DIR / SEQDATA, keep_top_dir=True)
async def test_SeqDataMaker(datafiles):
    pre_fastq1 = PreFASTQFile(
        seqdata_id=UUID,
        read_file_number=1,
        path=datafiles / SEQDATA / "OUSAMG_wgs123_HG123456_R1.fastq.gz",
    )
    pre_fastq2 = PreFASTQFile(
        seqdata_id=UUID,
        read_file_number=2,
        path=datafiles / SEQDATA / "OUSAMG_wgs123_HG123456_R2.fastq.gz",
    )
    with open(datafiles / SEQDATA / "sample1.sample") as f:
        contents = f.read()
        dotsample = DotSampleFile.model_validate_json(contents)
    sdm = SeqDataMaker()
    artefacts = await sdm.process(pre_fastq1=pre_fastq1, pre_fastq2=pre_fastq2, dotsample=dotsample)

    assert len(artefacts) == 1
    assert artefacts[0].fastqs[0].read_file_number == 1
    assert artefacts[0].fastqs[1].read_file_number == 2
    assert hasattr(artefacts[0].fastqs[0], "md5_at_creation")
    assert not hasattr(artefacts[0], "stats")

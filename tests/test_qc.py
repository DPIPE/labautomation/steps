import asyncio
import json
from pathlib import Path
from typing import override
from uuid import uuid4

import pytest
from artefacts.qc import BasepipeQCForceDelivered, BasepipeQCPass
from artefacts.vcpipe import QCResult, VCPipeFileArtefact
from maestro import (
    Job,
    MessageWatcher,
    WorkflowModel,
    order_workflow,
    run_workflow_steps,
)
from maestro.config import Config
from maestro.models import JobStatus
from maestro.nats.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import create_node, merge_job_node, read_node
from workflowsteps.checkbasepipeqc import CheckBasepipeQC

QC_JSON = Path("/steps/tests/data/qc/qc_results.json")
JOB_MAESTRO_ID = uuid4()


class DummyQCResult(MessageWatcher):
    @override
    async def process(self, input_artefact: VCPipeFileArtefact) -> list[QCResult]:
        qc_result = QCResult.model_validate(
            {
                "seqdata_id": input_artefact.seqdata_id,
                "workflow_id": input_artefact.workflow_id,
                "path": input_artefact.path.parent / QC_JSON.name,
            }
        )

        return [qc_result]


@pytest.mark.asyncio
@pytest.mark.parametrize("qc_pass", [True, False])
async def test_qc_step(truncate_database, tmp_path: Path, qc_pass: bool):
    """Test the CheckBasepipeQC workflow step."""

    # Update arbitrary QC JSON field with QC pass/fail status
    with QC_JSON.open("r") as f:
        qc_json = json.load(f)
        qc_json["VcfQuality"][0] = qc_pass

    with open(tmp_path / QC_JSON.name, "w") as f:
        json.dump(qc_json, f)

    completed_future = asyncio.Future()

    if not qc_pass:
        unhalted_future = asyncio.Future()

    async def unhalted_callback(msg):
        if not unhalted_future.done():
            unhalted_future.set_result(True)
        await msg.ack()

    async def completed_callback(msg):
        if not completed_future.done():
            completed_future.set_result(True)
        await msg.ack()

    async with NATSSession() as nats_session:
        seqdata_id = uuid4()
        workflow_id = uuid4()

        async with Neo4jTransaction() as neo4j_tx:
            input_artefact = VCPipeFileArtefact(
                seqdata_id=seqdata_id, workflow_id=workflow_id, path=tmp_path / "dummy"
            )
            await create_node(neo4j_tx, input_artefact)

        await run_workflow_steps(
            message_watcher_classes=[DummyQCResult, CheckBasepipeQC],
            artefact_classes=[
                QCResult,
                BasepipeQCPass,
                BasepipeQCForceDelivered,
                VCPipeFileArtefact,
            ],
        )

        await nats_session.subscribe(
            Config.STREAM,
            "workflow_step.CheckBasepipeQC.done",
            completed_callback,
            config={"deliver_policy": "new"},
        )

        if not qc_pass:
            await nats_session.subscribe(
                Config.STREAM,
                "workflow_step.CheckBasepipeQC.done",
                unhalted_callback,
                config={"deliver_policy": "new"},
            )

        workflow = WorkflowModel(
            workflow_name="DummyWorkflow",
            jobs={
                Job(
                    workflow_step=DummyQCResult,
                    inputs={
                        "input_artefact": (
                            VCPipeFileArtefact,
                            {"seqdata_id": seqdata_id, "workflow_id": workflow_id},
                        )
                    },
                ),
                Job(
                    maestro_id=JOB_MAESTRO_ID,
                    workflow_step=CheckBasepipeQC,
                    inputs={
                        "qc_result": (
                            QCResult,
                            {"seqdata_id": seqdata_id, "workflow_id": workflow_id},
                        )
                    },
                ),
            },
        )

        await order_workflow(workflow)

        if qc_pass:
            # Wait for workflow to finish
            await asyncio.wait_for(completed_future, timeout=10)

            async with Neo4jTransaction() as neo4j_tx:
                basepipe_qc_pass = await read_node(
                    neo4j_tx, BasepipeQCPass, {"seqdata_id": seqdata_id}
                )
                assert (
                    basepipe_qc_pass is not None
                ), f"{BasepipeQCPass} node not found for {seqdata_id}"
        else:
            # Wait for timeout due to halted workflow
            with pytest.raises(asyncio.TimeoutError):
                await asyncio.wait_for(completed_future, timeout=3.0)

            # Force deliver by "unhalting" workflow step
            async with Neo4jTransaction() as neo4j_tx:
                db_job = await read_node(neo4j_tx, Job, {"maestro_id": JOB_MAESTRO_ID})
                assert db_job is not None
                db_job.status = JobStatus.RUNNING

                await merge_job_node(neo4j_tx, db_job)

            # Wait for "unhalted" workflow to finish
            await asyncio.wait_for(unhalted_future, timeout=10)

            async with Neo4jTransaction() as neo4j_tx:
                basepipe_qc_force_delivered = await read_node(
                    neo4j_tx, BasepipeQCForceDelivered, {"seqdata_id": seqdata_id}
                )
                assert (
                    basepipe_qc_force_delivered is not None
                ), f"{BasepipeQCForceDelivered} node found for {seqdata_id}"

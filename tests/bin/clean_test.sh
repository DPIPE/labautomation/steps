rm -rf /tmp/maestro/samples/*
rm -rf /tmp/maestro/analyses-work/*
rm -rf /tmp/maestro/jobs
rm -rf /tmp/maestro/maestro.log

mkdir -p /tmp/maestro/{samples,analyses-work,jobs}

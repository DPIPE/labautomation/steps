#!/usr/bin/env bash
set -eu

# usage: generate_test_data.sh [options]
# example: ./generate_test_data.sh --singles 30 --trios 10 --testdir /tmp/maestro

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

python "${DIR}/../data/generate_test_data.py" "$@"

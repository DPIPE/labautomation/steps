import uuid
from pathlib import Path
from unittest.mock import patch

import artefacts.vcpipe
import pytest
import utils
from artefacts.pipeline_order import SpecializedPipeline
from maestro import FileArtefactModel
from workflowsteps.basepipe import Basepipe
from workflowsteps.dotsamplewatcher import DotsampleFileWatcher
from workflowsteps.fastqwatcher import FASTQFileWatcher, SeqDataMaker
from workflowsteps.taqmanwatcher import TaqmanFileWatcher

from config import config
from tests.conftest import dvc_require

DATA_DIR = Path(__file__).resolve().parent / "data/samples"
SEQDATA = Path("OUSAMG_wgs123_HG123456_2f08b36a-b3e3-406b-9c6d-e8e47d20d922")
UUID = uuid.UUID("2f08b36a-b3e3-406b-9c6d-e8e47d20d922")


async def filewatcher_file(watcher_cls, *paths):
    fw = watcher_cls()
    fam = FileArtefactModel(
        path=Path.joinpath(*paths),
    )
    return (await fw.process(fam))[0]


@dvc_require(
    [
        "vcpipe-testdata/dragen/*",
        "vcpipe-refdata/genomic/common/general/human_g1k_v37_decoy*",
        "vcpipe-refdata/genomic/knownSites/dbsnp/*",
        "vcpipe-refdata/mtdna/index/general/*",
        "vcpipe-refdata/capturekit/common/msh2/*",
        "singularity/compression/*",
        "singularity/vcpipe-essentials/*",
        "singularity/svtools/*",
    ]
)
@patch("workflowsteps.dotsamplewatcher.order_workflow_with_job_filtering")
@pytest.mark.datafiles(DATA_DIR / SEQDATA, keep_top_dir=True)
@pytest.mark.asyncio
async def test_basepipe(SeqDataMaker_order_mock, datafiles, monkeypatch: pytest.MonkeyPatch):
    config.steps.job_root_dir = datafiles / "maestro" / "jobs"
    monkeypatch.setattr(utils, "config", config)
    pre_fastq1 = await filewatcher_file(
        FASTQFileWatcher, datafiles, SEQDATA, "OUSAMG_wgs123_HG123456_R1.fastq.gz"
    )
    pre_fastq2 = await filewatcher_file(
        FASTQFileWatcher, datafiles, SEQDATA, "OUSAMG_wgs123_HG123456_R2.fastq.gz"
    )
    dotsample = await filewatcher_file(DotsampleFileWatcher, datafiles, SEQDATA, "sample1.sample")
    SeqDataMaker_order_mock.assert_awaited_once()
    sdm = SeqDataMaker()
    sdm_artefacts = await sdm.process(
        pre_fastq1=pre_fastq1, pre_fastq2=pre_fastq2, dotsample=dotsample
    )
    assert len(sdm_artefacts) == 1
    sdm_artefact = sdm_artefacts[0]
    basepipe = Basepipe()
    basepipe_artefacts = await basepipe.process(
        seqdata=sdm_artefact,
        workflow_id=uuid.UUID("c7d9a7b9-ca2e-44fb-b6e1-0c4694b24712"),
        workflow_name="DUMMY_WORKFLOW_NAME",
        job_id=uuid.UUID("dcccf2c7-bfe0-4bf7-bca5-f2ec49b87b55"),
        specialized_pipeline=SpecializedPipeline.Dragen,
        analysis_name="ANALYSIS_NAME_DUMMY",
    )
    assert {type(a) for a in basepipe_artefacts} == {
        artefacts.vcpipe.SeqMappingFile,
        artefacts.vcpipe.SeqMappingIndexFile,
        artefacts.vcpipe.VariantFile,
        artefacts.vcpipe.GenomicVariantFile,
        artefacts.vcpipe.GenomicVariantIndexFile,
        artefacts.vcpipe.CNVVariantFile,
        artefacts.vcpipe.QCReport,
        artefacts.vcpipe.QCWarnings,
        artefacts.vcpipe.QCResult,
        artefacts.vcpipe.CoverageBigWig,
        artefacts.vcpipe.MTVariantFile,
        artefacts.vcpipe.MTVariantIndexFile,
    }

    assert len(basepipe_artefacts) == 12


@dvc_require(
    [
        "vcpipe-testdata/CuCaV3/*",
        "vcpipe-testdata/sensitive-db/*",
        "vcpipe-refdata/genomic/index/pms2cl_masked_bwa-mem2/*",
        "vcpipe-refdata/genomic/common/general/*",
        "vcpipe-refdata/genomic/knownSites/dbsnp/*",
        "vcpipe-refdata/capturekit/common/illumina_customercancer_v02/*",
        "vcpipe-refdata/capturekit/common/illumina_customercancer_v03/*",
        "singularity/compression/*",
        "singularity/convading/*",
        "singularity/svtools/*",
        "singularity/vcpipe-essentials/*",
    ]
)
@patch("workflowsteps.dotsamplewatcher.order_workflow_with_job_filtering")
@pytest.mark.datafiles(DATA_DIR, keep_top_dir=True)
@pytest.mark.asyncio
async def test_basepipe_cuca(SeqDataMaker_order_mock, datafiles):
    config.steps.job_root_dir = datafiles / "maestro" / "jobs"
    DATA_DIR = Path(
        "/steps/vcpipe-testdata/CuCaV3/Diag_EKG241002_NA12878_2f08b36a-b3e3-406b-9c6d-e8e47d20d922"
    )
    pre_fastq1 = await filewatcher_file(
        FASTQFileWatcher, DATA_DIR, "Diag-EKG241002-NA12878N72_R1.fastq.gz"
    )
    pre_fastq2 = await filewatcher_file(
        FASTQFileWatcher, DATA_DIR, "Diag-EKG241002-NA12878N72_R2.fastq.gz"
    )
    dotsample = await filewatcher_file(
        DotsampleFileWatcher, DATA_DIR, "Diag-EKG200203-NA12878N6.sample"
    )
    taqman = await filewatcher_file(TaqmanFileWatcher, DATA_DIR, "Diag-EKG200203-NA12878N6.taqman")

    SeqDataMaker_order_mock.assert_awaited_once()
    sdm = SeqDataMaker()
    sdm_artefacts = await sdm.process(
        pre_fastq1=pre_fastq1, pre_fastq2=pre_fastq2, dotsample=dotsample
    )

    assert len(sdm_artefacts) == 1
    sdm_artefact = sdm_artefacts[0]
    basepipe = Basepipe()
    basepipe_artefacts = await basepipe.process(
        seqdata=sdm_artefact,
        workflow_id=uuid.UUID("c7d9a7b9-ca2e-44fb-b6e1-0c4694b24712"),
        workflow_name="DUMMY_WORKFLOW_NAME",
        job_id=uuid.UUID("dcccf2c7-bfe0-4bf7-bca5-f2ec49b87b55"),
        specialized_pipeline=SpecializedPipeline.NoSpecification,
        analysis_name="ANALYSIS_NAME_DUMMY",
        taqman=taqman,
    )
    assert {type(a) for a in basepipe_artefacts} == {
        artefacts.vcpipe.VariantFile,
        artefacts.vcpipe.GenomicVariantFile,
        artefacts.vcpipe.GenomicVariantIndexFile,
        artefacts.vcpipe.SeqMappingFile,
        artefacts.vcpipe.SeqMappingIndexFile,
        artefacts.vcpipe.CNVVariantFile,
        artefacts.vcpipe.QCReport,
        artefacts.vcpipe.QCWarnings,
        artefacts.vcpipe.QCResult,
        artefacts.vcpipe.CoverageBigWig,
        artefacts.vcpipe.ConvadingLogFile,
        artefacts.vcpipe.ConvadingLonglistFile,
        artefacts.vcpipe.ConvadingShortlistFile,
        artefacts.vcpipe.ConvadingTotallistFile,
    }
    assert len(basepipe_artefacts) == 14


@dvc_require(
    [
        "vcpipe-testdata/excap/*",
        "vcpipe-refdata/genomic/common/general/*",
        "vcpipe-refdata/genomic/index/pms2cl_masked_bwa-mem2/*",
        "vcpipe-refdata/funcAnnot/bed/master-genepanel/*",
        "vcpipe-refdata/genomic/knownSites/1000G/*",
        "vcpipe-refdata/genomic/knownSites/Mills/*",
        "vcpipe-refdata/genomic/knownSites/dbsnp/*",
        "vcpipe-refdata/capturekit/common/agilent_sureselect_v07/*",
        "singularity/vcpipe-r-cnv/*",
        "singularity/compression/*",
        "singularity/vcpipe-essentials/*",
        "singularity/svtools/*",
    ]
)
@patch("workflowsteps.dotsamplewatcher.order_workflow_with_job_filtering")
@pytest.mark.datafiles(DATA_DIR, keep_top_dir=True)
@pytest.mark.asyncio
async def test_basepipe_excap(SeqDataMaker_order_mock, datafiles):
    config.steps.job_root_dir = datafiles / "maestro" / "jobs"
    DATA_DIR = Path(
        "/steps/vcpipe-testdata/excap/Diag_excap240_NA12878_2f08b36a-b3e3-406b-9c6d-e8e47d20d922"
    )
    pre_fastq1 = await filewatcher_file(
        FASTQFileWatcher, DATA_DIR, "Diag-excap240-NA12878N52_R1.fastq.gz"
    )
    pre_fastq2 = await filewatcher_file(
        FASTQFileWatcher, DATA_DIR, "Diag-excap240-NA12878N52_R2.fastq.gz"
    )
    dotsample = await filewatcher_file(
        DotsampleFileWatcher, DATA_DIR, "Diag-excap240-NA12878N52.sample"
    )

    SeqDataMaker_order_mock.assert_awaited_once()
    sdm = SeqDataMaker()
    sdm_artefacts = await sdm.process(
        pre_fastq1=pre_fastq1, pre_fastq2=pre_fastq2, dotsample=dotsample
    )

    assert len(sdm_artefacts) == 1
    sdm_artefact = sdm_artefacts[0]
    basepipe = Basepipe()
    basepipe_artefacts = await basepipe.process(
        seqdata=sdm_artefact,
        workflow_id=uuid.UUID("c7d9a7b9-ca2e-44fb-b6e1-0c4694b24712"),
        workflow_name="DUMMY_WORKFLOW_NAME",
        job_id=uuid.UUID("dcccf2c7-bfe0-4bf7-bca5-f2ec49b87b55"),
        specialized_pipeline=SpecializedPipeline.NoSpecification,
        analysis_name="ANALYSIS_NAME_DUMMY",
    )

    assert {type(a) for a in basepipe_artefacts} == {
        artefacts.vcpipe.VariantFile,
        artefacts.vcpipe.GenomicVariantFile,
        artefacts.vcpipe.GenomicVariantIndexFile,
        artefacts.vcpipe.SeqMappingFile,
        artefacts.vcpipe.SeqMappingIndexFile,
        artefacts.vcpipe.CNVBedFile,
        artefacts.vcpipe.QCReport,
        artefacts.vcpipe.QCWarnings,
        artefacts.vcpipe.QCResult,
        artefacts.vcpipe.CoverageBigWig,
    }

    assert len(basepipe_artefacts) == 10

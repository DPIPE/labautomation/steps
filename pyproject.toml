[tool.poetry]
name = "steps"
version = "0.1.0"
description = ""
authors = ["OUS AMG <diag-bioinf@medisin.uio.no>"]
readme = "README.md"
package-mode = false

[tool.poetry.dependencies]
python = "^3.12"
maestro = {git = "https://gitlab.com/DPIPE/labautomation/maestro.git", rev = "dev", subdirectory = "src"}
dvc = "^3.50.0"
dvc-s3 = "^3.1.0"
jinja2 = ">=2.11.2"
structlog = "^24.4.0"
cyvcf2 = "^0.31.1"
tsd-s3cmd = {url = "https://github.com/unioslo/tsd-s3cmd/archive/refs/tags/v0.1.6.tar.gz"}
types-requests = "^2.32.0.20241016"
sapiopylib = "^2024.12.20.241"

[tool.poetry.group.dev.dependencies]
ruff = "^0.1.3"
pytest = "^7.1.3"
mypy = "^1.6.0"
pdbpp = "^0.10.3"
pytest-datafiles = "^3.0.0"
pytest-asyncio = "^0.21.2"
pytest-error-for-skips = "^2.0.2"
pytest-cov = "^5.0.0"
hypothesis = "^6.112.2"

[build-system]
requires = ["poetry-core"]
build-backend = "poetry.core.masonry.api"

[tool.ruff]
line-length = 100
target-version = "py312"

[tool.ruff.lint]
select = ["E4", "E7", "E9", "F", "UP"]

[tool.mypy]
mypy_path = "./src"
python_version = "3.12"
plugins = ["pydantic.mypy"]
exclude = ["vcpipe", "ella-sapio"]
ignore_missing_imports = true
no_implicit_optional = true
warn_redundant_casts = true
warn_unused_ignores = true
check_untyped_defs = true
strict_equality = true
warn_unreachable = true
strict = true
disallow_any_generics = false
enable_error_code = [
    "redundant-expr",
    "truthy-bool",
    "truthy-iterable",
    "ignore-without-code",
    "unused-awaitable",
    "explicit-override",
    "unimported-reveal"
]

[[tool.mypy.overrides]]
module = "tests.*"
strict = false
disable_error_code = [
    "var-annotated",
    "no-redef",
    "no-untyped-call",
    "union-attr",
    "no-untyped-def",
    "unused-awaitable",
    "explicit-override",
]

[tool.pytest.ini_options]
markers = [
	"destructive: mark test to be skipped by default because it is destructive, e.g. it uses the truncate_database fixture"
]
testpaths = ["tests"]
pythonpath = "src"
asyncio_mode = "auto"
filterwarnings = [
    "ignore::hypothesis.errors.HypothesisDeprecationWarning"
]

[tool.coverage.run]
branch = true
data_file = "coverage/.coverage"
concurrency = ["thread", "multiprocessing"]
omit = [
    "tests/data/generate_test_data.py",
    "*/vcpipe/**",
    ".command.sh"
]

[tool.coverage.report]
exclude_also = [
    "pragma: no cover",
    "def __repr__",
    "raise AssertionError",
    "raise NotImplementedError",
    "@(abc\\.)?abstractmethod",
    "if __name__ == .__main__.:",
]
precision = 1

[tool.coverage.xml]
output = "coverage/coverage.xml"

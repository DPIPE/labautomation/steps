import asyncio
import os

from maestro import FileWatcher, MessageWatcher, run_workflow_steps

import zappy.run
from workflowsteps.annopipe import Annopipe
from workflowsteps.basepipe import Basepipe
from workflowsteps.dotanalysiswatcher import DotanalysisFileWatcher
from workflowsteps.dotsamplewatcher import DotsampleFileWatcher
from workflowsteps.fastqwatcher import FASTQFileWatcher, SeqDataMaker
from workflowsteps.packagestager import PackageStager
from workflowsteps.packagetransferrer import PackageTransferrer
from workflowsteps.packageunpacker import PackageUnpacker
from workflowsteps.taqmanwatcher import TaqmanFileWatcher
from workflowsteps.triopipe import Triopipe

FILEWATCHER_NAME_MAPPING = {
    "DotanalysisFileWatcher": DotanalysisFileWatcher,
    "DotsampleFileWatcher": DotsampleFileWatcher,
    "FASTQFileWatcher": FASTQFileWatcher,
    "TaqmanFileWatcher": TaqmanFileWatcher,
}

MESSAGEWATCHER_NAME_MAPPING = {
    "Annopipe": Annopipe,
    "Basepipe": Basepipe,
    "PackageStager": PackageStager,
    "PackageTransferrer": PackageTransferrer,
    "PackageUnpacker": PackageUnpacker,
    "SeqDataMaker": SeqDataMaker,
    "Triopipe": Triopipe,
}


def get_message_watchers() -> list[type[MessageWatcher]]:
    message_watchers = []
    message_watchers_env = os.environ.get("MESSAGE_WATCHERS")
    if message_watchers_env:
        for watcher in message_watchers_env.split(","):
            if watcher in MESSAGEWATCHER_NAME_MAPPING:
                message_watchers.append(MESSAGEWATCHER_NAME_MAPPING[watcher])
            else:
                raise ValueError(f"Invalid message watcher: {watcher}")

    return message_watchers


def get_file_watchers() -> list[type[FileWatcher]]:
    file_watchers = []
    file_watchers_env = os.environ.get("FILE_WATCHERS")
    if file_watchers_env:
        for watcher in file_watchers_env.split(","):
            if watcher in FILEWATCHER_NAME_MAPPING:
                file_watchers.append(FILEWATCHER_NAME_MAPPING[watcher])
            else:
                raise ValueError(f"Invalid file watcher: {watcher}")

    return file_watchers


async def main() -> None:
    task = await run_workflow_steps(
        message_watcher_classes=get_message_watchers(),
        file_watcher_classes=get_file_watchers(),
    )
    zappy_task = asyncio.create_task(zappy.run.main())
    await task
    if not zappy_task.done():
        zappy_task.cancel()
    try:
        await zappy_task
    except asyncio.CancelledError:
        pass


if __name__ == "__main__":
    asyncio.run(main())

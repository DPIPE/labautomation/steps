import logging
import os
import tomllib
from pathlib import Path
from typing import Annotated, Literal

from pydantic import (
    BaseModel,
    BeforeValidator,
    DirectoryPath,
    Field,
    FilePath,
    HttpUrl,
    PositiveFloat,
    PositiveInt,
    TypeAdapter,
    field_validator,
)

# Allow for URL validation of strings
# See https://github.com/pydantic/pydantic/discussions/6395#discussioncomment-7361416
http_url_adapter = TypeAdapter(HttpUrl)

Url = Annotated[str, BeforeValidator(lambda value: str(http_url_adapter.validate_python(value)))]


class StepsConfig(BaseModel):
    logging_path: Path = Path(__file__).parent.parent / "ops/logs"
    logging_level: Literal["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"] = "INFO"
    logging_max_bytes: PositiveInt = 1024 * 1024 * 128
    logging_backup_count: PositiveInt = 5
    dotsample_landing_dir: DirectoryPath
    taqman_dir: DirectoryPath
    dotanalysis_landing_dir: DirectoryPath
    fastq_landing_dir: DirectoryPath
    job_root_dir: DirectoryPath

    @field_validator("logging_path", mode="after")
    @classmethod
    def check_logging_path(cls, v: Path) -> Path:
        if not v.exists():
            v.mkdir(parents=True)
        return v


class VcpipePipeline(BaseModel):
    concurrency_limit: PositiveInt = 1


class VcpipeConfig(BaseModel):
    vcpipe_dir: DirectoryPath
    analyses_work_dir: DirectoryPath
    nextflow_config: FilePath
    system: str
    test_settings: str
    sensitive_db_dir: DirectoryPath
    genepanels_dir: DirectoryPath
    stub_run: bool
    basepipe: VcpipePipeline
    triopipe: VcpipePipeline
    annopipe: VcpipePipeline
    qc: VcpipePipeline


class PackageStagingConfig(BaseModel):
    """NSC instance configuration for staging package artefacts for transfer."""

    concurrency_limit: PositiveInt = Field(
        ..., description="Maximum number of concurrent package stagings."
    )
    instance: str = Field(..., description="Staging instance for package artefacts (NSC).")
    transfer_dir: Path = Field(
        ..., description="Directory for staging artefacts for transfer (as found on NSC instance)."
    )


class PackageTransferConfig(BaseModel):
    """NSC transfer instance configuration for transferring package artefacts between instances."""

    api_endpoint: Url = Field(
        "https://alt.api.tsd.usit.no/v1/p22/auth/basic/token",
        description="URL to the TSD API endpoint.",
    )
    api_key: str = Field(..., description="Long-lived API key.")
    bucket: str = Field(..., description="S3 bucket name.")
    filelock: Path = Field(..., description="Filelock directory on TSD.")
    chunk_size: PositiveInt = Field(200, description="Chunk size (MB) for file transfer.")
    concurrency_limit: PositiveInt = Field(
        ..., description="Maximum number of concurrent transfers."
    )
    instance: str = Field(
        ..., description="Transfer instance for package artefacts (NSC transfer)."
    )
    project: str = Field("p22", description="Header value of x-project-select.")
    token_type: str = Field("s3import", description="Type of requested token.")
    transfer_dir: Path = Field(
        ...,
        description="Directory for staging artefacts for transfer (as found on transfer instance).",
    )


class PackageUnpackingConfig(BaseModel):
    """TSD instance configuration for unpacking package artefacts after transfer.

    Note that file routings in @package() decorator take precedence if glob patterns overlap."""

    class FileRouting(BaseModel):
        """File routing configuration for unpacking package artefacts."""

        glob: str = Field(..., description="Glob pattern to match file names.")
        target_dir: Path = Field(..., description="Target directory for matched files.")

    concurrency_limit: PositiveInt = Field(
        ..., description="Maximum number of concurrent unpackings."
    )
    file_routings: list[FileRouting]
    filelock_instance: str = Field(
        ...,
        description="Artificial instance for not-yet-unpacked package artefacts.",
    )
    instance: str = Field(
        ..., description="Target and unpacking instance for package artefacts (TSD)."
    )


class PackageConfig(BaseModel):
    staging: PackageStagingConfig
    transfer: PackageTransferConfig
    unpacking: PackageUnpackingConfig
    run_transfer: bool = Field(..., description="Whether or not to run package transfer.")


class ZappyConfig(BaseModel):
    sapio_url: str | None = None
    sapio_guid: str | None = None
    sapio_api_token: str | None = None
    sapio_server_cert_fingerprint: str | None = None
    polling_interval: PositiveFloat = 60


class AppConfig(BaseModel):
    steps: StepsConfig
    package: PackageConfig
    vcpipe: VcpipeConfig
    zappy: ZappyConfig


config_path = Path(
    os.environ.get("STEPS_CONFIG", Path(__file__).parent.parent / "config" / "steps.toml")
)
with open(config_path, "rb") as f:
    config_dict = tomllib.load(f)


config = AppConfig.model_validate(config_dict)


def with_mocked_vcpipe(config: AppConfig) -> AppConfig:
    """
    Override vcpipe config with mock vcpipe directory.

    This is useful for testing, where we don't want to run the actual vcpipe pipelines.

    _setattr is a function that can be used to set attributes on the config object.
    """
    vcpipe_base_dir = Path(__file__).parent.parent / "tests/vcpipe"
    # Copy to avoid modifying the original config
    config_copy = AppConfig.model_validate(config.model_dump())

    config_copy.vcpipe.vcpipe_dir = vcpipe_base_dir
    config_copy.vcpipe.nextflow_config = vcpipe_base_dir / "config/nextflow.config"

    # Revalidate config
    return AppConfig.model_validate(config_copy)


# Override config if MOCK_VCPIPE is set to 'true', and use mock vcpipe directory
if os.environ.get("MOCK_VCPIPE", "").lower() in ("true", "yes", "1"):
    # Log here, even though logging is not yet configured
    logger = logging.getLogger("steps")
    logger.warning("environment variable 'MOCK_VCPIPE' set - using mock vcpipe directory")
    config = with_mocked_vcpipe(config)

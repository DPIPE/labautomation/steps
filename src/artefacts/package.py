from pydantic import UUID4, Field

from artefacts.base import StepsBaseFileArtefact
from config import PackageUnpackingConfig

FileRouting = PackageUnpackingConfig.FileRouting


class PackageArtefact(StepsBaseFileArtefact):
    """Meta artefact bundling file artefacts for NSC-to-TSD transfer."""

    file_artefacts: set[StepsBaseFileArtefact] = Field(
        default_factory=set, description="Output file artefacts to be transferred."
    )
    extra_artefacts: set[StepsBaseFileArtefact] = Field(
        default_factory=set,
        description="Additional files not included in step output artefacts.",
    )
    file_routings: list[FileRouting] = Field(
        ...,
        description="File routing configuration for unpacking package artefacts on TSD.",
    )
    transfer_id: UUID4 = Field(..., description="Unique identifier for one package transfer.")

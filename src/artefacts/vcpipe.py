"""Artefacts representing files generated by vcpipe"""

from uuid import UUID

from pydantic import BaseModel, Field

from artefacts.base import StepsBaseArtefact, StepsBaseFileArtefact


class SeqDataWorkflowIDsBase(BaseModel):
    """Artefact for SeqData and workflow identifiers.

    Artefacts can usually be uniquely indentified by a single SeqData identifier.
    If there are multiple valid artefacts of the same type for one specific SeqData in Neo4j,
    the artefacts must be further distinguished by workflow identifier, timepoint,
    or an other artefact-specific property when placing a job order."""

    seqdata_id: UUID = Field(
        ..., description="The UUID for the SeqData used upstream to generate the artefact"
    )
    workflow_id: UUID = Field(..., description="The workflow the artefact was generated in")


class VCPipeFileArtefact(StepsBaseFileArtefact, SeqDataWorkflowIDsBase):
    """Base class for file artefacts generated by vcpipe."""

    ...


class VCPipeArtefact(StepsBaseArtefact, SeqDataWorkflowIDsBase):
    """Base class for artefacts generated by vcpipe."""

    ...


class SeqMappingFile(VCPipeFileArtefact):
    """Artefact for a single-sample 'BAM' file.

    The file can be in BAM (.bam) or CRAM (.cram) format.
    Artefact users should check the file extension to determine the format"""

    ...


class SeqMappingIndexFile(VCPipeFileArtefact):
    """Artefact for a single-sample 'BAM' index file.

    The file can be BAM index (.bam.bai) or CRAM index (.cram.crai) format."""

    ...


class VariantFile(VCPipeFileArtefact):
    """Artefact for a single-sample VCF file.

    The file can be VCF (.vcf) or gzipped (.vcf.gz) format.
    Artefact users should check the file extension to determine the format.

    This artefact is typically used for small variants (SNVs and indels).
    Structural variants (SVs) go into a different artefact type."""

    ...


class MultiSampleVariantFile(VCPipeFileArtefact):
    """Artefact for a multi-sample VCF file.

    The file can be VCF (.vcf) or gzipped (.vcf.gz) format.
    Artefact users should check the file extension to determine the format.

    This artefact is typically used for SNVs and small INDELs.
    Structural variants (SVs) go into a different artefact type."""

    ...


class VariantIndexFile(VCPipeFileArtefact):
    """Artefact for a Tabix (.tbi) index file for VCF file."""

    ...


class GenomicVariantFile(VCPipeFileArtefact):
    """Artefact for a single-sample GVCF file."""

    ...


class GenomicVariantIndexFile(VCPipeFileArtefact):
    """Artefact for a single-sample GVCF index file (g.vcf.gz.tbi)."""

    ...


class QCReport(VCPipeFileArtefact):
    """Artefact for a qc_report.md file"""

    ...


class QCReportTrio(VCPipeFileArtefact):
    """Artefact for a qc_report.md file for trio"""

    ...


class QCWarnings(VCPipeFileArtefact):
    """Artefact for a qc_warnings.md file"""

    ...


class QCWarningsTrio(VCPipeFileArtefact):
    """Artefact for a qc_warnings.md file for trio"""

    ...


class QCResult(VCPipeFileArtefact):
    """Artefact for a qc_result.json file"""

    ...


class QCResultTrio(VCPipeFileArtefact):
    """Artefact for a qc_result.json file of trio"""

    ...


class CoverageBigWig(VCPipeFileArtefact):
    """Artefact for a bigWig file with coverage per basepair (used in CNV context)"""

    ...


class CNVVariantFile(VCPipeFileArtefact):
    """Artefact for a CNV variants file.

    In vcpipe, a VCF file with the merged call set from several callers."""

    ...


class CNVBedFile(VCPipeFileArtefact):
    """Artefact for a CNV bed file.

    In vcpipe, a CNV bed file."""

    ...


class ConvadingLogFile(VCPipeFileArtefact):
    """Artefact for convading aligned only best score log file."""

    ...


class ConvadingTotallistFile(VCPipeFileArtefact):
    """Artefact for convading aligned only best score totallist txt file."""

    ...


class ConvadingLonglistFile(VCPipeFileArtefact):
    """Artefact for convading aligned only best score longlist txt file."""

    ...


class ConvadingShortlistFile(VCPipeFileArtefact):
    """Artefact for convading aligned only best score shortlist txt file."""

    ...


class MTVariantFile(VCPipeFileArtefact):
    """Artefact for a VCF file with mitochondrial variants."""

    ...


class MultiSampleMTVariantFile(VCPipeFileArtefact):
    """Artefact for a multi-sample VCF file with mitochondrial variants."""

    ...


class MTVariantIndexFile(VCPipeFileArtefact):
    """Artefact for an index file for VCF with mitochondrial variants."""

    ...


class AnnopipeOutput(VCPipeFileArtefact):
    """Tarball of a folder containing output files generated by annopipe for ella"""

    ...

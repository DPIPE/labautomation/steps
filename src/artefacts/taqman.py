from artefacts.base import StepsBaseFileArtefact
from pydantic import Field


class TaqmanFile(StepsBaseFileArtefact):
    """Artefact for a Taqman file.

    Taqman is used for SNP fingerprinting qc check, assuring that sample swap has not happened
    as part of the HTS library prep in the lab.
    We assume that only 1 Taqman file is valid for any dna_id."""

    dna_id: str = Field(
        ...,
        description="The DNA identifier for the artefact",
        json_schema_extra={"examples": ["HG123456"]},
    )

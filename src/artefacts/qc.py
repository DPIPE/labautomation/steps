from artefacts.vcpipe import VCPipeArtefact


class BasepipeQC(VCPipeArtefact):
    """Base class for basepipe QC step output artefact."""

    ...


class BasepipeQCPass(BasepipeQC):
    """Artefact for a basepipe QC step that passes QC checks."""

    ...


class BasepipeQCForceDelivered(BasepipeQC):
    """Artefact for a QC step that failed but was force delivered."""

    ...

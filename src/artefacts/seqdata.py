"""Artefacts and pydantic models for sequencing metadata and FASTQ files.

SeqData is an artefact with sequencing metadata and FASTQFile artefacts.
The sequencing metadata come from .sample files."""

from datetime import date
from enum import IntEnum, StrEnum
from typing import Any
from uuid import UUID

from pydantic import BaseModel, Field, computed_field, model_validator

from artefacts.base import StepsBaseArtefact, StepsBaseFileArtefact


class PreFASTQFile(StepsBaseFileArtefact):
    """A throw-away artefact missing some metadata that a FASTQFile artefact has."""

    seqdata_id: UUID = Field(..., description="The UUID for the seqdata (sample)")
    read_file_number: int = Field(
        ...,
        description="The read file number. Typically 1 for single-end sequencing and 1 or 2 for paired-end sequencing",
    )


class FASTQFile(PreFASTQFile):
    """Artefact for a FASTQ file."""

    md5_at_creation: str = Field(
        ...,
        description="The md5 checksum of the FASTQ file at the time it was created",
        min_length=32,
        max_length=32,
    )
    size_at_creation: int = Field(
        ...,
        description="""The size of the FASTQ file in bytes at the time it was created.
        Depends on compression method at creation time. The purpose is to check validity after transfer etc.""",
        ge=0,
    )


class PriorityEnum(IntEnum):
    normal = 1
    high = 2
    urgent = 3


class GivenSexEnum(StrEnum):
    male = "male"
    female = "female"
    na = "na"


class CommonSeqDataModel(BaseModel):
    """A common model for DotSampleFile and SeqData."""

    seqdata_id: UUID | None = Field(None, description="The UUID for the SeqData")
    owner_id: str = Field(
        ...,
        description="The legal owner of the SeqData",
        json_schema_extra={"examples": ["OUSAMG"]},
    )
    batch_id: str = Field(
        ...,
        description="The batch identifier for the SeqData",
        json_schema_extra={"examples": ["wgs123", "EKG20231201"]},
    )
    dna_id: str = Field(
        ...,
        description="The DNA identifier for the SeqData",
        json_schema_extra={"examples": ["HG123456"]},
    )
    sequence_date: date = Field(
        date.fromtimestamp(0), description="The date YYYY-MM-DD of sequencing start"
    )
    sequencer_id: str = Field("N/A", description="The identifier of the sequencing instrument used")
    sequencer_type: str = Field("N/A", description="The type of sequencer used, e.g. NovaseqX")
    flowcell_id: str = Field("N/A", description="The identifier of the flowcell used")
    flowcell_type: str = Field(
        "N/A",
        description="The flowcell type used for sequencing",
        json_schema_extra={"examples": "S4"},
    )
    index: str = Field("N/A", description="The sequencing index used for de-multiplexing")
    lanes: list[str] | None = Field(
        None,
        description="The lane(s) on the flowcell used for this SeqData. Typically only 1 value.",
        json_schema_extra={"examples": ["1"]},
    )
    given_sex: GivenSexEnum = Field(
        GivenSexEnum.na, description="The sex of the sample as given in requisition"
    )
    priority: PriorityEnum = Field(
        PriorityEnum.normal, description="The priority (for file transfer)"
    )
    capturekit: str = Field(
        default="", description="The lab prep or capture kit used for sequencing"
    )
    nickname: str = Field(default="", description="Optional human/simple name")

    @model_validator(mode="before")
    def validate_seqdata_id(cls, values: dict[str, Any]) -> dict[str, Any]:
        """
        Migrate old style seqdata to new style

        Note: Will not set seqdata_id
        """
        name = values.pop("name", None)
        if "batch_id" not in values:
            if not name:
                raise ValueError("name must be set if batch_id is not set")

            import re

            split_name = re.split(r"[-_]", name)
            if len(split_name) < 3:
                raise ValueError("name must be in format ownerid_batchid_dnaid")
            values["owner_id"] = "OUSAMG" if split_name[0].lower() == "diag" else split_name[0]
            values["batch_id"] = split_name[1]
            values["dna_id"] = split_name[2]

        if "flowcell_id" in values and values["flowcell_id"] is None:
            values["flowcell_id"] = "N/A"

        return values

    @computed_field  # type: ignore[prop-decorator]
    @property
    def name(self) -> str:
        """The name of the SeqData.

        Full name like OUSAMG_batch_id_DNA_id_UUID.
        Example: OUSAMG_wgs123_HG123456_2f08b36a-b3e3-406b-9c6d-e8e47d20d922"""
        return f"{self.owner_id}_{self.batch_id}_{self.dna_id}_{self.seqdata_id}"


class SeqData(StepsBaseArtefact, CommonSeqDataModel):
    """Artefact for a sequencing dataset, aka 'sample'."""

    fastqs: list[FASTQFile] = Field(
        default_factory=list,
        description="The FASTQ file artefact(s)' seqdata_id(s) for this SeqData",
    )


class ReadFileEntry(BaseModel):
    """Model for a FASTQ entry in the reads field in .sample metadata file."""

    path: str = Field(..., description="The name (not path) of the FASTQ file")
    md5: str = Field(
        ..., description="The MD5 checksum of the FASTQ file", min_length=32, max_length=32
    )
    size: int = Field(..., description="The size of the FASTQ file in bytes", ge=0)


class DotSampleFile(StepsBaseArtefact, CommonSeqDataModel):
    """Model for .sample metadata file.

    These files are today used for metadata transfer from Clarity LIMS to vcpipe
    and reside within each sample directory together with FASTQ files."""

    reads: list[ReadFileEntry] = Field(..., description="The FASTQ files for the sample")
    stats: dict = Field({}, description="Optional quality statistics")

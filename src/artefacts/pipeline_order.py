"""
vcpipe orders come from .analysis files which should be in specific format.
"""

import re
from datetime import date
from enum import StrEnum
from inspect import isclass
from typing import Annotated, Any, ClassVar, TypeVar, get_args
from uuid import uuid4

from pydantic import (
    UUID4,
    UUID5,
    BaseModel,
    ConfigDict,
    Discriminator,
    Field,
    Tag,
    TypeAdapter,
    ValidationInfo,
    field_serializer,
    field_validator,
    model_validator,
)
from utils import Identifiers

from artefacts.base import StepsBaseArtefact, StepsBaseFileArtefact
from artefacts.seqdata import DotSampleFile, GivenSexEnum, PriorityEnum, SeqData
from artefacts.taqman import TaqmanFile
from artefacts.vcpipe import (
    CNVBedFile,
    CNVVariantFile,
    ConvadingLogFile,
    ConvadingLonglistFile,
    ConvadingShortlistFile,
    ConvadingTotallistFile,
    CoverageBigWig,
    GenomicVariantFile,
    MTVariantFile,
    MTVariantIndexFile,
    MultiSampleVariantFile,
    QCReport,
    QCReportTrio,
    QCResult,
    QCResultTrio,
    QCWarnings,
    QCWarningsTrio,
    SeqMappingFile,
    VariantFile,
)

T = TypeVar("T", bound=StepsBaseArtefact | StepsBaseFileArtefact)
InputArtefactType = tuple[type[T], dict[str, Any]]

SAMPLE_ID_PATTERN = r"[A-Za-z]+[0-9]+"  # TODO Tighten at a later stage
UUID_PATTERN = r"[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"
SAMPLE_PATTERN = rf"^.*_.*_{SAMPLE_ID_PATTERN}_{UUID_PATTERN}$"
GENEPANEL_PATTERN = r"[A-Za-z0-9]+_v\d+\.\d+\.\d+"


class JointVariantCallingType(StrEnum):
    SINGLE = "single"
    TRIO = "trio"


class DotAnalysisFileArtefact(StepsBaseFileArtefact):
    """An artefact for a .analysis file, but only storing path to file.

    Data from the file goes into a PipelineOrder artefact."""

    ...


class PipelineType(StrEnum):
    basepipe = "basepipe"
    triopipe = "triopipe"
    annopipe = "annopipe"


class SpecializedPipeline(StrEnum):
    Dragen = "Dragen"
    Tumor = "Tumor"
    NoSpecification = "NoSpecification"


class PedigreeMember(BaseModel):
    sample: str = Field(..., description="sample name of a pedigree member")
    given_sex: GivenSexEnum = Field(
        default=GivenSexEnum.na, alias="gender", description="given_sex of a pedigree member"
    )


class Pedigree(BaseModel):
    father: PedigreeMember = Field(..., description="father of a triopipe order")
    mother: PedigreeMember = Field(..., description="mother of a triopipe order")
    proband: PedigreeMember = Field(..., description="child of a triopipe order")


class PipelineOrderBase(BaseModel):
    model_config = ConfigDict(use_enum_values=True)

    date_analysis_requested: date | None = Field(
        default=None, description="date of analysis requested of a pipeline order"
    )
    given_sex: GivenSexEnum = Field(
        default=GivenSexEnum.na,
        alias="gender",
        description="given_sex of samples of a pipeline order",
    )
    samples: list[str] | None = None  # redefined in subclasses with length constraints
    pipeline_type: PipelineType = Field(..., alias="type", description="type of a pipeline order")
    priority: PriorityEnum | None = Field(
        default=PriorityEnum.normal, validate_default=True, description="priority of an order"
    )

    @field_validator("samples", mode="after")
    @classmethod
    def validate_samples(cls, samples: list[str]) -> list[str]:
        """Ensure sample names match pattern."""
        for sample in samples:
            if not re.match(SAMPLE_PATTERN, sample):
                raise ValueError(f"Sample name {sample} does not match pattern {SAMPLE_PATTERN}")

        return samples


class InputBase(BaseModel):
    """Base class for Job inputs of a pipeline order."""

    @field_validator("*", mode="before")
    @classmethod
    def validate_field(cls, data: dict | tuple, info: ValidationInfo) -> dict | tuple:
        """Construct input artefact tuples for Job instantiation."""

        # Format assumed correct, necessary for union of artefact types where
        # artefact class cannot be inferred unambiguously from field annotation alone
        # (e.g. TypeA | TypeB where TypeA != None and TypeB != None)
        if isinstance(data, tuple) and issubclass(
            data[0], StepsBaseArtefact | StepsBaseFileArtefact
        ):
            return data
        elif isinstance(data, dict):
            # Retrieve artefact class from field annotation
            assert info.field_name is not None
            field_info = cls.model_fields[info.field_name]
            type_args = get_args(field_info.annotation)

            # Handle nullable fields (i.e. TypeA | None)
            if type(None) in type_args:
                assert len(type_args) == 2, f"Expected 2 type args, found {len(type_args)}."
                type_args = tuple(get_args(t)[0] for t in type_args if t is not type(None))

            artefact_class = get_args(type_args[0])[0]

            if not isclass(artefact_class):
                raise ValueError(f"Unexpected artefact class: {artefact_class}")
        else:
            raise ValueError(
                f"Unexpected data type: {type(data)} when constructing input artefact tuples"
            )

        return (artefact_class, data)


class ParamsBase(BaseModel):
    """Base class for Job params of a pipeline order."""

    ANALYSIS_NAME_PATTERN: ClassVar[str] = ""

    analysis_name: str = Field(alias="name", description="name of a analysis")
    job_id: UUID4 = Field(
        default_factory=lambda: uuid4(),
        description="job id of a pipeline order, used for uniqueness of directory names",
    )
    pedigree_json: Pedigree | None = Field(
        None, alias="pedigree", description="pedigree of a triopipe order"
    )
    proband_or_single: str = Field(
        ..., description="proband of triopipe or single of a basepipe order"
    )
    specialized_pipeline: SpecializedPipeline = Field(
        SpecializedPipeline.NoSpecification, description="specialized pipeline of a pipeline order"
    )
    workflow_id: UUID4 | UUID5 = Field(..., description="workflow id of a pipeline order")
    workflow_name: str = Field(
        pattern=rf"^.*_.*_{SAMPLE_ID_PATTERN}_.+_v\d+\.\d+\.\d+$",
        description="workflow name of a pipeline order",
    )

    @field_validator("analysis_name", mode="before")
    @classmethod
    def validate_analysis_name(cls, analysis_name: str) -> str:
        """Ensure analysis name matches pattern."""
        pattern = cls.ANALYSIS_NAME_PATTERN
        assert pattern, "Regex pattern for analysis name must be defined as a class variable"

        if not re.match(pattern, analysis_name):
            raise ValueError(f"Analysis name {analysis_name} does not match pattern {pattern}")

        return analysis_name

    @field_serializer("pedigree_json")
    def serialize_pedigree(self, pedigree_json: Pedigree | None) -> str | None:
        """Serialize to json string for Neo4j."""
        if pedigree_json is None:
            return None
        return pedigree_json.model_dump_json()


class BasePipeOrder(PipelineOrderBase):
    class BasePipeInputs(InputBase):
        """Inputs for a basepipe order."""

        seqdata: InputArtefactType[SeqData]
        taqman: InputArtefactType[TaqmanFile] | None = None

    class BasePipeParams(ParamsBase):
        """Parameters for a basepipe order."""

        ANALYSIS_NAME_PATTERN: ClassVar[str] = rf"^.*_.*_{SAMPLE_ID_PATTERN}_{UUID_PATTERN}$"

        check_taqman: bool = Field(
            ..., description="check taqman of a basepipe order", alias="taqman"
        )

    inputs: BasePipeInputs = Field(..., description="inputs of a basepipe order")
    params: BasePipeParams = Field(..., description="params of a basepipe order")
    samples: list[str] = Field(
        ..., min_length=1, max_length=1, description="samples of a pipeline order"
    )

    @model_validator(mode="before")
    @classmethod
    def validate_model(cls, data: dict) -> dict:
        """Prepare artefact inputs for a basepipe order."""

        sample = data["samples"][0]
        identifiers = Identifiers.from_string(sample)
        data["inputs"] = {
            "seqdata": {
                "seqdata_id": identifiers.seqdata_id,
            }
        }

        data["params"]["proband_or_single"] = sample

        # Construct model to handle alias
        data["params"] = cls.BasePipeParams(**data["params"])
        if data["params"].check_taqman:
            data["inputs"]["taqman"] = {"dna_id": identifiers.dna_id}

        return data


class TrioPipeOrder(PipelineOrderBase):
    class TrioPipeInputs(InputBase):
        """Inputs for a triopipe order."""

        proband_dot_sample: InputArtefactType[DotSampleFile]
        gvcf_1: InputArtefactType[GenomicVariantFile]
        gvcf_2: InputArtefactType[GenomicVariantFile]
        gvcf_3: InputArtefactType[GenomicVariantFile]
        qc_results_1: InputArtefactType[QCResult]
        qc_results_2: InputArtefactType[QCResult]
        qc_results_3: InputArtefactType[QCResult]
        mtvcf_1: InputArtefactType[MTVariantFile] | None = None
        mtvcf_2: InputArtefactType[MTVariantFile] | None = None
        mtvcf_3: InputArtefactType[MTVariantFile] | None = None
        mtvcf_idx_1: InputArtefactType[MTVariantIndexFile] | None = None
        mtvcf_idx_2: InputArtefactType[MTVariantIndexFile] | None = None
        mtvcf_idx_3: InputArtefactType[MTVariantIndexFile] | None = None

    class TrioPipeParams(ParamsBase):
        ANALYSIS_NAME_PATTERN: ClassVar[str] = rf"^.*_.*_{SAMPLE_ID_PATTERN}_TRIO_{UUID_PATTERN}$"

    inputs: TrioPipeInputs = Field(..., description="inputs of a triopipe order")
    params: TrioPipeParams = Field(..., description="params of a triopipe order")
    samples: list[str] = Field(
        ..., min_length=3, max_length=3, description="samples of a pipeline order"
    )

    @model_validator(mode="before")
    @classmethod
    def validate_model(cls, data: dict) -> dict:
        """Prepare artefact inputs for a triopipe order."""

        proband_sample = data["params"]["pedigree"]["proband"]["sample"]
        data["inputs"] = {}
        data["inputs"]["proband_dot_sample"] = {
            "seqdata_id": Identifiers.from_string(proband_sample).seqdata_id,
        }
        for i, sample in enumerate(data["samples"]):
            identifiers = Identifiers.from_string(sample)
            data["inputs"].update(
                {
                    f"gvcf_{i + 1}": {"seqdata_id": identifiers.seqdata_id},
                    f"qc_results_{i + 1}": {"seqdata_id": identifiers.seqdata_id},
                    f"mtvcf_{i + 1}": {"seqdata_id": identifiers.seqdata_id},
                    f"mtvcf_idx_{i + 1}": {"seqdata_id": identifiers.seqdata_id},
                }
            )

        data["params"]["proband_or_single"] = proband_sample

        return data


class AnnoPipeOrder(PipelineOrderBase):
    class AnnoPipeSingleInputs(InputBase):
        """Inputs for an annopipe order for single."""

        dot_sample_1: InputArtefactType[DotSampleFile]
        qc_result: InputArtefactType[QCResult]
        qc_report_md: InputArtefactType[QCReport]
        qc_warnings_md: InputArtefactType[QCWarnings]
        vcf_file: InputArtefactType[VariantFile]
        convading_log: InputArtefactType[ConvadingLogFile] | None = None
        convading_longlist: InputArtefactType[ConvadingLonglistFile] | None = None
        convading_shortlist: InputArtefactType[ConvadingShortlistFile] | None = None
        convading_totallist: InputArtefactType[ConvadingTotallistFile] | None = None
        bam_1: InputArtefactType[SeqMappingFile]
        bigwig_1: InputArtefactType[CoverageBigWig]
        cnv_1: InputArtefactType[CNVBedFile] | InputArtefactType[CNVVariantFile]

    class AnnoPipeTrioInputs(InputBase):
        """Inputs for an annopipe order for trio."""

        dot_sample_1: InputArtefactType[DotSampleFile]
        dot_sample_2: InputArtefactType[DotSampleFile]
        dot_sample_3: InputArtefactType[DotSampleFile]
        qc_result: InputArtefactType[QCResultTrio]
        qc_report_md: InputArtefactType[QCReportTrio]
        qc_warnings_md: InputArtefactType[QCWarningsTrio]
        vcf_file: InputArtefactType[MultiSampleVariantFile]
        bam_1: InputArtefactType[SeqMappingFile]
        bigwig_1: InputArtefactType[CoverageBigWig]
        cnv_1: InputArtefactType[CNVBedFile] | InputArtefactType[CNVVariantFile]
        bam_2: InputArtefactType[SeqMappingFile]
        bigwig_2: InputArtefactType[CoverageBigWig]
        cnv_2: InputArtefactType[CNVBedFile] | InputArtefactType[CNVVariantFile]
        bam_3: InputArtefactType[SeqMappingFile]
        bigwig_3: InputArtefactType[CoverageBigWig]
        cnv_3: InputArtefactType[CNVBedFile] | InputArtefactType[CNVVariantFile]

    class AnnoPipeParams(ParamsBase):
        ANALYSIS_NAME_PATTERN: ClassVar[str] = (  # fmt: skip
            rf"^.*_.*_{SAMPLE_ID_PATTERN}_(?:TRIO_)?{GENEPANEL_PATTERN}_{UUID_PATTERN}$"
        )

        genepanel: str = Field(
            pattern=rf"^{GENEPANEL_PATTERN}$", description="genepanel of an annopipe order"
        )

    inputs: AnnoPipeSingleInputs | AnnoPipeTrioInputs = Field(
        ..., description="inputs of an annopipe order"
    )
    params: AnnoPipeParams = Field(..., description="params of an annopipe order")
    samples: list[str] = Field(..., description="samples of a annopipe order")

    @model_validator(mode="before")
    @classmethod
    def validate_model(cls, data: dict) -> dict:
        """Prepare artefact inputs for a annopipe order (single or trio)."""

        data["inputs"] = {}

        # Annopipe for single or trio
        if len(data["samples"]) == 1:
            sample = data["samples"][0]
            identifiers = Identifiers.from_string(sample)
            single_or_proband_id = identifiers.seqdata_id
            proband_or_single = sample

            # Single EKG
            if identifiers.project_type.lower() == "ekg":
                data["inputs"].update(
                    {
                        "convading_log": {
                            "seqdata_id": single_or_proband_id,
                        },
                        "convading_longlist": {
                            "seqdata_id": single_or_proband_id,
                        },
                        "convading_shortlist": {
                            "seqdata_id": single_or_proband_id,
                        },
                        "convading_totallist": {
                            "seqdata_id": single_or_proband_id,
                        },
                    }
                )

            input_type = cls.AnnoPipeSingleInputs

        elif len(data["samples"]) == 3:
            proband_or_single = data["params"]["pedigree"]["proband"]["sample"]
            identifiers = Identifiers.from_string(proband_or_single)
            single_or_proband_id = identifiers.seqdata_id
            input_type = cls.AnnoPipeTrioInputs  # type: ignore[assignment]

        else:
            raise ValueError(
                f"Unexpected number of samples: {len(data['samples'])}, expected 1 (single) or 3 (trio)"
            )

        # QC files
        data["inputs"].update(
            {
                "qc_result": {
                    "seqdata_id": single_or_proband_id,
                },
                "qc_report_md": {
                    "seqdata_id": single_or_proband_id,
                },
                "qc_warnings_md": {
                    "seqdata_id": single_or_proband_id,
                },
            }
        )

        # VCF file
        data["inputs"]["vcf_file"] = {
            "seqdata_id": single_or_proband_id,
        }

        cnv_artefact_type = (
            CNVBedFile if identifiers.project_type.lower() == "excap" else CNVVariantFile
        )
        for i, sample in enumerate(data["samples"]):
            sample_seqdata_id = Identifiers.from_string(sample).seqdata_id

            # Dot sample files
            data["inputs"][f"dot_sample_{i + 1}"] = {"seqdata_id": sample_seqdata_id}

            data["inputs"].update(
                {
                    f"bam_{i + 1}": {
                        "seqdata_id": sample_seqdata_id,
                    },
                    f"bigwig_{i + 1}": {
                        "seqdata_id": sample_seqdata_id,
                    },
                    f"cnv_{i + 1}": (
                        cnv_artefact_type,
                        {
                            "seqdata_id": sample_seqdata_id,
                        },
                    ),
                }
            )

        data["params"]["proband_or_single"] = proband_or_single

        data["inputs"] = input_type(**data["inputs"])

        return data


PipelineOrderType = BasePipeOrder | TrioPipeOrder | AnnoPipeOrder
PipelineOrder: TypeAdapter[PipelineOrderType] = TypeAdapter(
    Annotated[
        Annotated[BasePipeOrder, Tag(PipelineType.basepipe)]
        | Annotated[TrioPipeOrder, Tag(PipelineType.triopipe)]
        | Annotated[AnnoPipeOrder, Tag(PipelineType.annopipe)],
        Discriminator(lambda v: v["type"]),
    ]
)

import re
import subprocess
from datetime import datetime
from pathlib import Path
from typing import Any
from uuid import UUID

from pydantic import BaseModel

from config import config


class Identifiers(BaseModel):
    owner_id: str
    project_type: str
    batch_id: str
    dna_id: str
    seqdata_id: UUID

    @classmethod
    def from_path(cls, path: Path) -> "Identifiers":
        return cls.from_composed_name(path.parent.name)

    @classmethod
    def from_string(cls, composed_name: str) -> "Identifiers":
        return cls.from_composed_name(composed_name)

    @classmethod
    def from_composed_name(cls, composed_name: str) -> "Identifiers":
        regex = re.compile(
            r"^(?P<owner_id>[^_]*)_(?P<batch_id>[^_]*)_(?P<dna_id>[^_]*)_(?P<seqdata_id>[^_]*)$"
        )
        matched = regex.match(composed_name)

        if not matched:
            raise RuntimeError(f"Failed to parse identifiers from {composed_name}")
        else:
            project_type = re.match(
                r"(?P<project_type>\D+)\d+",
                matched.groupdict().get("batch_id"),  # type: ignore[arg-type]
            )
            return cls(**matched.groupdict(), **(project_type.groupdict() if project_type else {}))


def run_subprocess(cmd: str, **popen_args: Any) -> int:
    """Run a subprocess synchronously."""
    return subprocess.check_call(cmd, shell=True, **popen_args)


def get_job_output_dir(
    prefixes: list[str], workflow_id: UUID, job_id: UUID, step_name: str
) -> Path:
    """Returns a standard job output directory path.

    ROOT/YYYY/prefixes_workflow-id/prefixes_stepname_job-id where ROOT comes from config.
    Prefixes are optional but usually equal to seqdata [owner-id, batch-id, dna-id]
    e.g. /tmp/maestro/jobs/2024/OUSAMG_wgs123_HG12345678_d01f47a5-1e0c-4057-a62d-3ca40482a727/Diag_wgs123_HG12345678_Basepipe_a8f295db-3838-488d-937f-e6f4aebc0557
    """
    assert isinstance(workflow_id, UUID)
    assert isinstance(job_id, UUID)
    root = config.steps.job_root_dir
    wf = f"_{workflow_id}" if prefixes else workflow_id
    step = f"_{step_name}" if prefixes else step_name
    return (
        root
        / f"{datetime.now().year}"
        / f"{'_'.join(prefixes)}{wf!s}"
        / f"{'_'.join(prefixes)}{step}_{job_id!s}"
    )


def seqdata_maker_workflow_id_and_name(path: Path) -> tuple[UUID, str]:
    """
    Extracts the seqdata id to be used as the workflow id,
    and the dna_id to be used for the workflow name.
    """
    identifiers = Identifiers.from_path(path)
    return identifiers.seqdata_id, f"SeqDataMaker:{identifiers.dna_id}"


def strip_uuid(uuid_str: str) -> str:
    """Strips UUIDs from a string, plus any leading separators (':', '-', or '_')."""
    return re.sub(
        r"[-_:]?[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}", "", uuid_str
    )

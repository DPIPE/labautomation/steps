import logging
import logging.config
import sys
from logging.handlers import RotatingFileHandler

import structlog

from config import config

humanized_timestamper = structlog.processors.TimeStamper(fmt="%Y-%m-%d %H:%M:%S")
iso_timestamper = structlog.processors.TimeStamper(fmt="iso")

shared_processors = [
    structlog.stdlib.add_log_level,
    structlog.stdlib.add_logger_name,
    structlog.stdlib.ProcessorFormatter.remove_processors_meta,
]

LOGGING_CONFIG = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "console": {
            "()": structlog.stdlib.ProcessorFormatter,
            "processors": [
                humanized_timestamper,
                structlog.processors.CallsiteParameterAdder(
                    {
                        structlog.processors.CallsiteParameter.FILENAME,
                        structlog.processors.CallsiteParameter.FUNC_NAME,
                        structlog.processors.CallsiteParameter.LINENO,
                    }
                ),
                structlog.stdlib.PositionalArgumentsFormatter(),
                structlog.dev.ConsoleRenderer(
                    colors=sys.stderr.isatty(),
                ),
            ],
            "foreign_pre_chain": shared_processors,
        },
        "json": {
            "()": structlog.stdlib.ProcessorFormatter,
            "processors": [
                iso_timestamper,
                structlog.processors.dict_tracebacks,
                structlog.processors.JSONRenderer(),
            ],
            "foreign_pre_chain": shared_processors,
        },
    },
    "handlers": {
        "default": {
            "level": config.steps.logging_level,
            "class": logging.StreamHandler,
            "formatter": "console",
        },
        "file": {
            "level": "DEBUG",
            "class": RotatingFileHandler,
            "filename": config.steps.logging_path / "steps.log",
            "maxBytes": config.steps.logging_max_bytes,
            "backupCount": config.steps.logging_backup_count,
            "formatter": "json",
        },
    },
    "loggers": {
        "steps": {
            "handlers": ["default", "file"],
            "level": config.steps.logging_level,
            "propagate": True,
        }
    },
}

logging.config.dictConfig(LOGGING_CONFIG)

structlog.configure(
    logger_factory=structlog.stdlib.LoggerFactory(),
    wrapper_class=structlog.stdlib.BoundLogger,
    cache_logger_on_first_use=True,
)

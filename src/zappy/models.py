from collections.abc import AsyncGenerator
from datetime import date, datetime
from enum import StrEnum, auto
from typing import Literal

import schemas.models
from artefacts.pipeline_order import (
    AnnoPipeOrder,
    BasePipeOrder,
    PipelineOrderType,
    PipelineType,
    TrioPipeOrder,
)
from artefacts.seqdata import GivenSexEnum
from pydantic import BaseModel, Field

from maestro.models import JobStatus

from .exceptions import InvalidSapioOrder
from .sapioclient import SapioClient
from .sapiorecord import SapioRecord


class TriggerTypeEnum(StrEnum):
    NEW_SEQUENCING_DATASET = auto()
    NEW_ANALYSIS = auto()
    NEW_DEMULTIPLEXING = auto()


class HandleStatusEnum(StrEnum):
    UNHANDLED = auto()
    HANDLED = auto()
    ERROR = auto()


class PedigreeRoleEnum(StrEnum):
    PROBAND = auto()
    MOTHER = auto()
    FATHER = auto()


class BioinfTrigger(SapioRecord):
    type: TriggerTypeEnum
    root_id: int
    trigger_time: datetime
    handle_status: HandleStatusEnum = HandleStatusEnum.UNHANDLED
    handle_time: datetime | None = None


class Individual(SapioRecord):
    sample: str | None
    given_sex: GivenSexEnum
    mother_id: int | None = None
    father_id: int | None = None


class Analysis(SapioRecord):
    priority: int
    date_analysis_requested: date
    proband_id: int
    specialized_pipeline: str | None
    taqman: bool | None
    genepanel: str | None = None
    status: JobStatus | None = None

    async def create_pipeline_orders(self, workflow_id: str) -> AsyncGenerator[PipelineOrderType]:
        proband = await SapioClient().get_record(Individual, self.proband_id)
        if proband.sample is None:
            raise InvalidSapioOrder("Proband must have a DNA sample")
        pedigree: dict[PedigreeRoleEnum, Individual] = {PedigreeRoleEnum.PROBAND: proband}
        if proband.mother_id is not None:
            mother = await SapioClient().get_record(Individual, proband.mother_id)
            if mother.sample is not None:
                pedigree[PedigreeRoleEnum.MOTHER] = mother
        if proband.father_id is not None:
            father = await SapioClient().get_record(Individual, proband.father_id)
            if father.sample is not None:
                pedigree[PedigreeRoleEnum.FATHER] = father
        if len(pedigree) not in {1, 3}:
            raise NotImplementedError("Only single and trio analysis is supported")
        owner_id, batch_id, dna_id, _ = proband.sample.split("_")
        workflow_name = f"{owner_id}_{batch_id}_{dna_id}"
        if self.genepanel is not None:
            workflow_name += f"_{self.genepanel}"
        common_params = {
            "workflow_id": workflow_id,
            "workflow_name": workflow_name,
        }
        for individual in pedigree.values():
            yield BasePipeOrder(
                type=PipelineType.basepipe,
                priority=self.priority,
                params=common_params
                | {
                    "name": f"{owner_id}_{batch_id}_{dna_id}_{workflow_id}",
                    "taqman": self.taqman,
                },
                date_analysis_requested=self.date_analysis_requested,
                specialized_pipeline=self.specialized_pipeline,
                samples=[individual.sample],
                gender=individual.given_sex,
                inputs={},
            )
        if len(pedigree) == 3:
            pipeline_order_pedigree = {
                role: {"sample": individual.sample, "gender": individual.given_sex}
                for role, individual in pedigree.items()
            }
            yield TrioPipeOrder(
                type=PipelineType.triopipe,
                priority=self.priority,
                params=common_params
                | {
                    "name": f"{owner_id}_{batch_id}_{dna_id}_TRIO_{workflow_id}",
                    "pedigree": pipeline_order_pedigree,
                },
                date_analysis_requested=self.date_analysis_requested,
                specialized_pipeline=self.specialized_pipeline,
                samples=[individual.sample for individual in pedigree.values()],
                inputs={},
            )
        if self.genepanel is not None:
            annopipe_params = common_params | {
                "name": f"{owner_id}_{batch_id}_{dna_id}_{self.genepanel}_{workflow_id}",
                "genepanel": self.genepanel,
            }
            if len(pedigree) == 1:
                yield AnnoPipeOrder(
                    type=PipelineType.annopipe,
                    priority=self.priority,
                    params=annopipe_params,
                    date_analysis_requested=self.date_analysis_requested,
                    specialized_pipeline=self.specialized_pipeline,
                    samples=[proband.sample],
                    gender=proband.given_sex,
                    inputs={},
                )
            elif len(pedigree) == 3:
                yield AnnoPipeOrder(
                    type=PipelineType.annopipe,
                    priority=self.priority,
                    params=annopipe_params
                    | {
                        "pedigree": pipeline_order_pedigree,
                    },
                    date_analysis_requested=self.date_analysis_requested,
                    specialized_pipeline=self.specialized_pipeline,
                    samples=[individual.sample for individual in pedigree.values()],
                    inputs={},
                )
            else:
                assert False


class TranscriptHGVS(SapioRecord):
    allele_id: int
    gene: str = Field(..., examples=["BRCA1", "BRCA2", "TP53", "MLH1", "MSH2"])
    hgvsc: str | None = Field(..., examples=["NM_007294.3:c.68_69del", "NM_000059.3:c.68_69del"])
    hgvsp: str | None = Field(
        ..., examples=["NP_009225.1:p.Gly23ValfsTer17", "NP_009225.1:p.Gly23ValfsTer17"]
    )


class Allele(SapioRecord):
    reference_genome: str = Field(..., examples=["GRCh37"])
    chromosome: str = Field(..., examples=["1", "2", "16", "17", "18", "X"])
    vcf_pos: int = Field(..., examples=[12345, 67890, 1234567, 8901234])
    vcf_ref: str = Field(..., examples=["A", "TCC", "CA", "G"])
    vcf_alt: str = Field(..., examples=["TACG", "C", "G", "AAAC"])
    length: int = Field(..., examples=[1, 2, 3, 4, 5])
    change_type: str = Field(
        description="Type of change",
        examples=[
            "SNP",
            "del",
            "ins",
            "indel",
            "dup",
            "dup_tandem",
            "del_me",
            "ins_me",
            "inv",
            "bnd",
        ],
    )
    genome_hgvs: str = Field(
        description="HGVS string representing on the given reference genome contig. Note: *Should* be redundant with the other fields in this model.",
        examples=[
            "NC_000023.10:g.33038255C>A",
            "NC_000023.10:g.33344590_33344592del",
            "NC_000023.10:g.32343183dup",
            "NC_000023.10:g.32343183_32343184insA",
            "NC_000023.10:g.32343183_32343184inv",
            "NC_000023.10:g.32343183_32343185delinsTGG",
        ],
    )

    @classmethod
    async def find(cls, allele: schemas.models.Allele) -> "Allele | None":
        async for db_allele in SapioClient().get_records_matching(
            cls, "genome_hgvs", [allele.genome_hgvs]
        ):
            if db_allele.model_dump(exclude={"RecordId"}) == allele.model_dump(
                exclude={"transcript_hgvs"}
            ):
                db_transcript_hgvs = {
                    tuple(sorted(hgvs.model_dump(exclude={"RecordId", "allele_id"}).items()))
                    async for hgvs in SapioClient().get_records_matching(
                        TranscriptHGVS, "allele_id", [db_allele.RecordId]
                    )
                }
                transcript_hgvs = {
                    tuple(sorted(hgvs.model_dump().items())) for hgvs in allele.transcript_hgvs
                }
                if db_transcript_hgvs == transcript_hgvs:
                    return db_allele
        return None

    @classmethod
    async def find_or_create(cls, allele: schemas.models.Allele) -> "Allele":
        record = await cls.find(allele)
        if record is not None:
            return record
        [record_id] = await SapioClient().batchupdate(
            new_records=[cls(RecordId=-1, **allele.model_dump(exclude={"transcript_hgvs"}))]
        )
        await SapioClient().batchupdate(
            new_records=[
                TranscriptHGVS(RecordId=-1 - i, allele_id=record_id, **transcript_hgvs.model_dump())
                for i, transcript_hgvs in enumerate(allele.transcript_hgvs)
            ]
        )
        return await SapioClient().get_record(cls, record_id)


class NeedsVerification(SapioRecord):
    """Variant that needs verification by some other method."""

    workflow_step_change_id: int
    allele_id: int = Field(description="Unambiguous representation of the variant")
    zygosity: str = Field(
        description="Zygosity of the variant - TBD: what are the possible values?",
        examples=["heterozygous", "homozygous"],
    )
    method: str = Field(
        description="Method that should be used to identify the variant - TBD: what are the possible values?",
        examples=["Sanger sequencing", "MLPA", "Array CGH", "PCR", "NGS"],
    )

    @classmethod
    async def create(
        cls, workflow_change_step_id: int, need_verification: schemas.models.NeedsVerification
    ) -> "NeedsVerification":
        allele = await Allele.find_or_create(need_verification.allele)
        return cls(
            workflow_step_change_id=workflow_change_step_id,
            allele_id=allele.RecordId,
            **need_verification.model_dump(exclude={"allele"}),
        )


class ACMG(SapioRecord):
    variant_id: int
    criteria: str = Field(
        description="ACMG criteria", examples=["PVS1", "PS2", "PM2", "PP3", "BP7"]
    )
    strength: str = Field(description="Strength of the evidence", examples=["VS", "S", "PP", "BP"])
    evaluation: str = Field(
        description="Evaluation of the ACMG criteria as HTML string", examples=["<div>Text</div>"]
    )


class VariantReference(SapioRecord):
    variant_id: int
    reference: str = Field(
        description="Reference available for the variant (not necessarily used for classification) - format TBD",
        examples=["PMID:12345678", "PMID:23456789"],
    )


class Variant(SapioRecord):
    """Variant that has been reported in the analysis, marked semi-automatically on the ELLA report page"""

    workflow_step_change_id: int
    allele_id: int = Field(description="Unambiguous representation of the variant")
    zygosity: str = Field(
        description="Zygosity of the variant - TBD: what are the possible values?",
        examples=["heterozygous", "homozygous"],
    )
    classification: (
        Literal["1", "2", "3", "4", "5", "Risk factor", "Not provided", "Drug response"] | None
    ) = Field(description="Classification of the variant at this point in time")
    date_classified: datetime | None = Field(
        description="Datetime the variant was classified (UTC)"
    )
    username: str = Field(
        description="ELLA username of the user who classified the variant",
        examples=["user1", "user2"],
    )
    report_text: str = Field(
        description="HTML string to be included in the report for this variant - Note: Does not include 'header', e.g. NM_004006.2:c.[93+1G>T];[c.93+1=]",
        examples=[
            "<div>Variants in this gene are associated with a high risk of developing cancer.</div>"
        ],
    )

    @classmethod
    async def create(
        cls, workflow_change_step_id: int, variant: schemas.models.Variant
    ) -> "Variant":
        allele = await Allele.find_or_create(variant.allele)
        return cls(
            workflow_step_change_id=workflow_change_step_id,
            allele_id=allele.RecordId,
            **variant.model_dump(exclude={"allele", "acmg_criteria", "references"}),
        )


class ReportedVariant(Variant):
    pass


class NotRelevantVariant(Variant):
    pass


class EllaWorkflowStepChange(SapioRecord):
    analysis_id: int
    analysis_name: str = Field(
        description="Name of the analysis, as it appears in ELLA",
        examples=["Diag-wgs123-HG327582-Panel-v1.0.0"],
    )
    timestamp: datetime = Field(
        description="Datetime of the analysis, in UTC",
    )
    username: str = Field(
        description="ELLA username of the user who made the change", examples=["user1", "user2"]
    )
    workflow_step: Literal["Not ready", "Interpretation", "Review", "Medical review"] = Field(
        description="New workflow step of the analysis"
    )
    finalized: bool = Field(
        description="Whether the analysis is finalized - if true, the analysis is considered complete"
    )
    analysis_report: str = Field(
        description="The analysis report as an HTML string", examples=["<div>Report text</div>"]
    )


class EllaInput(BaseModel):
    sapio_id: int

import asyncio
import logging
import uuid
from datetime import UTC, datetime
from typing import override

from artefacts.vcpipe import AnnopipeOutput
from nats.aio.msg import Msg
from schemas.models import WorkflowStepChange
from workflowsteps.dotanalysiswatcher import DotanalysisFileWatcher

from config import config
from maestro import Job, MessageWatcher, WorkflowModel
from maestro.config import Config
from maestro.models import WorkflowStepModelWithArtefacts
from maestro.nats.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import fetch_node_by_rel, read_node
from maestro.workflow_utils import order_workflow_with_job_filtering

from .models import (
    ACMG,
    Analysis,
    BioinfTrigger,
    EllaInput,
    EllaWorkflowStepChange,
    HandleStatusEnum,
    NeedsVerification,
    NotRelevantVariant,
    ReportedVariant,
    TriggerTypeEnum,
    Variant,
    VariantReference,
)
from .sapioclient import SapioClient, SapioCommunicationError

logger = logging.getLogger("steps")


class EllaDelivery(MessageWatcher):
    @override
    async def process(self, input: AnnopipeOutput, sapio_id: int) -> list:
        async with NATSSession() as nats_session:
            await nats_session.publish(
                "input",
                EllaInput(sapio_id=sapio_id).model_dump_json().encode(),
                "ella",
            )
        return []


def uuid_from_sapio_record_id(record_id: int) -> str:
    return str(uuid.uuid5(uuid.NAMESPACE_URL, f"sapio:{record_id}"))


async def handle_new_analysis(trigger: BioinfTrigger) -> bool:
    analysis = await SapioClient().get_record(Analysis, trigger.root_id)
    assert trigger.RecordId is not None
    workflow_id = uuid_from_sapio_record_id(trigger.RecordId)
    async with Neo4jTransaction() as neo4j_tx:
        if await read_node(neo4j_tx, WorkflowModel, {"maestro_id": workflow_id}) is not None:
            return True
    async for pipeline_order in analysis.create_pipeline_orders(workflow_id):
        await DotanalysisFileWatcher.order_workflow(pipeline_order)
    workflow = WorkflowModel(
        maestro_id=workflow_id,
        workflow_name=pipeline_order.params.workflow_name,
        jobs={
            Job(
                workflow_step=EllaDelivery,
                inputs={"input": (AnnopipeOutput, {"workflow_id": workflow_id})},
                params={"sapio_id": analysis.RecordId},
            )
        },
    )
    await order_workflow_with_job_filtering(workflow)
    return True


async def handle_trigger(trigger: BioinfTrigger) -> bool:
    if trigger.type == TriggerTypeEnum.NEW_ANALYSIS:
        return await handle_new_analysis(trigger)
    else:
        raise NotImplementedError(f"Unsupported trigger type: {trigger.type}")


async def handle_workflow_step_done(msg: Msg) -> None:
    workflow_step = WorkflowStepModelWithArtefacts.model_validate_json(msg.data.decode())
    if hasattr(workflow_step, "sapio_id"):
        analysis = await SapioClient().get_record(Analysis, workflow_step.sapio_id)
        async with Neo4jTransaction() as neo4j_tx:
            job = await fetch_node_by_rel(
                neo4j_tx,
                WorkflowStepModelWithArtefacts,
                {"maestro_id": workflow_step.maestro_id},
                Job,
            )
        assert job is not None
        analysis.status = job.status
        await SapioClient().batchupdate(updated_records=[analysis])
        await msg.ack()
    else:
        await msg.ack()


async def handle_ella_workflow_step_change(msg: Msg) -> None:
    workflow_step_change = WorkflowStepChange.model_validate_json(msg.data.decode())
    analysis = await SapioClient().get_record(Analysis, int(workflow_step_change.external_id))
    already_reported_workflow_step_changes = [
        workflow_step_change
        async for workflow_step_change in SapioClient().get_records_matching(
            EllaWorkflowStepChange, "analysis_id", [analysis.RecordId]
        )
    ]
    # If the workflow step change is older than the latest already reported workflow step change,
    # we can ignore it.
    if len(already_reported_workflow_step_changes) > 0 and workflow_step_change.timestamp <= max(
        workflow_step_change.timestamp
        for workflow_step_change in already_reported_workflow_step_changes
    ):
        await msg.ack()
        return

    [workflow_step_change_id] = await SapioClient().batchupdate(
        new_records=[
            EllaWorkflowStepChange(
                RecordId=-1,
                analysis_id=analysis.RecordId,
                **workflow_step_change.model_dump(
                    exclude={
                        "external_id",
                        "reported_variants",
                        "not_relevant_variants",
                        "needs_verification",
                    }
                ),
            )
        ]
    )

    first_stage_new_records: list[Variant | NeedsVerification] = (
        [
            await ReportedVariant.create(workflow_step_change_id, variant)
            for variant in workflow_step_change.reported_variants
        ]
        + [
            await NotRelevantVariant.create(workflow_step_change_id, variant)
            for variant in workflow_step_change.not_relevant_variants
        ]
        + [
            await NeedsVerification.create(workflow_step_change_id, needs_verification)
            for needs_verification in workflow_step_change.needs_verification
        ]
    )
    for i, first_stage_new_record in enumerate(first_stage_new_records):
        first_stage_new_record.RecordId = -1 - i
    new_record_ids = await SapioClient().batchupdate(new_records=first_stage_new_records)

    second_stage_new_records: list[ACMG | VariantReference] = []
    for (
        record_id,
        variant,
    ) in zip(
        new_record_ids,
        workflow_step_change.reported_variants + workflow_step_change.not_relevant_variants,
    ):
        for acmg_criterium in variant.acmg_criteria:
            second_stage_new_records.append(
                ACMG(variant_id=record_id, **acmg_criterium.model_dump())
            )
        for reference in variant.references:
            second_stage_new_records.append(
                VariantReference(variant_id=record_id, reference=reference)
            )
    for i, second_stage_new_record in enumerate(second_stage_new_records):
        second_stage_new_record.RecordId = -1 - i

    await SapioClient().batchupdate(new_records=second_stage_new_records)

    await msg.ack()


async def main() -> None:
    if config.zappy.sapio_url is None:
        return
    async with NATSSession() as nats_session:
        await nats_session.subscribe(
            Config.STREAM, "workflow_step.*.done", handle_workflow_step_done, durable="zappy"
        )
        await nats_session.subscribe(
            "ella", "output", handle_ella_workflow_step_change, durable="zappy"
        )
        await loop()


async def loop() -> None:
    while True:
        try:
            async for trigger in SapioClient().get_records_matching(
                BioinfTrigger, "handle_status", [HandleStatusEnum.UNHANDLED]
            ):
                if await handle_trigger(trigger):
                    trigger.handle_status = HandleStatusEnum.HANDLED
                    trigger.handle_time = datetime.now(UTC)
                    await SapioClient().batchupdate(updated_records=[trigger])
        except SapioCommunicationError as e:
            logger.exception(e)

        await asyncio.sleep(config.zappy.polling_interval)

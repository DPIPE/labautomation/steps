from collections.abc import AsyncGenerator, Sequence
from contextlib import asynccontextmanager
from typing import Any, TypeVar

from aiohttp import ClientResponse, ClientSession, Fingerprint

from config import config

from .sapiorecord import SapioRecord


class SapioCommunicationError(RuntimeError):
    pass


class SapioRecordNotFound(RuntimeError):
    pass


class SapioClient:
    """Client for interacting with the Sapio API.

    Handles authentication, requests, and response processing for Sapio API endpoints.
    Provides methods for retrieving and updating records in Sapio.
    """

    def __init__(self, read_only: bool = False) -> None:
        "Initialize the SapioClient with connection details and authentication."
        assert config.zappy.sapio_url is not None
        self.url = config.zappy.sapio_url
        self.headers = {}
        if config.zappy.sapio_guid is not None:
            self.headers["X-APP-KEY"] = config.zappy.sapio_guid
        if config.zappy.sapio_api_token is not None:
            self.headers["X-API-TOKEN"] = config.zappy.sapio_api_token
        self.ssl = (
            Fingerprint(bytes.fromhex(config.zappy.sapio_server_cert_fingerprint))
            if config.zappy.sapio_server_cert_fingerprint is not None
            else True
        )
        self.read_only = read_only

    @asynccontextmanager
    async def _request(
        self,
        method: str,
        path: str,
        **kwargs: Any,
    ) -> AsyncGenerator[ClientResponse]:
        """Internal method to make HTTP requests to Sapio API.

        Args:
            method: HTTP method (GET, POST, etc.)
            path: API endpoint path
            **kwargs: Additional arguments to pass to the request

        Yields:
            ClientResponse: The response from the API
        """
        async with ClientSession() as session:
            async with session.request(
                method, self.url + path, headers=self.headers, ssl=self.ssl, **kwargs
            ) as response:
                yield response

    @asynccontextmanager
    async def get(self, path: str, **kwargs: Any) -> AsyncGenerator[ClientResponse]:
        """Make a GET request to the Sapio API.

        Args:
            path: API endpoint path
            **kwargs: Additional arguments to pass to the request

        Yields:
            ClientResponse: The response from the API
        """
        async with self._request("GET", path, **kwargs) as response:
            yield response

    @asynccontextmanager
    async def post(self, path: str, **kwargs: Any) -> AsyncGenerator[ClientResponse]:
        """Make a POST request to the Sapio API.

        Args:
            path: API endpoint path
            **kwargs: Additional arguments to pass to the request

        Yields:
            ClientResponse: The response from the API
        """
        async with self._request("POST", path, **kwargs) as response:
            yield response

    async def _handle_response(self, response: ClientResponse) -> Any:
        """Handle and process the API response.

        Args:
            response: The response from the API

        Returns:
            Parsed JSON response if successful.

        Raises:
            SapioCommunicationError if unsuccessful.
        """
        if response.status == 200:
            return await response.json()
        raise SapioCommunicationError(
            f"API request to Sapio failed - Status: {response.status}, URL: {response.url}, Method: {response.method}"
        )

    SapioRecordSubType = TypeVar("SapioRecordSubType", bound=SapioRecord)

    async def get_records_matching(
        self, record_type: type[SapioRecordSubType], field_name: str, field_values: list
    ) -> AsyncGenerator[SapioRecordSubType]:
        """Retrieve records matching specified field values from Sapio.

        Yields:
            SapioRecordSubType: Records matching the specified field values.

        Raises:
            SapioCommunicationError if unsuccessful.
        """
        async with self.post(
            "/datarecordmanager/querydatarecords",
            params={"dataTypeName": record_type.__name__, "dataFieldName": field_name},
            json=field_values,
        ) as response:
            result = await self._handle_response(response)
            if result is not None:
                for result in result["resultList"]:
                    for field_name in result["fields"].keys() - record_type.model_fields.keys():
                        result["fields"].pop(field_name)
                    yield record_type.model_validate(result["fields"])

    async def batchupdate(
        self,
        new_records: Sequence[SapioRecord] | None = None,
        updated_records: Sequence[SapioRecord] | None = None,
    ) -> Sequence[int]:
        """Add and/or update records in Sapio.

        Args:
            new_records: New records to add
            updated_records: Existing records to update

        Returns:
            List of record IDs for the new records, or empty list if "new_records" was None or empty.

        Raises:
            SapioCommunicationError if unsuccessful.
        """

        assert not self.read_only

        # Assert that all new record IDs are negative and unique.
        new_record_ids = {record.RecordId for record in new_records or []}
        assert len(new_record_ids) == len(new_records or [])
        assert all({id is not None and id < 0 for id in new_record_ids})
        # Assert that all updated record IDs are positive and unique.
        updated_record_ids = {record.RecordId for record in updated_records or []}
        assert len(updated_record_ids) == len(updated_records or [])
        assert all({id is not None and id > 0 for id in updated_record_ids})

        def wrap(record: SapioRecord) -> dict:
            return {
                "dataTypeName": type(record).__name__,
                "recordId": record.RecordId,
                "fields": record.model_dump(mode="json", exclude={"RecordId"}),
            }

        json = {
            "recordsAdded": [
                wrap(record) | {"new": True, "deleted": False} for record in new_records or []
            ],
            "recordFieldsChanged": [
                wrap(record) | {"new": False, "deleted": False} for record in updated_records or []
            ],
            "recordsDeleted": [],
        }
        async with self.post("/datarecordlist/runbatchupdate", json=json) as response:
            result = await self._handle_response(response)
            return [
                result["addedRecordUpdates"][str(record.RecordId)]["recordId"]
                for record in new_records or []
            ]

    async def get_record(
        self, record_type: type[SapioRecordSubType], record_id: int
    ) -> SapioRecordSubType:
        """Retrieve SapioRecordSubType record by ID from Sapio.

        Args:
            record_id: The ID of the record to retrieve

        Returns:
            SapioRecordSubType: The retrieved record.

        Raises:
            SapioRecordNotFound if the record is not found.
            SapioCommunicationError if unsuccessful.
        """
        async with self.get(
            "/datarecord", params={"dataTypeName": record_type.__name__, "recordId": record_id}
        ) as response:
            if response.status == 204:
                raise SapioRecordNotFound(
                    f"Record not found: Type: {record_type.__name__} ID: {record_id}"
                )
            result = await self._handle_response(response)
            for field_name in result["fields"].keys() - record_type.model_fields.keys():
                result["fields"].pop(field_name)
            return record_type.model_validate(result["fields"])

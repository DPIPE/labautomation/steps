from pydantic import BaseModel, ConfigDict


class SapioRecord(BaseModel):
    model_config = ConfigDict(extra="forbid")

    RecordId: int | None = None

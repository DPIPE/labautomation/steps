"""
File watcher for dotanalysis pipeline order files.
Order pipeline jobs based on the pipeline type and create DotAnalysisFileArtefact.
"""

import json
import logging
from typing import override

from artefacts.pipeline_order import (
    DotAnalysisFileArtefact,
    PipelineOrder,
    PipelineOrderType,
    PipelineType,
)
from maestro import (
    FileArtefactModel,
    FileWatcher,
    Job,
    WorkflowModel,
)
from maestro.workflow_utils import order_workflow_with_job_filtering
from utils import strip_uuid

from config import config
from workflowsteps.annopipe import Annopipe
from workflowsteps.basepipe import Basepipe
from workflowsteps.triopipe import Triopipe

logger = logging.getLogger("steps")


WORKFLOW_STEPS: dict[PipelineType, type[Basepipe] | type[Triopipe] | type[Annopipe]] = {
    PipelineType.basepipe: Basepipe,
    PipelineType.triopipe: Triopipe,
    PipelineType.annopipe: Annopipe,
}

DOTANALYSIS_PATH = config.steps.dotanalysis_landing_dir
if not DOTANALYSIS_PATH.exists():
    raise RuntimeError(f"DOTANALYSIS_LANDING_DIR directory {DOTANALYSIS_PATH} does not exist")


class DotanalysisFileWatcher(FileWatcher):
    """Watch for .analysis metadata files and create DotAnalysisFileArtefacts."""

    path = DOTANALYSIS_PATH
    pattern = r".+\.analysis"

    @override
    async def process(self, input_artefact: FileArtefactModel) -> list[DotAnalysisFileArtefact]:
        """Validates the .analysis file and creates DotAnalysisFileArtefact."""

        metafile_path = input_artefact.path
        with metafile_path.open() as f:
            contents = f.read()
            contents_dict = json.loads(contents)
            pipeline_order = PipelineOrder.validate_python(contents_dict)

        logger.debug(f"Validated .analysis file: {metafile_path}")

        dotanalysis = DotAnalysisFileArtefact(path=metafile_path)
        logger.info(
            f"Creating DotAnalysisFileArtefact for workflow {pipeline_order.params.workflow_id} from {metafile_path}"
        )

        await self.order_workflow(pipeline_order)

        return [dotanalysis]

    @staticmethod
    async def order_workflow(pipeline_order: PipelineOrderType) -> None:
        workflow_step = WORKFLOW_STEPS.get(pipeline_order.pipeline_type)
        if workflow_step is None:
            raise RuntimeError(f"Unexpected pipeline type {pipeline_order.pipeline_type}")

        # Create job to run pipeline
        job = Job(
            maestro_id=pipeline_order.params.job_id,
            display_name=f"{workflow_step.__name__}:{strip_uuid(pipeline_order.params.analysis_name)}",
            workflow_step=workflow_step,
            inputs=pipeline_order.inputs.model_dump(exclude_none=True),
            params=pipeline_order.params.model_dump(exclude_none=True),
            priority=pipeline_order.priority,
        )

        workflow = WorkflowModel(
            maestro_id=pipeline_order.params.workflow_id,
            workflow_name=pipeline_order.params.workflow_name,
            jobs=[job],
        )

        await order_workflow_with_job_filtering(workflow)

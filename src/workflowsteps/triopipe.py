"""Maestro MessageWatcher for running vcpipe's triopipe.nf Nextflow pipeline"""

import json
import logging
import uuid
from collections.abc import Sequence
from pathlib import Path
from typing import Any, override

from artefacts.base import StepsBaseFileArtefact
from artefacts.seqdata import DotSampleFile
from artefacts.vcpipe import (
    GenomicVariantFile,
    MTVariantFile,
    MTVariantIndexFile,
    MultiSampleMTVariantFile,
    MultiSampleVariantFile,
    QCReportTrio,
    QCResult,
    QCResultTrio,
    QCWarningsTrio,
)
from maestro import MessageWatcher
from utils import (
    Identifiers,
    get_job_output_dir,
    run_subprocess,
)

from config import config
from workflowsteps.utils import package

logger = logging.getLogger("steps")


class Triopipe(MessageWatcher):
    """Starts vcpipe triopipe.nf

    Expects 3 GenomicVariantFile artefacts from Basepipe, 1 for each trio family members.
    Expects 3 QCResult artefacts from Basepipe, 1 for each trio family members.
    Expects 1 DotSampleFileArtefact artefact from DotSampleWatcher, for the proband sample
    Expects Zero or 2 MTVariantFile artefacts from Basepipe, 1 for proband and 1 for mother.
    Uses analysis_name as sub directory name for work (temporary files) and final output files."""

    input_types = {
        GenomicVariantFile,
        QCResult,
        DotSampleFile,
        MTVariantFile,
        MTVariantIndexFile,
    }

    concurrency_limit = config.vcpipe.triopipe.concurrency_limit

    dir_globs_artefacts = (  # Maps subdirectory and glob pattern with artefact type
        ("variantcalling", "*.all.final.vcf", MultiSampleVariantFile),
        ("qc", "*.qc_result.json", QCResultTrio),
        ("qc", "*.qc_report.md", QCReportTrio),
        ("qc", "*.qc_warnings.md", QCWarningsTrio),
        ("mtdna", "*.all.filter.chrMT.vcf", MultiSampleMTVariantFile),
    )

    @override
    @package(
        job_output_dir=lambda self, *args, **kwargs: self.job_output_dir,
        workflow_maestro_id=lambda self, *args, **kwargs: self.workflow_id,
    )
    async def process(
        self,
        workflow_id: uuid.UUID,
        workflow_name: str,
        job_id: uuid.UUID,
        analysis_name: str,
        specialized_pipeline: str,
        proband_dot_sample: DotSampleFile,
        pedigree_json: str,
        gvcf_1: GenomicVariantFile,
        gvcf_2: GenomicVariantFile,
        gvcf_3: GenomicVariantFile,
        qc_results_1: QCResult,
        qc_results_2: QCResult,
        qc_results_3: QCResult,
        mtvcf_1: MTVariantFile | None = None,
        mtvcf_2: MTVariantFile | None = None,
        mtvcf_3: MTVariantFile | None = None,
        **kwargs: Any,
    ) -> Sequence[StepsBaseFileArtefact]:
        self.workflow_id = workflow_id
        self.workflow_name = workflow_name
        self.job_id = job_id

        gvcfs: dict[str, GenomicVariantFile] = dict()
        mtvcfs: dict[str, MTVariantFile] = dict()
        qc_results: dict[str, QCResult] = dict()

        pedigree = json.loads(pedigree_json)
        for family_member, member_info in pedigree.items():
            seqdata_id = Identifiers.from_string(member_info["sample"]).seqdata_id
            gvcfs[family_member] = next(
                x for x in [gvcf_1, gvcf_2, gvcf_3] if x.seqdata_id == seqdata_id
            )
            qc_results[family_member] = next(
                x for x in [qc_results_1, qc_results_2, qc_results_3] if x.seqdata_id == seqdata_id
            )
            try:
                mtvcfs[family_member] = next(
                    x
                    for x in [mtvcf_1, mtvcf_2, mtvcf_3]
                    if x is not None and x.seqdata_id == seqdata_id
                )
            except StopIteration:
                pass

        assert len(gvcfs) == 3, f"Expected 3 GenomicVariantFile files, found {len(gvcfs)}"
        assert len(qc_results) == 3, f"Expected 3 QCResult files, found {len(qc_results)}"

        # get capturekit and proband given_sex from proband dot sample file
        capturekit = proband_dot_sample.capturekit
        proband_given_sex = proband_dot_sample.given_sex
        owner_id = proband_dot_sample.owner_id
        batch_id = proband_dot_sample.batch_id
        dna_id = proband_dot_sample.dna_id
        self.seqdata_id = proband_dot_sample.seqdata_id

        self.job_output_dir: Path = get_job_output_dir(
            prefixes=[owner_id, batch_id, dna_id],
            workflow_id=self.workflow_id,
            job_id=self.job_id,
            step_name=self.__class__.__name__,
        )
        self.job_output_dir.mkdir(mode=0o755, parents=True, exist_ok=True)

        nf_params = [
            f"--analysis_name {analysis_name}",
            f"--capture_kit {capturekit}",
            f"--specialized_pipeline {specialized_pipeline}",
            f"--gender_proband {proband_given_sex}",
            f"--output {self.job_output_dir / 'results'} ",
        ]

        for family_member, member_info in pedigree.items():
            nf_params.append(f"--sample_id_{family_member} {member_info['sample']}")
            nf_params.append(f"--gvcf_{family_member} {gvcfs[family_member].path}")
            nf_params.append(f"--qc_json_{family_member} {qc_results[family_member].path}")
            if family_member in ["proband", "mother"]:
                nf_params.append(f"--mt_vcf_{family_member} {mtvcfs[family_member].path}")

        if config.vcpipe.stub_run:
            nf_params.append("-stub-run")

        cmd = (
            f"nextflow -C {config.vcpipe.nextflow_config} run -profile standard,{config.vcpipe.system} "
            f"-work-dir {self.job_output_dir / 'work'} "
            f"{config.vcpipe.vcpipe_dir}/src/pipeline/triopipe.nf "
            f"--test_settings {config.vcpipe.test_settings} -resume {' '.join(nf_params)}"
        )

        run_subprocess(cmd, cwd=self.job_output_dir)
        return self.triopipe_artefacts()

    def triopipe_artefacts(self) -> Sequence[StepsBaseFileArtefact]:
        """Create artefacts from triopipe output

        Only files that are used downstream as input are turned into artefacts."""

        artefacts = []
        for subdir, file_glob, artefact_type in Triopipe.dir_globs_artefacts:
            glob_list = list(Path(self.job_output_dir / "results/data" / subdir).glob(file_glob))
            assert (
                len(glob_list) == 1
            ), f"Expected 1 file matching '{file_glob}', found {len(glob_list)}"

            artefacts.append(
                artefact_type(
                    path=glob_list[0].absolute(),
                    workflow_id=self.workflow_id,
                    seqdata_id=self.seqdata_id,
                )
            )

        return artefacts

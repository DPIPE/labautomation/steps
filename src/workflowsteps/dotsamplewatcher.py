"""File watcher for dotsample sequencing metadata files."""

import logging
from pathlib import Path
from typing import override
from uuid import UUID

from artefacts.seqdata import DotSampleFile, PreFASTQFile
from maestro import FileArtefactModel, FileWatcher, Job, WorkflowModel
from maestro.workflow_utils import order_workflow_with_job_filtering
from utils import seqdata_maker_workflow_id_and_name

from config import config
from workflowsteps.fastqwatcher import SeqDataMaker

logger = logging.getLogger("steps")

DOTSAMPLE_PATH = config.steps.dotsample_landing_dir
if not DOTSAMPLE_PATH.exists():
    raise RuntimeError(f"DOTSAMPLE_LANDING_DIR directory {DOTSAMPLE_PATH} does not exist")


class DotsampleFileWatcher(FileWatcher):
    """Watch for .sample metadata files, create DotSampleFileArtefacts and SeqDataMaker job."""

    path = DOTSAMPLE_PATH
    pattern = r".*\.sample"

    @override
    @classmethod
    def workflow_id_and_name(cls, path: Path) -> tuple[UUID, str]:
        """Extracts the seqdata_id from the FASTQ file path."""
        return seqdata_maker_workflow_id_and_name(path)

    @override
    async def process(self, input: FileArtefactModel) -> list[DotSampleFile]:
        """Validates the .sample file and creates DotSampleFile."""
        metafile_path = input.path
        with open(metafile_path) as f:
            contents = f.read()
            dotsample = DotSampleFile.model_validate_json(contents)
        logger.debug(f"Validated .sample file: {metafile_path}")

        logger.info(
            f"Creating DotSampleFile for {dotsample.batch_id} {dotsample.dna_id} {dotsample.seqdata_id} ({dotsample.nickname}) from {metafile_path}"
        )

        # order SeqDataMaker job
        seqdata_id = str(dotsample.seqdata_id)
        job = Job(
            workflow_step=SeqDataMaker,
            display_name=f"SeqDataMaker:{dotsample.dna_id}",
            inputs={
                "pre_fastq1": (
                    PreFASTQFile,
                    {
                        "seqdata_id": seqdata_id,
                        "read_file_number": 1,
                    },
                ),
                "pre_fastq2": (
                    PreFASTQFile,
                    {
                        "seqdata_id": seqdata_id,
                        "read_file_number": 2,
                    },
                ),
                "dotsample": (
                    DotSampleFile,
                    {
                        "seqdata_id": seqdata_id,
                    },
                ),
            },
            priority=dotsample.priority,
        )

        maestro_id, workflow_name = self.workflow_id_and_name(metafile_path)
        workflow = WorkflowModel(
            maestro_id=maestro_id,
            workflow_name=workflow_name,
            jobs=[job],
        )

        await order_workflow_with_job_filtering(workflow)

        return [dotsample]

from pathlib import Path
from typing import override

import requests
from artefacts.base import StepsBaseFileArtefact
from artefacts.package import PackageArtefact
from maestro import MessageWatcher
from maestro.neo4j.invalidate import invalidate_artefacts
from maestro.neo4j.neo4j import Neo4jTransaction
from utils import run_subprocess

from config import config


class PackageTransferrer(MessageWatcher):
    """Handles transfer of a staged PackageArtefact from NSC to TSD.

    Transfers package artefact files to the TSD filelock."""

    concurrency_limit = config.package.transfer.concurrency_limit

    def get_short_lived_token(self) -> str:
        """Retrieve a short-lived access token for TSD S3 API."""
        endpoint = config.package.transfer.api_endpoint
        headers = {"Authorization": "Bearer " + config.package.transfer.api_key}
        params = {"type": config.package.transfer.token_type}

        response = requests.post(endpoint, headers=headers, params=params)
        response.raise_for_status()

        try:
            token: str = response.json()["token"]
        except KeyError as e:
            raise RuntimeError("Failed to retrieve short-lived token") from e

        return token

    def build_s3_command(
        self,
        source_dir: Path,
    ) -> list[str]:
        # Build command for s3 transfer
        token = self.get_short_lived_token()

        base_command = [
            "tsd-s3cmd",
            f"--add-header=token:{token}",
            f"--add-header=x-project-select:{config.package.transfer.project}",
            f"--multipart-chunk-size-mb={config.package.transfer.chunk_size}",
            "--no-delete-removed",
            "--no-preserve",
            "--stop-on-error",
        ]

        transfer_command = [
            "put",
            "--recursive",
            source_dir.as_posix(),
            f"s3://{config.package.transfer.bucket}",
        ]

        command = base_command + transfer_command

        return command

    @override
    async def process(self, package: PackageArtefact) -> list[PackageArtefact]:
        # Prepare and execute transfer command
        command = self.build_s3_command(package.path)

        run_subprocess(" ".join(command))

        # Create transferred package artefacts
        def create_transferred_file_artefact(
            file_artefact: StepsBaseFileArtefact,
        ) -> StepsBaseFileArtefact:
            model_dict = file_artefact.model_dump(exclude={"maestro_id"}, exclude_defaults=True)
            model_dict["path"] = config.package.transfer.filelock / file_artefact.path.name
            model_dict["instance_id"] = config.package.unpacking.filelock_instance

            return type(file_artefact).model_validate(model_dict)

        file_artefacts = {
            create_transferred_file_artefact(file_artefact)
            for file_artefact in package.file_artefacts
        }
        extra_artefacts = {
            create_transferred_file_artefact(file_artefact)
            for file_artefact in package.extra_artefacts
        }

        transferred_package = PackageArtefact(
            instance_id=config.package.unpacking.instance,
            path=config.package.transfer.filelock,
            file_artefacts=file_artefacts,
            extra_artefacts=extra_artefacts,
            file_routings=package.file_routings,
            transfer_id=package.transfer_id,
        )

        # Invalidate original package artefact and delete staged files
        async with Neo4jTransaction() as neo4j_tx:
            await invalidate_artefacts(neo4j_tx, [package.maestro_id])

        for file_artefact in package.file_artefacts | package.extra_artefacts:
            file_artefact.path.unlink()

        package.path.rmdir()

        return [transferred_package]

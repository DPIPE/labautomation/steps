import logging
from collections.abc import Callable
from functools import wraps
from pathlib import Path
from typing import Any
from uuid import UUID, uuid4

from artefacts.base import StepsBaseFileArtefact
from artefacts.package import PackageArtefact
from maestro import (
    BaseArtefactModel,
    FileWatcher,
    Job,
    MessageWatcher,
    WorkflowModel,
)
from maestro.config import Config as MaestroConfig
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import read_node
from maestro.workflow_utils import order_workflow_with_job_filtering

from config import PackageUnpackingConfig, config
from workflowsteps.packagestager import PackageStager
from workflowsteps.packagetransferrer import PackageTransferrer
from workflowsteps.packageunpacker import PackageUnpacker

logger = logging.getLogger("steps")

RoutingType = tuple[str, Path]
FileRouting = PackageUnpackingConfig.FileRouting


def package(
    job_output_dir: Callable | Path,
    workflow_maestro_id: Callable | UUID,
    delayers: list[tuple[type[BaseArtefactModel], dict[str, Any]]] | None = None,
    routings: list[RoutingType] = [],
) -> Callable:
    """Decorator to add a PackageArtefact to a process method’s output.

    Establishes relationships with all output files and file artefacts, and schedules staging for
    NSC-to-TSD transfer.

    Expects 'job_output_dir' to the output directory of the job, where all files, including
    non-artefact files, are written. Also expects 'workflow_maestro_id', such that the transfer jobs
    can be added to the original workflow.

    Optional `delayers` can specify delayer artefacts to delay package transfer until the
    artefacts are delivered.

    Optional `routings` can specify file glob patterns and target directories on TSD,
    which take precedence over the default file routings.
    """

    def decorator(func: Callable) -> Callable:
        @wraps(func)
        async def wrapper(*args: Any, **kwargs: Any) -> list[BaseArtefactModel]:
            # Decorator can only be applied to valid process method
            instance = args[0]
            if not (
                (isinstance(instance, MessageWatcher) or isinstance(instance, FileWatcher))
                and func.__name__ == "process"
            ):
                raise TypeError(
                    "Decorator only applicable to process method of MessageWatcher or FileWatcher."
                )

            # Run decorated process method
            output_artefacts: list[BaseArtefactModel] = await func(*args, **kwargs)

            if (
                not config.package.run_transfer
                or MaestroConfig.INSTANCE_ID != config.package.staging.instance
            ):
                return output_artefacts

            # Check that job_output_dir is set and is existing directory
            if callable(job_output_dir):
                output_dir: Path = job_output_dir(instance, *args, **kwargs)
            else:
                output_dir = job_output_dir

            assert isinstance(output_dir, Path), f"Expected Path, got {type(output_dir)}."

            if not output_dir.is_dir():
                raise AttributeError(f"job_output_dir {output_dir} is not an existing directory.")

            file_artefacts = {
                artefact
                for artefact in output_artefacts
                if isinstance(artefact, StepsBaseFileArtefact)
            }

            # Identify remaining output files and create generic file artefacts
            artefact_paths = {artefact.path for artefact in file_artefacts}
            extra_files = set(output_dir.glob("*")) - artefact_paths
            extra_artefacts = {StepsBaseFileArtefact(path=file) for file in extra_files}

            # Check that all files are in output directory
            for artefact in file_artefacts | extra_artefacts:
                if artefact.path.parent != output_dir:
                    raise ValueError(
                        f"File artefact {artefact.path} is not in output directory {output_dir}."
                    )

            # If any files to transfer, create package artefact and order package transfer steps
            if file_artefacts or extra_artefacts:
                file_routings = [
                    FileRouting(glob=glob, target_dir=target_dir) for glob, target_dir in routings
                ]
                all_file_routings = file_routings + config.package.unpacking.file_routings

                # Check that all files match a file routing
                all_paths = {artefact.path for artefact in file_artefacts | extra_artefacts}
                for file_routing in file_routings + all_file_routings:
                    matching_paths = {path for path in all_paths if path.match(file_routing.glob)}
                    all_paths -= set(matching_paths)

                if all_paths:
                    raise ValueError(f"No file routing matches file artefacts {all_paths}.")

                transfer_id = uuid4()  # unique identifier for one package transfer

                package = PackageArtefact(
                    path=output_dir,
                    file_artefacts=file_artefacts,
                    extra_artefacts=extra_artefacts,
                    file_routings=all_file_routings,
                    transfer_id=transfer_id,
                )

                stager_inputs: dict[str, tuple[type[BaseArtefactModel], dict[str, Any]]] = {
                    "package": (PackageArtefact, {"transfer_id": transfer_id})
                }

                # Add delayer artefacts to delay package transfer until delivered
                if delayers:
                    for i, delayer in enumerate(delayers):
                        for key, value in delayer[1].copy().items():
                            if callable(value):
                                param = value(instance, *args, **kwargs)
                                delayer[1].update({key: param})

                        stager_inputs.update({f"delayer_{i}": delayer})

                transfer_jobs = {
                    Job(  # ordered on this (staging) instance
                        workflow_step=PackageStager,
                        inputs=stager_inputs,
                    ),
                    Job(
                        instance_id=config.package.transfer.instance,
                        workflow_step=PackageTransferrer,
                        inputs={
                            "package": (
                                PackageArtefact,
                                {
                                    "transfer_id": transfer_id,
                                    "instance_id": config.package.transfer.instance,
                                },
                            )
                        },
                    ),
                    Job(
                        instance_id=config.package.unpacking.instance,
                        workflow_step=PackageUnpacker,
                        inputs={
                            "package": (
                                PackageArtefact,
                                {
                                    "transfer_id": transfer_id,
                                    "instance_id": config.package.unpacking.instance,
                                },
                            )
                        },
                    ),
                }

                # Retrieve existing workflow
                if callable(workflow_maestro_id):
                    maestro_id = workflow_maestro_id(instance, *args, **kwargs)
                else:
                    maestro_id = workflow_maestro_id

                async with Neo4jTransaction() as neo4j_tx:
                    workflow = await read_node(neo4j_tx, WorkflowModel, {"maestro_id": maestro_id})

                assert workflow is not None, f"Workflow {maestro_id} not found in Neo4j."

                workflow.jobs = transfer_jobs

                await order_workflow_with_job_filtering(workflow)

                # Add package artefact to output artefacts
                output_artefacts.append(package)
            else:
                raise ValueError(
                    f"{args[0].__class__.__name__} has no output files to stage for transfer."
                )

            return output_artefacts

        return wrapper

    return decorator

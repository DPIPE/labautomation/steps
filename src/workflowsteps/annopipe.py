"""Maestro MessageWatcher for running vcpipe's annopipe.nf Nextflow pipeline"""

import json
import logging
import uuid
from pathlib import Path
from typing import TypeVar, override

from artefacts.pipeline_order import JointVariantCallingType
from artefacts.seqdata import DotSampleFile
from artefacts.vcpipe import (
    AnnopipeOutput,
    CNVBedFile,
    CNVVariantFile,
    ConvadingLogFile,
    ConvadingLonglistFile,
    ConvadingShortlistFile,
    ConvadingTotallistFile,
    CoverageBigWig,
    MultiSampleVariantFile,
    QCReport,
    QCReportTrio,
    QCResult,
    QCResultTrio,
    QCWarnings,
    QCWarningsTrio,
    SeqMappingFile,
    VariantFile,
    VCPipeFileArtefact,
)
from maestro import MessageWatcher
from utils import (
    Identifiers,
    get_job_output_dir,
    run_subprocess,
)

from config import config
from workflowsteps.utils import package

logger = logging.getLogger("steps")


GenericFileArtefact = TypeVar("GenericFileArtefact", bound=VCPipeFileArtefact | DotSampleFile)


def get_latest_gene_panel_version(gene_panel: str, panels: dict[str, dict]) -> str:
    """if gene_panel already has version part, return it as is;
    otherwise, look up the latest version in panels."""

    # gene_panel in format: <panelName>_<panelVersion>
    if "_" in gene_panel:
        return gene_panel

    # only gene panel name is provided
    for k in panels.keys():
        gp_name, gp_version = k.split("_")
        if gp_name == gene_panel and gp_version:
            logger.info("Found latest version for gene panel %s: %s", gene_panel, gp_version)
            return gene_panel + "_" + gp_version

    raise RuntimeError(f"Unknown gene panel {gene_panel}")


class Annopipe(MessageWatcher):
    """
    Starts vcpipe Annopipe.nf

    Expects 1 (single) or 3 (trio) SeqMappingFile artefacts from Basepipe
    Expects 1 QCResult or QCResultTrio artefacts from Basepipe or Triopipe
    Expects 1 QCReport or QCReportTrio artefacts from Basepipe or Triopipe
    Expects 0 or 1 QCWarnings or QCWarningsTrio artefacts from Basepipe or Triopipe
    Expects 1 VariantFile file from basepipe or 1 MultiSampleVariantFile artefact from Triopipe
    Expects 1 or 3 DotSampleFile artefact from DotSampleWatcher
    Uses analysis_name as sub directory name for work (temporary files) and final output files.
    """

    dir_globs_artefacts = [("", "ella.tar", AnnopipeOutput)]

    concurrency_limit = config.vcpipe.annopipe.concurrency_limit

    @staticmethod
    def get_triple_artefact(
        inputs: list[GenericFileArtefact | None],
        pedigree: dict | None,
        person_seqdata_id: uuid.UUID,
    ) -> dict[str, GenericFileArtefact]:
        triple: dict[str, GenericFileArtefact] = dict()
        for input in filter(bool, inputs):
            assert input is not None
            if pedigree is not None:
                for family_member, member_info in pedigree.items():
                    seqdata_id = Identifiers.from_string(member_info["sample"]).seqdata_id
                    if input.seqdata_id == seqdata_id:
                        triple[family_member] = input
            else:
                if input.seqdata_id == person_seqdata_id:
                    triple["proband_or_single"] = input

        return triple

    @override
    @package(
        job_output_dir=lambda self, *args, **kwargs: self.job_output_dir,
        workflow_maestro_id=lambda self, *args, **kwargs: self.workflow_id,
    )
    async def process(
        self,
        workflow_id: uuid.UUID,
        workflow_name: str,
        job_id: uuid.UUID,
        genepanel: str,
        analysis_name: str,
        vcf_file: VariantFile | MultiSampleVariantFile,
        qc_result: QCResult | QCResultTrio,
        qc_report_md: QCReport | QCReportTrio,
        qc_warnings_md: QCWarnings | QCWarningsTrio,
        dot_sample_1: DotSampleFile,
        bam_1: SeqMappingFile,
        bigwig_1: CoverageBigWig,
        cnv_1: CNVVariantFile | CNVBedFile | None = None,
        dot_sample_2: DotSampleFile | None = None,
        bam_2: SeqMappingFile | None = None,
        bigwig_2: CoverageBigWig | None = None,
        cnv_2: CNVVariantFile | CNVBedFile | None = None,
        dot_sample_3: DotSampleFile | None = None,
        bam_3: SeqMappingFile | None = None,
        bigwig_3: CoverageBigWig | None = None,
        cnv_3: CNVVariantFile | CNVBedFile | None = None,
        convading_log: ConvadingLogFile | None = None,
        convading_longlist: ConvadingLonglistFile | None = None,
        convading_shortlist: ConvadingShortlistFile | None = None,
        convading_totallist: ConvadingTotallistFile | None = None,
        pedigree_json: str | None = None,
        analysis_requested_date: str | None = None,
        proband_or_single: str | None = None,
        priority: str | None = None,
        **kwargs: object,
    ) -> list[AnnopipeOutput]:
        self.workflow_id = workflow_id
        self.workflow_name = workflow_name
        self.job_id = job_id
        if pedigree_json is not None:
            pedigree = json.loads(pedigree_json)
            self.joint_variant_calling_type = JointVariantCallingType.TRIO
        else:
            pedigree = None
            self.joint_variant_calling_type = JointVariantCallingType.SINGLE

        assert proband_or_single, "Either a pedigree (trio) or a sample (single) must be provided"
        identifiers = Identifiers.from_string(proband_or_single)
        self.seqdata_id = identifiers.seqdata_id  # seqdata_id of trio proband or single sample
        owner_id = identifiers.owner_id
        batch_id = identifiers.batch_id
        dna_id = identifiers.dna_id

        self.job_output_dir: Path = get_job_output_dir(
            prefixes=[owner_id, batch_id, dna_id],
            workflow_id=self.workflow_id,
            job_id=self.job_id,
            step_name=self.__class__.__name__,
        )
        self.job_output_dir.mkdir(mode=0o755, parents=True, exist_ok=True)

        # trio or single sample
        bams: dict[str, SeqMappingFile] = self.get_triple_artefact(
            [bam_1, bam_2, bam_3], pedigree, self.seqdata_id
        )
        bigwigs: dict[str, CoverageBigWig] = self.get_triple_artefact(
            [bigwig_1, bigwig_2, bigwig_3], pedigree, self.seqdata_id
        )
        cnv_vcfs: dict[str, CNVVariantFile | CNVBedFile] = self.get_triple_artefact(
            [cnv_1, cnv_2, cnv_3], pedigree, self.seqdata_id
        )
        dot_samples: dict[str, DotSampleFile] = self.get_triple_artefact(
            [dot_sample_1, dot_sample_2, dot_sample_3],
            pedigree,
            self.seqdata_id,
        )

        # proband or single sample

        assert dot_samples, "Expected 1 or 3 DotSampleFile files, found 0"
        assert (
            vcf_file is not None
        ), "Expected 1 VariantFile or MultiSampleVariantFile file, found 0"
        assert qc_result is not None, "Expected 1 QCResult file, found 0"
        assert qc_report_md is not None, "Expected 1 QCReport file, found 0"
        assert qc_warnings_md is not None, "Expected 1 QCWarnings or QCWarningsTrio file, found 0"
        assert (
            pedigree and len(bams) == 3 or not pedigree and len(bams) == 1
        ), f"Found {len(bams)} bams"

        # get capturekit and (proband) given_sex from (proband) dot sample file
        if pedigree is not None:
            dot_sample = dot_samples["proband"]
        else:
            dot_sample = dot_samples["proband_or_single"]

        capturekit = dot_sample.capturekit
        proband_or_single_given_sex = dot_sample.given_sex
        self.output_artefact_identifier = dot_sample.name

        sensitive_db_dir = config.vcpipe.sensitive_db_dir
        sensitive_db_file = sensitive_db_dir / "sensitive-db.json"
        with open(sensitive_db_file) as f:
            sensitive_db = json.load(f)
            exome_cnv_inhouse_db = Path(
                sensitive_db_dir, sensitive_db["cnv"]["exCopyDepth"]["excap"]["database"]
            )
        wgs_cnv_inhouse_db_dir = Path(sensitive_db_dir, "wgs-sv")

        genepanels_home = config.vcpipe.genepanels_dir
        genepanels_file = Path(genepanels_home, "panels.json")
        with open(genepanels_file) as f:
            genepanels = json.load(f)
            genepanel = get_latest_gene_panel_version(genepanel, genepanels)

        transcripts = genepanels[genepanel]["transcripts"]
        phenotypes = genepanels[genepanel]["phenotypes"]
        report_config = genepanels[genepanel]["report_file"]
        exon_regions = genepanels[genepanel]["regions"]

        nf_params = [
            f"--analysis_name {analysis_name}",
            f"--capture_kit {capturekit}",
            f"--gender {proband_or_single_given_sex}",
            f"--output {self.job_output_dir / 'results'} ",
            f"--exome_cnv_inhouse_db {exome_cnv_inhouse_db} ",
            f"--wgs_cnv_inhouse_db_dir {wgs_cnv_inhouse_db_dir} ",
            f"--qc_result {qc_result.path}",
            f"--qc_report_md {qc_report_md.path}",
            f"--qc_warnings_md {qc_warnings_md.path}",
            f"--vcf_file {vcf_file.path}",
            f"--transcripts {transcripts}",
            f"--phenotypes {phenotypes}",
            f"--report_config {report_config}",
            f"--exon_regions {exon_regions}",
            f"--analysis_requested_date {analysis_requested_date}",
            f"--type {self.joint_variant_calling_type.value}",
            f"--priority {priority}",
        ]

        # INFO: cnvs (for single or trio-proband)
        # params.cnv for *.cnv.bed - for excap(exCopyDepth)
        # params.cnv_vcf for *.cnv.vcf (non-wgs, not used) or merged_*cnv.vcf (wgs)
        if pedigree is not None:
            for family_member, member_info in pedigree.items():
                if family_member == "proband":
                    nf_params.append(f"--merged_cnv_vcf {cnv_vcfs[family_member].path}")
                    nf_params.append(f"--bigwig {bigwigs[family_member].path}")
                    # --sample_id for proband
                    nf_params.append(f"--sample_id {member_info['sample']}")
                    # --bam for proband
                    nf_params.append(f"--bam {bams[family_member].path}")
                else:
                    # --father-bigwig, --mother-bigwig
                    nf_params.append(f"--{family_member}_bigwig {bigwigs[family_member].path}")
                    # --father-sample-id, --mother-sample-id
                    nf_params.append(f"--{family_member}_sample_id {member_info['sample']}")
                    # --father-bam, --mother-bam
                    nf_params.append(f"--{family_member}_bam {member_info['sample']}")
        else:
            if isinstance(cnv_vcfs["proband_or_single"], CNVBedFile):
                nf_params.append(f"--cnv {cnv_vcfs['proband_or_single'].path}")
            else:
                nf_params.append(f"--merged_cnv_vcf {cnv_vcfs['proband_or_single'].path}")
            nf_params.append(f"--bam {bams['proband_or_single'].path}")

        # convading files
        for convading_file in [
            convading_log,
            convading_longlist,
            convading_shortlist,
            convading_totallist,
        ]:
            if convading_file is not None:
                nf_params.append(f"--{convading_file} {convading_file.path}")

        if config.vcpipe.stub_run:
            nf_params.append("-stub-run")

        cmd = (
            f"nextflow -C {config.vcpipe.nextflow_config} run -profile standard,{config.vcpipe.system} "
            f"-work-dir {self.job_output_dir / 'work'} "
            f"{config.vcpipe.vcpipe_dir}/src/pipeline/annopipe.nf "
            f"--test_settings {config.vcpipe.test_settings} -resume {' '.join(nf_params)}"
        )

        run_subprocess(cmd, cwd=self.job_output_dir)
        return self.annopipe_artefacts()

    def annopipe_artefacts(self) -> list[AnnopipeOutput]:
        """Create artefacts from annopipe output, which is the folder containing input files for ella"""
        artefacts = []
        for subdir, file_glob, artefact_type in Annopipe.dir_globs_artefacts:
            glob_list = list(Path(self.job_output_dir / "results" / subdir).glob(file_glob))
            assert len(glob_list) == 1, f"Expected 1 file, found {len(glob_list)}"
            artefacts.append(
                artefact_type(
                    path=glob_list[0].absolute(),
                    workflow_id=self.workflow_id,
                    seqdata_id=self.seqdata_id,
                )
            )

        return artefacts

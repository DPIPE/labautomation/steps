"""File watcher for FASTQ files and MessageWatcher for creating SeqData and FASTQFile artefacts."""

import logging
import re
from pathlib import Path
from typing import override
from uuid import UUID

from artefacts.seqdata import DotSampleFile, FASTQFile, PreFASTQFile, SeqData
from maestro import FileArtefactModel, FileWatcher, MessageWatcher
from utils import (
    Identifiers,
    seqdata_maker_workflow_id_and_name,
)

from config import config

logger = logging.getLogger("steps")

FASTQS_PATH = config.steps.fastq_landing_dir
if not FASTQS_PATH.exists():
    raise RuntimeError(f"FASTQ directory {FASTQS_PATH} does not exist")


class FASTQFileWatcher(FileWatcher):
    """Watches for FASTQ files and creates PreFASTQFile artefacts.

    The path to watch is the root directory, which will contain subdirs with FASTQ files.
    Assumes each directory is named like: OUSAMG_wgs123_HG123456_2f08b36a-b3e3-406b-9c6d-e8e47d20d922.
    """

    path = FASTQS_PATH
    pattern = r".*\.fastq\.gz"

    @override
    @classmethod
    def workflow_id_and_name(cls, path: Path) -> tuple[UUID, str]:
        """Extracts the seqdata_id from the FASTQ file path."""
        return seqdata_maker_workflow_id_and_name(path)

    @override
    async def process(self, input: FileArtefactModel) -> list[PreFASTQFile]:
        fastq_path = input.path
        identifiers = Identifiers.from_path(fastq_path)
        read_file_number = FASTQFileWatcher.get_read_file_number_from_path(fastq_path)

        fq = PreFASTQFile(
            path=fastq_path,
            seqdata_id=identifiers.seqdata_id,
            read_file_number=read_file_number,
        )
        logger.info(f"Creating PreFASTQFile for seqdata_id {fq.seqdata_id} from {fastq_path}")
        return [fq]

    @staticmethod
    def get_read_file_number_from_path(path: Path) -> int:
        """Parse read file number from FASTQ file path.

        The read number can be found in several locations towards the end of the file name."""
        match = re.search(r"R(\d+)", str(path).split("-")[-1])
        if match:
            return int(match.group(1))
        else:
            raise ValueError(f"Could not parse read file number from {path}")


class SeqDataMaker(MessageWatcher):
    """Makes SeqData from DotSampleFileArtefact, and FASTQFile artefacts from PreFASTQFiles."""

    @override
    async def process(
        self,
        pre_fastq1: PreFASTQFile,
        pre_fastq2: PreFASTQFile,
        dotsample: DotSampleFile,
    ) -> list[SeqData]:
        pre_fastqs = [pre_fastq1, pre_fastq2]

        # Create FASTQFile artefacts from PreFASTQFiles
        # Find the corresponding read entry in dotsample and take md5 and size from there
        fastq_artefacts = []
        for pre_fastq in pre_fastqs:
            for i, read_entry in enumerate(dotsample.reads):
                if read_entry.path == pre_fastq.path.name:
                    break
            else:
                raise ValueError(f"Could not find FASTQ name {pre_fastq.path} in SeqData")
            fq = FASTQFile(
                md5_at_creation=dotsample.reads[i].md5,
                size_at_creation=dotsample.reads[i].size,
                **pre_fastq.model_dump(include={"path", "seqdata_id", "read_file_number"}),
            )
            logger.info(f"Creating FASTQFile for seqdata_id {fq.seqdata_id} from {pre_fastq.path}")
            fastq_artefacts.append(fq)

        # Create SeqData from dotsample and add FASTQFile artefacts
        dotsample_dict = dict(dotsample)
        dotsample_dict.pop("reads")  # Remove as Neo4j doesn't tolerate nested objects
        dotsample_dict.pop("stats")
        dotsample_dict.pop("maestro_id")

        sd = SeqData(
            fastqs=fastq_artefacts,
            **dotsample_dict,
        )
        logger.info(
            f"Creating SeqData for {sd.batch_id} {sd.dna_id} {sd.seqdata_id} ({sd.nickname})"
        )

        return [sd]

from typing import Any, override

from artefacts.base import StepsBaseFileArtefact
from artefacts.package import PackageArtefact
from maestro import MessageWatcher

from config import config


class PackageStager(MessageWatcher):
    """Handles staging of a PackageArtefact for NSC-to-TSD transfer.

    Hardlinks package artefact files to the staging directory accessible to the
    NSC transfer instance.

    Can be configured to delay transfer until specified delayer artefacts are delivered.

    Usage:

    stager_job = Job(
        workflow_step=PackageStager,
        inputs={
            "package": (PackageArtefact, {"transfer_id": transfer_id}),
            "delayer_0": (DelayerArtefact, {"param": "value_1"}),
            "delayer_1": (DelayerArtefact, {"param": "value_2"}),
            ...
        },
    )
    """

    concurrency_limit = config.package.staging.concurrency_limit

    @override
    async def process(self, package: PackageArtefact, **kwargs: Any) -> list[PackageArtefact]:
        # Sanity check of delayers
        for key in kwargs:
            assert key.startswith("delayer_"), f"Unexpected delayer key: {key}"

        target_dir = config.package.staging.transfer_dir / package.path.name
        target_dir.mkdir()

        # Create hard links in package staging directory
        for file_artefact in package.file_artefacts | package.extra_artefacts:
            staged_path = target_dir / file_artefact.path.name
            staged_path.hardlink_to(file_artefact.path)

        # Create staged artefacts for transfer
        transfer_instance = config.package.transfer.instance
        transfer_instance_dir = config.package.transfer.transfer_dir / package.path.name

        def create_staged_file_artefact(
            file_artefact: StepsBaseFileArtefact,
        ) -> StepsBaseFileArtefact:
            model_dict = file_artefact.model_dump(exclude={"maestro_id"}, exclude_defaults=True)
            model_dict["path"] = transfer_instance_dir / file_artefact.path.name
            model_dict["instance_id"] = transfer_instance

            return type(file_artefact).model_validate(model_dict)

        file_artefacts = {
            create_staged_file_artefact(file_artefact) for file_artefact in package.file_artefacts
        }
        extra_artefacts = {
            create_staged_file_artefact(file_artefact) for file_artefact in package.extra_artefacts
        }

        staged_package = PackageArtefact(
            instance_id=config.package.transfer.instance,
            path=transfer_instance_dir,
            file_artefacts=file_artefacts,
            extra_artefacts=extra_artefacts,
            file_routings=package.file_routings,
            transfer_id=package.transfer_id,
        )

        return [staged_package]

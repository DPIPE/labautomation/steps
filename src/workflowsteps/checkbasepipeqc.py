import json
from typing import Any, override

from artefacts.qc import BasepipeQC, BasepipeQCForceDelivered, BasepipeQCPass
from artefacts.vcpipe import QCResult
from maestro import MessageWatcher

from config import config


class CheckBasepipeQC(MessageWatcher):
    concurrency_limit = config.vcpipe.qc.concurrency_limit

    def verify_qc_result(self, qc_result: QCResult) -> bool:
        """Verify that all basepipe QC checks have passed."""
        with qc_result.path.open() as f:
            qc_json: dict[str, Any] = json.load(f)
            for value in qc_json.values():
                if not value[0]:
                    return False

        return True

    @override
    async def process(self, qc_result: QCResult) -> list[BasepipeQC]:
        params = {
            "seqdata_id": qc_result.seqdata_id,
            "workflow_id": qc_result.workflow_id,
        }
        output_artefact: BasepipeQC

        if self.verify_qc_result(qc_result):
            output_artefact = BasepipeQCPass.model_validate(params)
        else:
            # Halt workflow and wait for manual inspection of QC results
            await self.halt()

            output_artefact = BasepipeQCForceDelivered.model_validate(params)

        return [output_artefact]

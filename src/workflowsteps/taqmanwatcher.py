"""File watcher for .taqman files; create TaqmanFile artefacts."""

import logging
from pathlib import Path
from typing import override
from uuid import UUID

from artefacts.taqman import TaqmanFile
from maestro import FileArtefactModel, FileWatcher
from utils import (
    Identifiers,
    seqdata_maker_workflow_id_and_name,
)

from config import config

logger = logging.getLogger("steps")

# INFO: .taqman files are in the same folders as the .sample files
TAQMAN_PATH = config.steps.taqman_dir
if not TAQMAN_PATH.exists():
    raise RuntimeError(f"TAQMAN_DIR directory {TAQMAN_PATH} does not exist")


class TaqmanFileWatcher(FileWatcher):
    """Watches for .taqman files and creates TaqmanFile artefacts.

    The path to watch is the root directory, which will contain subdirs with .taqman files.
    Assumes each directory is named like: OUSAMG_wgs123_HG123456_2f08b36a-b3e3-406b-9c6d-e8e47d20d922.
    """

    path = TAQMAN_PATH
    pattern = r".*\.taqman"

    @override
    @classmethod
    def workflow_id_and_name(cls, path: Path) -> tuple[UUID, str]:
        """Extracts the seqdata_id from the FASTQ file path."""
        return seqdata_maker_workflow_id_and_name(path)

    @override
    async def process(self, input: FileArtefactModel) -> list[TaqmanFile]:
        taqman_path = input.path
        identifiers = Identifiers.from_path(taqman_path)

        tqm = TaqmanFile(
            path=taqman_path,
            dna_id=identifiers.dna_id,
        )
        logger.info(f"Creating TaqmanFile for dna_id {tqm.dna_id} from {taqman_path}")
        return [tqm]

from typing import override

from artefacts.base import StepsBaseFileArtefact
from artefacts.package import PackageArtefact
from maestro import MessageWatcher
from maestro.config import Config as MaestroConfig
from maestro.neo4j.invalidate import invalidate_artefacts
from maestro.neo4j.neo4j import Neo4jTransaction
from utils import run_subprocess

from config import config


class PackageUnpacker(MessageWatcher):
    """Unpacks a PackageArtefact, routing files to configured directories on TSD."""

    concurrency_limit = config.package.unpacking.concurrency_limit

    @override
    async def process(self, package: PackageArtefact) -> list[StepsBaseFileArtefact]:
        output_artefacts: list[StepsBaseFileArtefact] = []

        # @package file routings take precedence over default routings
        if package.file_routings:
            file_routings = package.file_routings + config.package.unpacking.file_routings
        else:
            file_routings = config.package.unpacking.file_routings

        all_file_artefacts = package.file_artefacts | package.extra_artefacts

        # Route files based on first matching glob pattern
        #
        # Example:
        #
        #    file_routings = [
        #        FileRouting(glob="*.bam*", target_dir=Path("/path/to/bam")),
        #        FileRouting(glob="*.vcf*", target_dir=Path("/path/to/vcf")),
        #        FileRouting(glob="*", target_dir=Path("/path/to/other")),
        #    ]
        #
        #    file_artefacts = {
        #        FileArtefact(path=Path("/results/file.bam")),
        #        FileArtefact(path=Path("/results/file.bam.genozip")),
        #        FileArtefact(path=Path("/results/file.vcf"))
        #    }
        #
        #    extra_artefacts = {FileArtefact(path=Path("/results/extra.json"))}
        #
        # Result:
        #    mv /results/file.bam /results/file.bam.genozip /path/to/bam
        #    mv /results/file.vcf /path/to/vcf
        #    mv /results/extra.json /path/to/other

        for file_routing in file_routings:
            matching_file_artefacts = {
                file_artefact
                for file_artefact in all_file_artefacts
                if file_artefact.path.match(file_routing.glob)
            }

            # Route matching files to target directory
            if matching_file_artefacts:
                command = [
                    "mv",
                    *map(
                        lambda file_artefact: file_artefact.path.as_posix(), matching_file_artefacts
                    ),
                    file_routing.target_dir.as_posix(),
                ]

                run_subprocess(" ".join(command))

            all_file_artefacts -= matching_file_artefacts

            # Create new artefacts for routed files
            for file_artefact in matching_file_artefacts:
                new_path = file_routing.target_dir / file_artefact.path.name
                assert new_path.exists(), f"Failed to route {file_artefact.path} to {new_path}."

                model_dict = file_artefact.model_dump(exclude={"maestro_id"}, exclude_defaults=True)
                model_dict["path"] = new_path
                model_dict["instance_id"] = MaestroConfig.INSTANCE_ID
                output_artefacts.append(type(file_artefact).model_validate(model_dict))

        # Invalidate unpacked package artefact
        async with Neo4jTransaction() as neo4j_tx:
            await invalidate_artefacts(neo4j_tx, [package.maestro_id])

        # Ensure all file artefacts have been routed
        for file_artefact in package.file_artefacts | package.extra_artefacts:
            assert not file_artefact.path.exists(), f"Failed to route {file_artefact.path}."

        return output_artefacts

"""Maestro MessageWatcher for running vcpipe's basepipe.nf Nextflow pipeline"""

import logging
import uuid
from collections.abc import Sequence
from pathlib import Path
from typing import override

from artefacts.base import StepsBaseFileArtefact
from artefacts.pipeline_order import SpecializedPipeline
from artefacts.qc import BasepipeQC
from artefacts.seqdata import SeqData
from artefacts.taqman import TaqmanFile
from artefacts.vcpipe import (
    CNVBedFile,
    CNVVariantFile,
    ConvadingLogFile,
    ConvadingLonglistFile,
    ConvadingShortlistFile,
    ConvadingTotallistFile,
    CoverageBigWig,
    GenomicVariantFile,
    GenomicVariantIndexFile,
    MTVariantFile,
    MTVariantIndexFile,
    QCReport,
    QCResult,
    QCWarnings,
    SeqMappingFile,
    SeqMappingIndexFile,
    VariantFile,
    VCPipeFileArtefact,
)
from maestro import BaseArtefactModel, MessageWatcher
from utils import get_job_output_dir, run_subprocess

from config import config
from workflowsteps.utils import package

logger = logging.getLogger("steps")


class Basepipe(MessageWatcher):
    """
    Starts vcpipe basepipe.nf

    Expects SeqData artefacts and optionally one TaqmanFile artefact as input
    Uses analysis_name as sub directory name for work (temporary files) and final output files.
    """

    concurrency_limit = config.vcpipe.basepipe.concurrency_limit

    @property
    def dir_globs_artefacts(self) -> Sequence[tuple[str, str, type[VCPipeFileArtefact]]]:
        """Maps subdirectory and glob pattern with artefact type"""
        if not hasattr(self, "seqdata"):
            raise AttributeError("seqdata is not initialized before accessing dir_globs_artefacts")

        common = [
            ("variantcalling", "*.final.vcf", VariantFile),
            ("variantcalling", "*.g.vcf.gz", GenomicVariantFile),
            ("variantcalling", "*.g.vcf.gz.tbi", GenomicVariantIndexFile),
            ("qc", "*.qc_result.json", QCResult),
            ("qc", "*.qc_report.md", QCReport),
            ("qc", "*.qc_warnings.md", QCWarnings),
            ("cnv", "*.bigWig", CoverageBigWig),
            ("mapping", "*.bam", SeqMappingFile),
            ("mapping", "*.bai", SeqMappingIndexFile),
            # ("mapping", "*.cram", SeqMappingFile),
            # ("mapping", "*.cram.crai", SeqMappingIndexFile),
        ]
        if self.seqdata.capturekit == "wgs":
            return common + [
                ("cnv", "merged*.vcf", CNVVariantFile),
                ("mtdna", "*chrMT.final.vcf.gz", MTVariantFile),
                ("mtdna", "*chrMT.final.vcf.gz.tbi", MTVariantIndexFile),
            ]
        elif self.seqdata.capturekit == "CuCaV3":
            return common + [
                ("cnv", "*.cnv.vcf", CNVVariantFile),
                ("cnv", "*.aligned.only.best.score.log", ConvadingLogFile),
                ("cnv", "*.aligned.only.best.score.longlist.txt", ConvadingLonglistFile),
                ("cnv", "*.aligned.only.best.score.shortlist.txt", ConvadingShortlistFile),
                ("cnv", "*.aligned.only.best.score.totallist.txt", ConvadingTotallistFile),
            ]
        elif self.seqdata.capturekit == "agilent_sureselect_v07":
            return common + [("cnv", "*.cnv.bed", CNVBedFile)]
        else:
            raise ValueError(f"Unknown capturekit: {self.seqdata.capturekit}")

    @override
    @package(
        job_output_dir=lambda self, *args, **kwargs: self.job_output_dir,
        workflow_maestro_id=lambda self, *args, **kwargs: self.workflow_id,
        delayers=[
            (
                BasepipeQC,
                {
                    "seqdata_id": lambda self, *args, **kwargs: self.seqdata.seqdata_id,
                    "workflow_id": lambda self, *args, **kwargs: self.workflow_id,
                },
            )
        ],
    )
    async def process(
        self,
        seqdata: SeqData,
        workflow_id: uuid.UUID,
        workflow_name: str,
        job_id: uuid.UUID,
        analysis_name: str,
        specialized_pipeline: SpecializedPipeline,
        taqman: TaqmanFile | None = None,
        **kwargs: object,
    ) -> Sequence[BaseArtefactModel]:
        self.workflow_id = workflow_id
        self.workflow_name = workflow_name
        self.job_id = job_id
        self.seqdata = seqdata
        assert (
            len(self.seqdata.fastqs) == 2
        ), f"Expected 2 FASTQ files, found {len(self.seqdata.fastqs)}"
        capturekit = self.seqdata.capturekit

        # Create job output directory
        self.job_output_dir: Path = get_job_output_dir(
            prefixes=[self.seqdata.owner_id, self.seqdata.batch_id, self.seqdata.dna_id],
            workflow_id=self.workflow_id,
            job_id=self.job_id,
            step_name=self.__class__.__name__,
        )
        self.job_output_dir.mkdir(mode=0o755, parents=True, exist_ok=True)

        nf_params = [
            f"--reads1 {self.seqdata.fastqs[0].path}",
            f"--reads2 {self.seqdata.fastqs[1].path}",
            f"--sample_id {self.seqdata.seqdata_id}",
            f"--capture_kit {capturekit}",
            f"--specialized_pipeline {specialized_pipeline}",
            f"--analysis_name {analysis_name}",
            f"--output {self.job_output_dir / 'results'}",
        ]

        if taqman is not None:
            nf_params.append(f"--taqman {taqman.path}")

        if config.vcpipe.stub_run:
            nf_params.append("-stub-run")

        cmd = (
            f"nextflow -C {config.vcpipe.nextflow_config} run -profile standard,{config.vcpipe.system} "
            f"-work-dir {self.job_output_dir / 'work'} "
            f"{config.vcpipe.vcpipe_dir}/src/pipeline/basepipe.nf "
            f"--test_settings {config.vcpipe.test_settings} -resume {' '.join(nf_params)}"
        )
        run_subprocess(cmd, cwd=self.job_output_dir)
        return self.basepipe_artefacts()

    def basepipe_artefacts(self) -> Sequence[StepsBaseFileArtefact]:
        """Create artefacts from basepipe output

        Only files that are used downstream as input are turned into artefacts."""
        if not hasattr(self, "seqdata") or not hasattr(self, "workflow_id"):
            raise AttributeError(
                "seqdata or workflow_id is not initialized before accessing basepipe_artefacts"
            )

        artefacts = []
        for subdir, file_glob, artefact_type in self.dir_globs_artefacts:
            glob_list = list(
                Path(self.job_output_dir / "results" / "data" / subdir).glob(file_glob)
            )
            if len(glob_list) == 1:
                artefacts.append(
                    artefact_type(
                        path=glob_list[0].absolute(),
                        seqdata_id=self.seqdata.seqdata_id,
                        workflow_id=self.workflow_id,
                    )
                )
            elif len(glob_list) == 0:
                logger.error(f"No files matched for pattern {file_glob} in {subdir}")
                raise FileNotFoundError(f"No {file_glob} file found in {subdir}")
            elif len(glob_list) > 1:
                logger.error(
                    f"Multiple files matched for pattern {file_glob} in {subdir}: {glob_list}"
                )
                raise ValueError(f"Multiple {file_glob} files found in {subdir}: {glob_list}")

        return artefacts

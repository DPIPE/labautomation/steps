FROM python:3.12-slim-bullseye AS python
ARG USERNAME=steps
ARG UID=1000
ARG GID=1000
ENV DEBIAN_FRONTEND=noninteractive
ENV LC_ALL=C.UTF-8 LANG=C.UTF-8
# Do not write .pyc files
ENV PYTHONDONTWRITEBYTECODE=1
# Do not ever buffer console output
ENV PYTHONUNBUFFERED=1

RUN \
    apt-get update && \
    apt-get install --no-install-recommends -y gettext git curl libfuse3-3 uidmap squashfs-tools fakeroot procps bc rsync && \
    rm -r /var/cache/apt /var/lib/apt
Run groupadd -g $GID $USERNAME \
    && useradd -m -u $UID -g $GID -s /bin/bash $USERNAME

# COPY jdk-21_linux-x64_bin.deb ./
# RUN dpkg -i jdk-21_linux-x64_bin.deb
RUN curl https://download.oracle.com/java/21/latest/jdk-21_linux-x64_bin.deb -o jdk-21_linux-x64_bin.deb \
    && dpkg -i jdk-21_linux-x64_bin.deb \
    && rm jdk-21_linux-x64_bin.deb

# Install nextflow
ENV NXF_HOME=/.nextflow
ENV NXF_VER=24.04.4
RUN curl -s https://get.nextflow.io | bash && \
    mv nextflow /usr/local/bin/nextflow && \
    chmod a+rx /usr/local/bin/nextflow && \
    chmod -R a+rwX /.nextflow

# Install apptainer (depends on libfuse3-3, uidmap, squashfs-tools, fakeroot)
RUN curl -L https://github.com/apptainer/apptainer/releases/download/v1.3.4/apptainer_1.3.4_amd64.deb -o apptainer_1.3.4_amd64.deb && \
    apt install -y ./apptainer_1.3.4_amd64.deb && \
    rm -rf ./apptainer_1.3.4_amd64.deb && \
    curl -L https://github.com/apptainer/apptainer/releases/download/v1.3.4/apptainer-suid_1.3.4_amd64.deb -o apptainer-suid_1.3.4_amd64.deb && \
    apt install -y ./apptainer-suid_1.3.4_amd64.deb && \
    rm -rf ./apptainer-suid_1.3.4_amd64.deb



# Install poetry and dependencies
FROM python AS poetry
WORKDIR /opt/poetry
ENV POETRY_HOME=/opt/poetry
ENV POETRY_VIRTUALENVS_IN_PROJECT=true
ENV PATH="$POETRY_HOME/bin:$PATH"
RUN curl https://install.python-poetry.org | POETRY_VERSION=1.8.5 python -
COPY README.md pyproject.toml poetry.lock ./
# Use cache for poetry
RUN --mount=type=cache,target=/opt/poetry/cache,mode=777 \
    poetry config cache-dir /opt/poetry/cache && \
    poetry config installer.max-workers 10 && \
    # Make sure build doesn't crash if lock file has not been updated (i.e. no new dependencies in pyproject.toml)
    poetry lock --no-update && \
    # Do not install steps project by default
    poetry install --no-interaction --no-ansi --sync -vvv --no-root && \
    chown -R $UID:$GID /opt/poetry && \
    # Run maestro post-install
    poetry run maestro-post-install

# Ensure DVC cache is always mounted into singularity containers run in vcpipe
# This allows symlinking files from the cache
RUN mkdir -m 7777 /cache


# # Install steps project
FROM python AS runtime
USER $USERNAME
COPY --from=poetry /opt/poetry /opt/poetry
ENV POETRY_HOME=/opt/poetry
ENV POETRY_VIRTUALENVS_IN_PROJECT=true
ENV PATH="$POETRY_HOME/bin:${POETRY_HOME}/.venv/bin:$PATH"
ENV VIRTUAL_ENV="${POETRY_HOME}/.venv"
ENV DVC_CACHE_DIR=/cache/.dvc
ENV SINGULARITY_RUN_OPTIONS="--bind /cache"
# Install nextflow runtime
RUN nextflow info

ENTRYPOINT ["maestro-api"]

# For dev env, copy steps project, and set PYTHONPATH
# Do not install project
FROM runtime AS dev
WORKDIR /steps
USER root
COPY . ./
RUN chown -R $UID:$GID /steps && \
    mkdir -p .dvc && \
    chown -R $UID:$GID .dvc && \
    chmod -R 755 .dvc
USER $USERNAME
ENV PYTHONPATH=/steps/src:/steps/ella-sapio/src/api
RUN mkdir -m 7777 -p /tmp/maestro /tmp/maestro/samples /tmp/maestro/analyses-work /tmp/maestro/jobs
ENV SINGULARITY_RUN_OPTIONS="${SINGULARITY_RUN_OPTIONS} --bind /steps/vcpipe-testdata"

# # For prod env, install steps project
FROM runtime AS prod
WORKDIR /steps
COPY . ./
RUN poetry install --no-interaction --no-ansi -vvv --only-root
USER $USERNAME

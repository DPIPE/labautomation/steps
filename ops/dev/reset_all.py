import asyncio

from maestro.nats.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction


async def main() -> None:
    async with NATSSession() as nats:
        streams = await nats._js.streams_info()
        for stream in streams:
            assert stream.config.name is not None
            await nats.delete_stream(stream.config.name)

    async with Neo4jTransaction() as neo4j:
        await neo4j.query("MATCH (n) WHERE NOT n:`__Neo4jMigration` DETACH DELETE n")


if __name__ == "__main__":
    asyncio.run(main())

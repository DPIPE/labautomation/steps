#!/usr/bin/env python3

import asyncio
import sys
import uuid
from pathlib import Path
from typing import override
from unittest.mock import patch

from artefacts import StepsBaseFileArtefact
from maestro import (
    Job,
    MessageWatcher,
    WorkflowModel,
    order_workflow,
    run_workflow_steps,
)
from maestro.config import Config
from maestro.neo4j.neo4j import Neo4jTransaction
from pydantic import UUID4
from workflowsteps.packagestager import PackageStager
from workflowsteps.packagetransferrer import PackageTransferrer
from workflowsteps.packageunpacker import PackageUnpacker
from workflowsteps.utils import package

FILE_CONTENTS = "Hello, world!"


class ArtefactTransferredToSecondary(StepsBaseFileArtefact):
    identifier: UUID4


class PrimaryInstanceMessageWatcher(MessageWatcher):
    @override
    @package(
        job_output_dir=lambda self, *args, **kwargs: self.job_output_dir,
        workflow_maestro_id=lambda self, *args, **kwargs: self.workflow_maestro_id,
        routings=[
            ("*.txt", Path("/target")),
        ],
    )
    async def process(
        self, identifier: UUID4, workflow_maestro_id: UUID4
    ) -> list[StepsBaseFileArtefact]:
        self.workflow_maestro_id = workflow_maestro_id
        self.job_output_dir = Path(f"/transfer/source/{identifier}/")
        self.job_output_dir.mkdir(parents=True, exist_ok=True)
        file = self.job_output_dir / "file.txt"
        file.write_text(FILE_CONTENTS)
        return [ArtefactTransferredToSecondary(identifier=identifier, path=file)]


class SecondaryInstanceMessageWatcher(MessageWatcher):
    @override
    async def process(self, file_artefact: ArtefactTransferredToSecondary) -> list:
        assert file_artefact.path.read_text() == FILE_CONTENTS
        return []


async def init_db() -> int:
    async with Neo4jTransaction():
        pass
    return 0


async def get_job_status_counts() -> dict[str, int]:
    async with Neo4jTransaction() as neo4j_tx:
        return {
            r["n.status"]: r["count(n)"]
            for r in await neo4j_tx.query("MATCH (n:Job) RETURN DISTINCT n.status, count(n)")
        }
    assert False


def mock_build_s3_command(self: PackageTransferrer, source_dir: Path) -> list[str]:
    target_path = "steps-2::filelock"
    return ["rsync", "-av", f"{source_dir.as_posix()}/", target_path]


async def place_workflow_order() -> int:
    prior_status_counts = await get_job_status_counts()
    identifier = uuid.uuid4()
    workflow_maestro_id = uuid.uuid4()
    jobs = [
        Job(
            workflow_step=PrimaryInstanceMessageWatcher,
            params={"identifier": identifier, "workflow_maestro_id": workflow_maestro_id},
            instance_id="primary",
        ),
        Job(
            workflow_step=SecondaryInstanceMessageWatcher,
            inputs={
                "file_artefact": (
                    ArtefactTransferredToSecondary,
                    {"identifier": identifier, "instance_id": "secondary"},
                )
            },
            instance_id="secondary",
        ),
    ]
    expected_status_counts = prior_status_counts.copy()
    expected_status_counts.setdefault("completed", 0)
    expected_status_counts["completed"] += (
        len(jobs) + 3
    )  # 3 extra transfer jobs (staging, transfer, unpack)
    await order_workflow(
        WorkflowModel(maestro_id=workflow_maestro_id, workflow_name="job_chain", jobs=jobs)
    )
    async with asyncio.timeout(120):
        while await get_job_status_counts() != expected_status_counts:
            await asyncio.sleep(1)
    return 0


@patch(
    "workflowsteps.packagetransferrer.PackageTransferrer.build_s3_command", mock_build_s3_command
)
@patch("config.config.package.run_transfer", True)
async def run_steps() -> int:
    message_watcher_classes: list[type[MessageWatcher]] = []
    if Config.INSTANCE_ID == "primary":
        message_watcher_classes.extend([PrimaryInstanceMessageWatcher, PackageStager])
    elif Config.INSTANCE_ID == "transfer":
        message_watcher_classes.append(PackageTransferrer)
    elif Config.INSTANCE_ID == "secondary":
        message_watcher_classes.extend([SecondaryInstanceMessageWatcher, PackageUnpacker])
    else:
        raise RuntimeError(f"Invalid instance id: {Config.INSTANCE_ID}")
    steps_task = await run_workflow_steps(
        message_watcher_classes=message_watcher_classes,
        file_watcher_classes=[],
    )
    await steps_task
    return 0


if __name__ == "__main__":
    options = {func.__name__: func for func in [init_db, run_steps, place_workflow_order]}
    exit(asyncio.run(options[sys.argv[1]]()))

#!/bin/bash

set -euo pipefail

thisdir="$(dirname "$0")"
certdir="$thisdir/certs"
mkdir -p "$certdir"

rm -f "$certdir/ca.key" "$certdir/ca.pem" "$certdir/nats.key" "$certdir/nats.pem"

# Generate certificate authority (CA)
openssl ecparam -name secp521r1 -genkey -noout -out "$certdir/ca.key"
openssl req -x509 -new -key "$certdir/ca.key" -sha512 -days 365 -out "$certdir/ca.pem" -subj "/CN=nats-ca"

# Generate server key and certificate signing request (CSR)
openssl ecparam -name secp521r1 -genkey -noout -out "$certdir/nats.key"
openssl req -new -key "$certdir/nats.key" -out "$certdir/server.csr" -config "$thisdir/openssl.cnf"

# Sign the server certificate with the CA
openssl x509 -req -in "$certdir/server.csr" -CA "$certdir/ca.pem" -CAkey "$certdir/ca.key" \
    -CAcreateserial -out "$certdir/nats.pem" -sha512 -days 365 \
    -extfile "$thisdir/openssl.cnf" -extensions req_ext

rm -f "$certdir/server.csr" "$certdir/ca.srl"
#!/bin/bash

set -euxo pipefail

export COMPOSE_FILE="docker-compose-triple.yml"

# Start rsync daemon in steps-2 (secondary)
docker compose exec steps-2 bash -c "
  echo '[filelock]
path = /filelock
read only = false
use chroot = no' > /tmp/rsyncd.conf && \
  rsync --daemon --config=/tmp/rsyncd.conf
"

# Change ownership of transfer directories
docker compose exec -u root steps-1 chown -R steps:steps /transfer
docker compose exec -u root steps-2 chown -R steps:steps /filelock /target

# Run the demo
docker compose exec steps-1 demo/demo.py init_db
docker compose exec steps-2 demo/demo.py init_db
docker compose exec --no-TTY steps-1 demo/demo.py run_steps < /dev/null &
docker compose exec --no-TTY steps-transfer demo/demo.py run_steps < /dev/null &
docker compose exec --no-TTY steps-2 demo/demo.py run_steps < /dev/null &
docker compose exec --no-TTY steps-1 demo/demo.py place_workflow_order
docker compose exec --no-TTY steps-2 demo/demo.py place_workflow_order
docker compose exec steps-1 pkill -f demo/demo.py
wait -n
docker compose exec steps-transfer pkill -f demo/demo.py
wait -n
docker compose exec steps-2 pkill -f demo/demo.py
wait -n
